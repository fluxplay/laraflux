<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**/
/* FLUXPLAY */
/**/
Route::resource('clients', 'ClientController');
Route::get('search/c/{search_nom}', ['uses' =>'ClientController@search']);
Route::get('search/f/{id}', ['uses' =>'FactureController@search']);
Route::get('dash', function () {
    return view('tst');
});

Route::resource('artisans', 'ArtisanController');

Route::get('search/a/{search_nom}', ['uses' =>'ArtisanController@search']);
//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'DashboardController@index')->name('dashboard');
Route::get('/', 'DashboardController@index')->name('dashboard');

Route::get('/factures', 'FactureController@getFactures')->name('factures');
Route::get('/afactures', 'FactureController@getFacturesArtisan')->name('afactures');
Route::get('/cfactures', 'FactureController@getFacturesClient')->name('cfactures');
Route::get('/recapitulatif', 'FactureController@getRecapitulatifs')->name('recapitulatifs');
Route::post('/recapitulatif', 'FactureController@getRecapitulatifs')->name('recapitulatifs');
Route::get('/pdf/{num_facture}', ['uses' =>'PdfController@read'])->name('pdf');
/* prestations */
Route::get('/prestations', 'PrestationController@index')->name('prestations');
Route::post('/prestations', 'PrestationController@store')->name('prestations.store');
Route::get('/prestations/edit/{id}', 'PrestationController@edit')->name('prestations.edit');
Route::patch('/prestations/update', 'PrestationController@update')->name('prestations.update');
Route::post('/rm/p', 'PrestationController@delete')->name('prestations.delete');
Route::post('/rm/f', 'FactureController@sendFacture')->name('factures.sendfacture');
Route::get('/rm/p/{id}', 'PrestationController@delete2');
// attestations
Route::get('/attestations', 'AttestationController@index')->name('attestations');
Route::get('/pdf/{idClient}/{year}', 'PdfController@readAttestation')->name('attestations.read');
Route::post('/attestations', 'AttestationController@index')->name('attestations.indexWithDate');
Route::post('/attestations/send', 'AttestationController@sendAttestation')->name('attestations.sendmail');
/* -- MAIL --*/
use App\Mail\MailtrapFacture;
use Illuminate\Support\Facades\Mail;
Route::get('/sendmail', function () {


    Mail::to('k3nrdx@example.com')->send(new MailtrapFacture('toto','15855')); 

    return 'A message has been sent to Mailtrap!';

});

Route::get('/sendmail2', 'PrestationController@update');

/**/
/* UPCV */
/**/
Route::get('upcv', function () {
    return view('upcv.home');
});

Route::get('upcv/particuliers', function () {
    return view('upcv.particuliers');
});

Route::get('upcv/professionnels', function () {
    return view('upcv.professionnels');
});

Route::get('upcv/contact', function () {
    return view('upcv.contact');
});

Route::get('upcv/monespace', function () {
    return view('upcv.monespace');
});

Route::any('{query}',
    function() { return redirect('/home'); })
    ->where('query', '.*');

Auth::routes();
