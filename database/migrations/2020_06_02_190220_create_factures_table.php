<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factures', function (Blueprint $table) {
            $table->increments('num_facture');
            $table->string('id_facture');
            $table->date('date_edition')->nullable();
            $table->date('date_lecture')->nullable();
            $table->enum('typeFacture', ['Client', 'Artisan','Cooperative']);
            $table->unsignedInteger('id_client')->nullable($value = true)->index();
            $table->unsignedInteger('id_artisans')->nullable($value = true)->index();
            $table->decimal('montantHT',8,2)->default(0);
            $table->decimal('montantTTC',8,2)->default(0);
            $table->decimal('montantCESU',8,2)->default(0);
            $table->date('date');	
            $table->string('reference_prestation')->nullable();
            $table->string('txt_complement')->nullable();
            $table->string('livret')->nullable();
            $table->integer('pub')->nullable();
        });

        Schema::table('factures', function($table) {
            $table->foreign('id_client')->references('id_client')->on('clients');
            $table->foreign('id_artisans')->references('id_artisans')->on('artisans');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factures');
    }
}
