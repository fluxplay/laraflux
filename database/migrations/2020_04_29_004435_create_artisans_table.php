<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtisansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('artisans', function (Blueprint $table) {
            $table->increments('id_artisans');
            $table->enum('type', ['eiae', 'eurl', 'sarl', 'sas']);
            $table->string('nom');
            $table->string('contact');
            $table->string('adresse');
            $table->string('ville');
            $table->string('cp');
            $table->string('siret');
            $table->string('telephone')->nullable($value = true);
            $table->string('mail')->nullable($value = true);
            $table->date('adhesion');
            $table->string('nature');
            $table->string('nature_sec');
            $table->decimal('tva',4,1)->default(0.0);
            $table->decimal('taux',4,1)->default(0.0);
            $table->string('siteweb')->nullable($value = true);
            $table->string('facebook')->nullable($value = true);
            $table->string('linkedin')->nullable($value = true);
            $table->string('logo')->nullable($value = true);
            $table->integer('actif')->default(1);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artisans');
    }
}
