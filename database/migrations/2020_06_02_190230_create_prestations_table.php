<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestations', function (Blueprint $table) {
            $table->increments('id_prestation');
            $table->string('typePayement');
            $table->string('num_cheque')->nullable();
            $table->unsignedInteger('id_client')->nullable($value = false)->index();
            $table->date('date_prestation');
            $table->date('date_virement');
            $table->enum('type', ['jardinage', 'informatique', 'nettoyage', 'traiteur', 'bricolage', 'cours','administratif']);
            $table->integer('nb_heure');
            $table->unsignedInteger('id_artisans')->nullable($value = false)->index();
            $table->string('num_factureClient');
            $table->string('num_factureArtisan');
            $table->decimal('montant',8,2)->default(0);
            $table->decimal('montantCESU',8,2)->default(0);
            $table->decimal('coeff',4,1)->default(0);
            $table->decimal('sz_tva',4,1);
            $table->decimal('sz_tva_artisan',4,1);
            $table->decimal('ttc_artisan',8,2)->default(0);
        });

        Schema::table('prestations', function($table) {
            $table->foreign('id_client')->references('id_client')->on('clients');
            $table->foreign('id_artisans')->references('id_artisans')->on('artisans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestations');
    }
}
