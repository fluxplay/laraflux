<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactureprestationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factureprestations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('num_facture')->nullable($value = false)->index();
            $table->unsignedInteger('id_prestation')->nullable($value = false)->index();
            $table->enum('type', ['client', 'artisan','coop']);
            $table->dateTime('date',0);
        });
        
        Schema::table('factureprestations', function($table) {
            $table->foreign('num_facture')->references('num_facture')->on('factures');
            $table->foreign('id_prestation')->references('id_prestation')->on('prestations');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factureprestations');
    }
}
