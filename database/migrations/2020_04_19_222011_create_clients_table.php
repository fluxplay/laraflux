<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id_client');
            $table->string('civilite')->default("M.");
            $table->string('attest_edition')->nullable($value = true);
            $table->string('attest_lecture')->nullable($value = true);
            $table->string('nom');
            $table->string('prenom');
            $table->string('adresse1');
            $table->string('adresse2')->nullable($value = true);
            $table->string('ville');
            $table->string('telephone')->nullable($value = true);
            $table->string('cp');
            $table->string('email')->nullable($value = true);
            $table->string('livret')->default("non");
            $table->string('fpapier')->default("non");
            $table->string('b_efacture')->default("non");
            $table->integer('actif')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
