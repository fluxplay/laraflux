#!/bin/bash

docker exec -i -w /var/www/html laraflux php artisan migrate:fresh


mysql -usplinter -pmyp laraflux < users.sql
mysql -usplinter -pmyp laraflux < clients.sql
mysql -usplinter -pmyp laraflux -e "update clients set email='gpe@fluxplay.fr' where email <>'';"
mysql -usplinter -pmyp laraflux < artisans.sql
mysql -usplinter -pmyp laraflux < factures.sql
mysql -usplinter -pmyp laraflux < fps.sql
mysql -usplinter -pmyp laraflux < prestations.sql

