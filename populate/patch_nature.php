<?php
// on se connecte MySQL
$host = "172.16.1.57";
$user = "root" ;
$pass = "myp";
$db = "laraflux_tmp";

$container_fichier_sql="/var/www/html/populate/patch_nature_artisans.sql";

$mysqli = new mysqli($host, $user, $pass, $db);

if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
  }

$sql="";
$result = $mysqli->query("select id_artisans, nature, nature_sec from artisans");
while ($row = $result->fetch_array()) 
{
    $pri = updatePriNature($row['nature']);
    $sec = updateSecNature($row['nature_sec'],$pri);
    $sql .= "UPDATE artisans set nature='".$pri."', nature_sec='".$sec."' where id_artisans=".$row['id_artisans'].";\n";
}
file_put_contents($container_fichier_sql, $sql);
$mysqli->close();
// # tests unitaires
// # 
// echo '<br>' . updatePriNature("jardinage,toto,tata");
// echo '<br>' . updatePriNature("toto,tata");
// echo '<br>' . updatePriNature("toto");
// echo '<br>' . updatePriNature("toto,tata,jardinage");
// echo '<br>' . updatePriNature("");
// echo '<br>' ;
// echo '<br>' . updateSecNature("jardinage,toto,tata", "toto");
// echo '<br>' . updateSecNature("toto,tata", "toto");
// echo '<br>' . updateSecNature("toto","toto");
// echo '<br>' . updateSecNature("toto,tata,jardinage", "toto");
function updatePriNature($nature)
{
    if(!empty($nature))
    {
        $priNature = "";
        $a_nature = explode(',', $nature);
        if(in_array('jardinage',$a_nature)){
            $priNature='jardinage';
        }else{
            $priNature=$a_nature[0];
        }
        return $priNature;
    }
}
function updateSecNature($nature,$todelete)
{
    if(!empty($nature)){
        $secNature = "";
        $a_nature = explode(',', $nature);
        $a_sec = array_diff($a_nature,array($todelete));
        if(is_array($a_sec) && count($a_sec)>0 )
        {
            $secNature = implode(',', $a_sec);
        }else{
            $secNature = "";
        }
        return $secNature;
    }
}
?> 