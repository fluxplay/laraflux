#!/bin/bash

# LES TABLES DOIVENT ETRE SUPPRIMER / RE-CREE PAR 'Laravel php Artisan'
# docker exec -i m57 /usr/bin/mysql -uroot -pmyp -e "drop table IF EXISTS users;"
# docker exec -i m57 /usr/bin/mysql -uroot -pmyp -e "drop table IF EXISTS client;"
# docker exec -i m57 /usr/bin/mysql -uroot -pmyp -e "drop table IF EXISTS artisans;"
# docker exec -i m57 /usr/bin/mysql -uroot -pmyp -e "drop table IF EXISTS factures;"

container=m57;

docker exec -i m57 /usr/bin/mysql -uroot -pmyp -e "CREATE DATABASE IF NOT EXISTS laraflux;"
docker exec -i -w /var/www/html laraflux php artisan migrate:fresh

# TODO tester ces requete avec docker exec pour voir si on a besoin d'echapper les $
# Generate the users.sql file for Laravel base users
#
#INSERT INTO `users` (`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`type`,`level`,`coops`,`id_user`) VALUES 
#(1,'Gregory PEREZ','k3nrdx@gmail.com','$2y$10$3CSDHx57Z.EqSQRZse0aPe5AYhwOU231QrF9FBwRZok0uPrNGgPuC',NULL,'2020-05-28 19:28:47','2020-05-28 19:28:47','adm','10','84','1');

#INSERT INTO `users` (`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`type`,`level`,`coops`,`id_user`) VALUES 
#(2,'Nicolas ARTISAN','artisan@gmail.com','$2y$10$3CSDHx57Z.EqSQRZse0aPe5AYhwOU231QrF9FBwRZok0uPrNGgPuC',NULL,'2020-05-28 19:28:47','2020-05-28 19:28:47','artisan','10','84','2');

#INSERT INTO `users` (`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`type`,`level`,`coops`,`id_user`) VALUES 
#(3,'Cecile CLIENT','client@gmail.com','$2y$10$3CSDHx57Z.EqSQRZse0aPe5AYhwOU231QrF9FBwRZok0uPrNGgPuC',NULL,'2020-05-28 19:28:47','2020-05-28 19:28:47','client','10','84','3');

#INSERT INTO `users` (`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`type`,`level`,`coops`,`id_user`) VALUES 
#(4,'Franck RETAMOSA','admin@gmail.com','$2y$10$3CSDHx57Z.EqSQRZse0aPe5AYhwOU231QrF9FBwRZok0uPrNGgPuC',NULL,'2020-05-28 19:28:47','2020-05-28 19:28:47','adm','10','84','4');


docker exec -i m57 /usr/bin/mysql -uroot -pmyp -e "SET @@GLOBAL.FOREIGN_KEY_CHECKS=0;"
docker exec -i m57 /usr/bin/mysql -uroot -pmyp -e "SELECT @@GLOBAL.foreign_key_checks, @@SESSION.foreign_key_checks;"

cat users.sql | docker exec -i $container /usr/bin/mysql -uroot -pmyp laraflux

cat clients.sql     |  docker exec -i $container /usr/bin/mysql -uroot -pmyp laraflux ;
cat artisans.sql    |  docker exec -i $container /usr/bin/mysql -uroot -pmyp laraflux ;
cat fps.sql         |  docker exec -i $container /usr/bin/mysql -uroot -pmyp laraflux ;
cat factures.sql    |  docker exec -i $container /usr/bin/mysql -uroot -pmyp laraflux ;
cat prestations.sql |  docker exec -i $container /usr/bin/mysql -uroot -pmyp laraflux ;

