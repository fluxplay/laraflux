@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            UPCV
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            {{-- &copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved. --}}
            <b>Franck RETAMOSA</b><br>
            Co Gérant Un ProChez Vous 84<br>
            3620 dites "un pro chez vous"<br>
            upcv84@unprochezvous.fr<br>
            http://www.unprochezvous.com<br>
        @endcomponent
    @endslot
@endcomponent
