@extends('layouts.base')
 
 {{-- Using for static function --}}
 <?php use App\Prestation; ?>
 <?php use App\Artisan; ?>

@section('header')
    <!-- DataTables -->
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">

    <link href="/vendor/xadmino/Admin/assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="/vendor/xadmino/Admin/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="/vendor/xadmino/Admin/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/vendor/xadmino/Admin/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    
<style>


.results {
    display: none;
    width: 228px;
    position: fixed;
    z-index: 100;
    border: 1px solid #AAA;
    border-top-width: 0;
    background-color: #FFF;
}
div.autoresult:hover 
{
    background-color: #F5F5F5;
    cursor: pointer;
}

.modal-dialog {
    width: 850px;
    margin: 30px auto;
    margin-top: 250px;
}

</style>

<script>

window.onload = function() 
{
    var menu = document.getElementById("menu-prestations");
    menu.className = menu.className + ' ' + 'active' ;

    jQuery("#datepicker").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months"
    });


    function setLineLight(id){
        var gh = document.getElementById(id);
        if(gh){ gh.className='success';}
    }

    @include('subjs.autoClient')
    @include('subjs.autoArtisan')

}

function jsSetYear(idYear)
{
	ListBox = document.getElementsByName('sz_year') ;
	if(ListBox[0] != null)
	{
    for(j=0; j<ListBox[0].length; j++)
        {//alert(idYear+"=("+l_name+")="+ListBox[0].options[j].value);
            if(idYear == ListBox[0].options[j].value)
            {
                ListBox[0].options[j].selected=true;
                //alert("c la bonne selection pour :"+idYear+" j:"+j+" nb:"+ListBox[0].options[j].value);
                break;
            }
        }
    }
}
function jsSetEntrepriseTaux(taux)
{
	ListBox = document.getElementsByName('se_taux') ;
	if(ListBox[0] != null)
	{
    for(j=0; j<ListBox[0].length; j++)
        {
            if(taux == ListBox[0].options[j].value)
            {
                ListBox[0].options[j].selected=true;
                ListBox[0].style="background-color:#01ba9a45";
                break;
            }
        }
    }
}
function jsSetEntrepriseTva(tva)
{
	ListBox = document.getElementsByName('sz_tva_artisan') ;
	if(ListBox[0] != null)
	{
        for(j=0; j<ListBox[0].length; j++)
            {
                if(tva == ListBox[0].options[j].value)
                {
                    ListBox[0].style="background-color:#01ba9a45";
                    ListBox[0].options[j].selected=true; 
                    break;
                }
            }
    }
}
function jsSetNature2Taux(tva)
{
	ListBox = document.getElementsByName('sz_tva_nature_2') ;
	if(ListBox[0] != null)
	{
        for(j=0; j<ListBox[0].length; j++)
            {
                if(tva == ListBox[0].options[j].value)
                {
                    ListBox[0].style="background-color:#01ba9a45";
                    ListBox[0].options[j].selected=true; 
                    break;
                }
            }
    }
}
function jsSetNatureTaux(tva)
{
	ListBox = document.getElementsByName('sz_tva_nature') ;
	if(ListBox[0] != null)
	{
        for(j=0; j<ListBox[0].length; j++)
            {
                if(tva == ListBox[0].options[j].value)
                {
                    ListBox[0].style="background-color:#01ba9a45";
                    ListBox[0].options[j].selected=true; 
                    break;
                }
            }
    }
}
function jsSetMonth(idMonth)
{	idMonth--;
	var forumulaire = document.forms['form_mois'].elements ;
	forumulaire.sz_mois.options[idMonth].selected=true;
}
function setTvaNature(nature,secondaire=false){
    if(nature=="nettoyage" || nature=="administratif" || nature=="accompagnement" || nature=="traiteur" || nature=="bricolage")
    {
        if(secondaire)
        {
            jsSetNature2Taux('10.0');
        }else{
            jsSetNatureTaux('10.0');
        }
        
    }else{
        if(secondaire)
        {
            jsSetNature2Taux('20.0');
        }else
        {
            jsSetNatureTaux('20.0');
        }
    }
}


function getSpanNature(type)
{
    if(type=="jardinage"){
        return "<span class='label label-success'>"+type+"</span>";
    }else if(type=="nettoyage"){
        return "<span class='label label-warning'>"+type+"</span>";
    }else if(type=="administratif"){
        return "<span class='label label-danger'>"+type+"</span>"; 
    }else if(type=="informatique"){
        return "<span class='label label-info'>"+type+"</span>";
    }else if(type=="accompagnement"){
        return "<span class='label label-default'>"+type+"</span>";
    }else if(type=="cours"){
        return "<span class='label label-success'>"+type+"</span>";
    }else if(type=="bricolage"){
        return "<span class='label label-info'>"+type+"</span>";
    }
}

function getDetailsPrestation(num_facture)
{
    //make an ajax call and get status value using the same 'id'
    $.ajax({
            type:"GET",//or POST
            url:'{{env('APP_URL')}}/public/search/f/'+num_facture,
            //can send multipledata like {data1:var1,data2:var2,data3:var3
            //can use dataType:'text/html' or 'json' if response type expected 
            success:function(responsedata)
            {
                // process on data
                var thepath = "'{{env('APP_URL')}}'";
                var data = JSON.parse(responsedata);
                var p5244 = document.getElementById("p5244");
                var html ="<table class='table'><thead><tr>";
                html+="<td>N°</td>";
                html+="<td>Nature</td>";
                html+="<td>Nb heure</td>";
                html+="<td>Montant TTC</td>";
                html+="<td>Montant CESU</td>";
                html+="<td>Taux</td>";
                html+="<td>TVA</td>";
                html+="<td>Date</td>";
                html+="<td>Action</td>";
                html+="</tr></thead><tbody></tbody>";
                for(i=0;i<data.length;i++){
                    html+= "<tr><td>"+data[i].id_prestation+"</td><td>"+getSpanNature(data[i].type)+"</td><td>"+data[i].nb_heure+"</td><td>"+data[i].montant+"</td><td>"+data[i].montantCESU+"</td><td>"+data[i].coeff+"</td><td>"+data[i].sz_tva+"</td>";
                    html+= "<td>"+data[i].date_prestation+"</td><td><button onclick=updatePrestation("+thepath+","+data[i].id_prestation+"); class='btn btn-danger btn-xs clickable'><span class='glyphicon glyphicon-pencil'></span></button><td></tr>";
                }
                html+="</table>";
                p5244.innerHTML = html;
                $('#modalDetailsPrestation').modal();
            }
    });

}

function setLigneLight(id){
    var gh = document.getElementById(id);
    if(gh){ gh.className='success';}
}
</script>
<style type="text/css">
    .form-control
    {
        text-transform: uppercase; 
    }

    .btn-upcv
    {
        background-color: #ef9303;
        color: #ffffff;
    }
</style>
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
<!-- body -->

<div class="row">
    <div class="col-sm-6 col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Prestation</h4>
            </div>
            <div class="panel-body">
            <form method="post" class="form-horizontal" action="{{ route('prestations.store') }}">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Client</label>
                    <div class="col-sm-9">
                        <input id="search_client" type="text" class="form-control" autocomplete="off">
                        <input type="hidden" id="id_client" name="id_client" value="2">
                        <div id="results_client" class="results"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">Entreprise</label>
                    <div class="col-sm-9">
                        <input id="search_artisan" type="text" class="form-control" autocomplete="off">
                        <input type="hidden" id="id_artisans" name="id_artisans" >
                        <div id="results_artisan" class="results"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword4" class="col-sm-3 control-label">TVA</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="sz_tva_artisan" id="sz_tva_artisan">
                            @foreach(Artisan::getTvaOptions() as $key => $value)
                                <option value="{{ $key }}" >{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="date" class="col-sm-3 control-label">Date</label>
                    <div class="col-sm-9">
                        <input type="date" name="sz_date" class="form-control" value="{{ date('Y-m-d') }}" placeholder="mm/dd/yyyy">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword4" class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="sz_textarea" cols="35" id="sz_textarea">Complément sur facture</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-sm-6 col-lg-4">
        <div class="panel-group" id="accordion">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Activité principale</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
            <div class="panel-body form-horizontal">
                
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nature</label>
                        <div class="col-sm-9">
                        <select class="form-control" name="sz_workType" id="sz_workType" onchange="setTvaNature(this.value);">
                            @foreach(Prestation::getNatureOptions() as $key => $value)
                                <option value="{{ $key }}" >{{ $value }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Temps</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="sz_nbTime" id="sz_nbTime" value="FORFAIT">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4" class="col-sm-3 control-label">Montant</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="i_price" id="i_price" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4" class="col-sm-3 control-label">CESU</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="i_priceCESU" id="i_priceCESU" max="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4" class="col-sm-3 control-label">TVA</label>
                        <div class="col-sm-9">
                        <select class="form-control" name="sz_tva_nature" id="sz_tva_nature">
                            @foreach(Prestation::getTvaNatureOptions() as $key => $value)
                                <option value="{{ $key }}" >{{ $value }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
            </div> {{-- panel-body --}}
            </div> {{-- collaps1 --}}
            
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Activité secondaire</a>
                </h4>
            </div> {{-- heading --}}
            <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body form-horizontal">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nature</label>
                        <div class="col-sm-9">
                        <select class="form-control" name="sz_workType_2" id="sz_workType_2" onchange="setTvaNature(this.value,1);">
                            @foreach(Prestation::getNatureOptions() as $key => $value)
                                <option value="{{ $key }}" >{{ $value }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Temps</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="sz_nbTime_2" id="sz_nbTime_2" value="FORFAIT">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4" class="col-sm-3 control-label">Montant</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="i_price_2" id="i_price_2" max="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4" class="col-sm-3 control-label">CESU</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="i_priceCESU_2" id="i_priceCESU_2" max="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4" class="col-sm-3 control-label">TVA</label>
                        <div class="col-sm-9">
                        <select class="form-control" name="sz_tva_nature_2" id="sz_tva_nature_2">
                            @foreach(Prestation::getTvaNatureOptions() as $key => $value)
                                <option value="{{ $key }}" >{{ $value }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                
            </div> {{-- panel-body --}}
            </div> {{-- heading --}}
        </div>
        </div>


    </div>

    <div class="col-sm-6 col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Paiement</h4>
            </div>
            <div class="panel-body form-horizontal">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Mode de réglement</label>
                    <div class="col-sm-9">
                    <select class="form-control" name="sz_typePayement" id="sz_typePayement">
                        @foreach(Prestation::getTypePaiementOptions() as $key => $value)
                            <option value="{{ $key }}" >{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">N° de Chèque</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="i_numCheque" id="i_numCheque" max="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword4" class="col-sm-3 control-label">Taux %</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="se_taux" id="se_taux">
                            @foreach(Artisan::getTauxOptions() as $key => $value)
                                <option value="{{ $key }}" >{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword4" class="col-sm-3 control-label">Référence</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="sz_reference" id="sz_reference" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword4" class="col-sm-3 control-label">Date virement</label>
                    <div class="col-sm-9">
                        <input type="date" name='sz_date_vire' class="form-control" value="{{ date('Y-m-d', strtotime(date('Y-m-d')." +4 day")) }}" placeholder="mm/dd/yyyy">
                </div>
                </div>

                <button type="submit" name="btn_add" class="btn btn-upcv form-control">Ajouter</button>
            </form>
            </div>
        </div>
    </div>
</div>

{{-- form status row  --}}
@include('subviews.formstatus')

{{-- for user status row  --}}
@include('subviews.status')


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des factures crée apres validation de la prestation</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-responsive"
                    class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                    width="100%">
                    <thead>
                        <tr>
                            <td>N° Facture</td>
                            <td>Client</td>
                            <td>Montant TTC</td>
                            <td>Entrepise</td>
                            <td>Montant TTC</td>
                            <td>Date</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($prestations as $key => $tab)
                            <tr id='{{ $tab['id_facture'] }}'>
                                <td><b><a href='{{$key}}' onclick='PopupFacture("{{ env('APP_URL') }}", {{$key}});return false;' >{{ $tab['id_facture'] }}</a></b></td>
                                <td><span class="glyphicon glyphicon-user" aria-hidden="true">
                                    </span>
                                    <b><a href='{{env('APP_URL')}}/public/clients/{{ $tab['id_client'] }}/edit' >{{ $tab['client'] }}</a><b></td>
                                <td>{{ $tab['cmontantTTC'] }} €</td>
                                <td>{{ $tab['artisan'] }}</td>
                                <td>{{ $tab['amontantTTC'] }} €</td>
                                <td>{{ $tab['date'] }}</td>
                                <td><button href="#modalDetailsPrestation" onclick="getDetailsPrestation('{{$tab['num_facture']}}');" class="btn btn-success btn-xs clickable btnDelPres">Detail</button></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->

 <!-- Modal box -->
<div id='modalDetailsPrestation' class='modal fade' role='dialog'>
<div class='modal-dialog'>
<div class='modal-content'>
<div class='modal-header'>
<button type='button' class='close' data-dismiss='modal'>&times;</button>
<h4 class='modal-title'>Detail des prestations associées a la facture</h4>
</div>
<div class='modal-body'>
    <div id='p5244' class='form-group'>

    </div>
</div>
    <div class='modal-footer'>
    </div>
</div>
</div>
</div>
</div>
    <!-- End Modal -->

    <!-- Modal box -->
    <div id='modalAdminDelPres' class='modal fade' role='dialog'>
    <div class='modal-dialog'>
    <div class='modal-content'>
    <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
    <h4 class='modal-title'>Supprimer la prestation</h4>
    </div>
    <div class='modal-body'>
    <form method="post" class="form-horizontal" action="{{ route('prestations.delete') }}">
    {{ csrf_field() }}
    <input type='hidden' name='idpres' id='idpres'>
    <div class='form-group'>
    <label class='control-label col-sm-10' for=''><h4>Voulez-vous vraiment supprimer la prestation ?</h4></label>
    </div>
    <div class='form-group'>
    <div class='col-sm-offset-2 col-sm-10'>
    </div>
    </div>
    </div>
    <div class='modal-footer'>
    <button name='btn_valider' type='submit' class='btn btn-default'>Oui</button>
    <button type='button' class='btn btn-default' data-dismiss='modal'>Non</button>
    </div>
    </div>
    </form> 
    </div>
    </div>
    </div>
    <!-- End Modal -->

        </div> <!-- container -->
    </div> <!-- content -->
    <footer class="footer">
        2016 - 2020 © FluXplaY.
    </footer>

</div>
@endsection


@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Plugins js -->
<script src="/vendor/xadmino/Admin/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

<!-- Datatable init js -->
{{-- <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script> --}}

<script src="/vendor/xadmino/Admin/assets/pages/datatables.prestation.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fluxbase.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>

<!-- Jquery for modal box -->
<script>
<!-- jquery for modal -->
$('.btnDelPres').click(function() {   
    $('#modalAdminDelPres #idpres').val($(this).data('idpres'));
});

@isset($highlights)
    @foreach ($highlights as $highlight)
        setLineLight('{{$highlight}}');
    @endforeach
@endisset
</script>
@endsection