@extends('layouts.base')

{{-- Using for static function --}}
<?php use App\Artisan; ?>
<?php use App\Prestation; ?>

@section('header')
    <!-- DataTables -->
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">

    <link href="/vendor/xadmino/Admin/assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="/vendor/xadmino/Admin/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="/vendor/xadmino/Admin/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/vendor/xadmino/Admin/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    

<style>
.results {
    display: none;
    width: 228px;
    position: fixed;
    z-index: 100;
    border: 1px solid #AAA;
    border-top-width: 0;
    background-color: #FFF;
}
div.autoresult:hover 
{
    background-color: #F5F5F5;
    cursor: pointer;
}
.form-control
{
    text-transform: uppercase; 
}
.btn-upcv
{
    background-color: #ef9303;
    color: #ffffff;
}
</style>

<script>
window.onload = function() 
{
    var menu = document.getElementById("menu-prestations");
    menu.className = menu.className + ' ' + 'active' ;

    jQuery("#datepicker").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months"
    });



function autocomplete_client(){

        var searchElement   = document.getElementById('search_client'),
            results         = document.getElementById('results_client'),
            selectedResult  = -1, // Permet de savoir quel résultat est sélectionné : -1 signifie "aucune sélection"
            previousRequest, // On stocke notre précédente requête dans cette variable
            previousValue   = searchElement.value; // On fait de même avec la précédente valeur

        function getResults(keywords) { // Effectue une requête et récupère les résultats
            var xhr = new XMLHttpRequest();
            var  thepath= "{{env('APP_URL')}}";
            xhr.open('GET', thepath + '/public/search/a/'+ encodeURIComponent(keywords));
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    displayResults(xhr.responseText);
                }
            };
            xhr.send(null);
            return xhr;
        }

    function displayResults(response) { // Affiche les résultats d'une requête
            results.style.display = response.length ? 'block' : 'none'; // On cache le conteneur si on n'a pas de résultats

            if (response.length) { // On ne modifie les résultats que si on en a obtenu

                response = response.split('@');
                var responseLen = response.length;

                results.innerHTML = ''; // On vide les résultats

                for (var i = 0, div ; i < responseLen ; i++) {

                    value = response[i].split(':');
                    div = results.appendChild(document.createElement('div'));
                    if(value[0]>0){
                    div.setAttribute("ref", value[0]);
                    div.setAttribute("class", "autoresult");
                    div.innerHTML = value[1];
                    
                    div.onclick = function() {
                        chooseResult(this);
                    };
                    }
                }

            }
        }


        function chooseResult(result) { // Choisi un des résultats d'une requête et gère tout ce qui y est attaché
            searchElement.value = previousValue = result.innerText; // On change le contenu du champ de recherche et on enregistre en tant que précédente valeur
            results.style.display = 'none'; // On cache les résultats
            result.className = ''; // On supprime l'effet de focus
            selectedResult = -1; // On remet la sélection à "zéro"
            searchElement.focus(); // Si le résultat a été choisi par le biais d'un clique alors le focus est perdu, donc on le réattribue
            document.getElementById('id_client').value = result.getAttribute('ref');
        }

        searchElement.onkeyup = function(e) {
            e = e || window.event; // On n'oublie pas la compatibilité pour IE
            var divs = results.getElementsByTagName('div');
            if (e.keyCode == 38 && selectedResult > -1) { // Si la touche pressée est la flèche "haut"
                divs[selectedResult--].className = '';
                if (selectedResult > -1) { // Cette condition évite une modification de childNodes[-1], qui n'existe pas, bien entendu
                    divs[selectedResult].className = 'result_focus';
                }
            }else if (e.keyCode == 40 && selectedResult < divs.length - 1) { // Si la touche pressée est la flèche "bas"
                results.style.display = 'block'; // On affiche les résultats
                if (selectedResult > -1) { // Cette condition évite une modification de childNodes[-1], qui n'existe pas, bien entendu
                    divs[selectedResult].className = '';
                }
                divs[++selectedResult].className = 'result_focus';
            }
            else if (e.keyCode == 13 && selectedResult > -1) { // Si la touche pressée est "Entrée"
                chooseResult(divs[selectedResult]);
            }
            else if (searchElement.value != previousValue) { // Si le contenu du champ de recherche a changé
                previousValue = searchElement.value;
                if (previousRequest && previousRequest.readyState < 4) {
                    previousRequest.abort(); // Si on a toujours une requête en cours, on l'arrête
                }
                previousRequest = getResults(previousValue); // On stocke la nouvelle requête
                selectedResult = -1; // On remet la sélection à "zéro" à chaque caractère écrit
            }
        };
    }

    function autocomplete_artisan(){

        var searchElement   = document.getElementById('search_artisan'),
            results         = document.getElementById('results_artisan'),
            selectedResult  = -1, // Permet de savoir quel résultat est sélectionné : -1 signifie "aucune sélection"
            previousRequest, // On stocke notre précédente requête dans cette variable
            previousValue   = searchElement.value; // On fait de même avec la précédente valeur

        function getResults(keywords) { // Effectue une requête et récupère les résultats
            var xhr = new XMLHttpRequest();
            var  thepath= "{{env('APP_URL')}}";
            xhr.open('GET', thepath + '/public/search/a/'+ encodeURIComponent(keywords));
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    displayResults(xhr.responseText);
                }
            };
            xhr.send(null);
            return xhr;
        }

    function displayResults(response) { // Affiche les résultats d'une requête
            results.style.display = response.length ? 'block' : 'none'; // On cache le conteneur si on n'a pas de résultats

            if (response.length) { // On ne modifie les résultats que si on en a obtenu

                response = response.split('@');
                var responseLen = response.length;

                results.innerHTML = ''; // On vide les résultats

                for (var i = 0, div ; i < responseLen ; i++) {

                    value = response[i].split(':');
                    div = results.appendChild(document.createElement('div'));
                    if(value[0]>0){
                    div.setAttribute("ref", value[0]);
                    div.setAttribute("class", "autoresult");
                    div.innerHTML = value[1];
                    
                    div.onclick = function() {
                        chooseResult(this);
                    };
                    }
                }

            }
        }


        function chooseResult(result) { // Choisi un des résultats d'une requête et gère tout ce qui y est attaché
            searchElement.value = previousValue = result.innerText; // On change le contenu du champ de recherche et on enregistre en tant que précédente valeur
            results.style.display = 'none'; // On cache les résultats
            result.className = ''; // On supprime l'effet de focus
            selectedResult = -1; // On remet la sélection à "zéro"
            searchElement.focus(); // Si le résultat a été choisi par le biais d'un clique alors le focus est perdu, donc on le réattribue
            document.getElementById('id_artisans').value = result.getAttribute('ref');
        }

        searchElement.onkeyup = function(e) {
            e = e || window.event; // On n'oublie pas la compatibilité pour IE
            var divs = results.getElementsByTagName('div');
            if (e.keyCode == 38 && selectedResult > -1) { // Si la touche pressée est la flèche "haut"
                divs[selectedResult--].className = '';
                if (selectedResult > -1) { // Cette condition évite une modification de childNodes[-1], qui n'existe pas, bien entendu
                    divs[selectedResult].className = 'result_focus';
                }
            }else if (e.keyCode == 40 && selectedResult < divs.length - 1) { // Si la touche pressée est la flèche "bas"
                results.style.display = 'block'; // On affiche les résultats
                if (selectedResult > -1) { // Cette condition évite une modification de childNodes[-1], qui n'existe pas, bien entendu
                    divs[selectedResult].className = '';
                }
                divs[++selectedResult].className = 'result_focus';
            }
            else if (e.keyCode == 13 && selectedResult > -1) { // Si la touche pressée est "Entrée"
                chooseResult(divs[selectedResult]);
            }
            else if (searchElement.value != previousValue) { // Si le contenu du champ de recherche a changé
                previousValue = searchElement.value;
                if (previousRequest && previousRequest.readyState < 4) {
                    previousRequest.abort(); // Si on a toujours une requête en cours, on l'arrête
                }
                previousRequest = getResults(previousValue); // On stocke la nouvelle requête
                selectedResult = -1; // On remet la sélection à "zéro" à chaque caractère écrit
            }
        };
    }

    autocomplete_client();
    autocomplete_artisan();
}
</script>
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
<!-- body -->
<div class="row">
    <div class="col-sm-6 col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Prestation</h4>
            </div>
            <div class="panel-body">
            <form method="post" class="form-horizontal" action="{{ route('prestations.update') }}">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Client</label>
                    <div class="col-sm-9">
                        <input id="search_client" type="text" class="form-control" autocomplete="off" value="{{$prestation->client->nom.' '.$prestation->client->prenom}}">
                        <input type="hidden" id="id_client" name="id_client" value={{$prestation->id_client}}>
                        <input type="hidden" id="id_prestation" name="id_prestation" value={{$prestation->id_prestation}}>
                        <div id="results_client" class="results"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">Artisans</label>
                    <div class="col-sm-9">
                        <input id="search_artisan" type="text" class="form-control" autocomplete="off" value={{$prestation->artisan->nom}}>
                        <input type="hidden" id="id_artisans" name="id_artisans" value={{$prestation->id_artisans}}>
                        <div id="results_artisan" class="results"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword4" class="col-sm-3 control-label">TVA Artisans</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="sz_tva_artisan" id="sz_tva_artisan">
                            @foreach(Artisan::getTvaOptions() as $key => $value)
                            <option value="{{ $key }}" {{ $prestation->sz_tva_artisan == $key ? 'selected' : '' }}>
                                {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="date" class="col-sm-3 control-label">Date</label>
                    <div class="col-sm-9">
                        <input type="date" name="sz_date" class="form-control" value="{{ $prestation->date_prestation }}" placeholder="mm/dd/yyyy">
                    </div>
                </div>
                <button type="submit" name="btn_add" class="btn btn-upcv">Mettre a jour</button>
            </div>
        </div>
    </div>


    <div class="col-sm-6 col-lg-4">
        <div class="panel-group" id="accordion">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Travaux</a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
            <div class="panel-body form-horizontal">
                
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nature</label>
                        <div class="col-sm-9">
                        <select class="form-control" name="sz_workType" id="sz_workType">
                            @foreach(Prestation::getNatureOptions() as $key => $value)
                            <option value="{{ $key }}" {{ $prestation->type == $key ? 'selected' : '' }}>
                                {{ $value }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-3 control-label">Temps</label>
                        <div class="col-sm-9">
                        @if($prestation->nb_heure==1)
                            <input class="form-control" type="text" name="sz_nbTime" id="sz_nbTime" value="FORFAIT">
                        @else
                            <input class="form-control" type="text" name="sz_nbTime" id="sz_nbTime" value="{{$prestation->nb_heure}}">
                        @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4" class="col-sm-3 control-label">Montant</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="i_price" id="i_price" value={{ $prestation->montant }}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4" class="col-sm-3 control-label">CESU</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="i_priceCESU" id="i_priceCESU" value={{ $prestation->montantCESU }}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4" class="col-sm-3 control-label">TVA</label>
                        <div class="col-sm-9">
                        <select class="form-control" name="sz_tva_nature" id="sz_tva_nature">
                            @foreach(Prestation::getTvaNatureOptions() as $key => $value)
                            <option value="{{ $key }}" {{ $prestation->sz_tva == $key ? 'selected' : '' }}>
                                {{ $value }}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                
            </div> {{-- panel-body --}}
            </div> {{-- collaps1 --}}
        </div>
        </div>
    </div>

    <div class="col-sm-6 col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Paiement</h4>
            </div>
            <div class="panel-body form-horizontal">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Type de payement</label>
                    <div class="col-sm-9">
                    <select class="form-control" name="sz_typePayement" id="sz_typePayement">
                        @foreach(Prestation::getTypePaiementOptions() as $key => $value)
                            <option value="{{ $key }}" {{ $prestation->typePayement == $key ? 'selected' : '' }}>
                                {{ $value }}</option>
                        @endforeach
                    </select></div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">Cheque N°</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="i_numCheque" id="i_numCheque" value={{$prestation->num_cheque}}>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword4" class="col-sm-3 control-label">Coeff</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="se_coeff" id="se_coeff">
                            @foreach(Artisan::getTauxOptions() as $key => $value)
                                <option value="{{ $key }}" {{ $prestation->coeff == $key ? 'selected' : '' }}>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword4" class="col-sm-3 control-label">Ref.</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="sz_reference" id="sz_reference" value="NON DISPONIBLE!!" disabled >
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword4" class="col-sm-3 control-label">Date virement</label>
                    <div class="col-sm-9">
                        <input type="date" name='sz_date_vire' class="form-control" placeholder="mm/dd/yyyy" value={{ $prestation->date_virement}}>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword4" class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="sz_textarea" cols="35" id="sz_textarea" disabled > NON DISPONIBLE!!</textarea>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

{{-- form status row  --}}
@include('subviews.formstatus')

{{-- for user status row  --}}
@include('subviews.status')

    </div> <!-- container -->
    </div> <!-- content -->
    <footer class="footer">
        2016 - 2020 © FluXplaY.
    </footer>

</div>
@endsection


@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Plugins js -->
<script src="/vendor/xadmino/Admin/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

<!-- Datatable init js -->
{{-- <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script> --}}

<script src="/vendor/xadmino/Admin/assets/pages/datatables.prestation.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>
@endsection