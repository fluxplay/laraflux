<!-- Display a box to show the view status -->
<div class="row"> {{-- for user status row  --}}
    <div class="col-sm-12">
        @isset($status['text'])
            <div class="alert alert-{{ $status['style'] }} alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                @foreach ( $status['text'] as $ligne)
                    {!! $ligne !!} </br>
                @endforeach
            </div>
        @endisset
    </div>
</div>