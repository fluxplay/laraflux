<!-- Display a box to show the missing filed after sumitted the for, It's a Laravel feature -->
<div class="panel-body"> {{-- form status row  --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>