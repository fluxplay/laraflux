
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>FluXplay - Outil pour les artisans du web</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/vendor/xadmino/Admin/assets/images/favicon.ico">

        <link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">

<style>
h2 {
  font-family: Arial, Helvetica, sans-serif;
}
.alert-danger{
    background-color: #f8d7da;
}
</style>

    </head>


    <body>

        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">
            <div class="ex-page-content text-center">
                <h1 class="text-white">404!</h1>
                <h2 class="text-white">Désolé, cette page n'existe pas</h2><br>
                @if(!empty($txt))
                    <div class="alert alert-danger" role="alert">{{ $txt }}</div>
                    <a class="btn btn-primary waves-effect waves-light" href="home">Revenir a la page d'acceuil</a>
                @endif
                <a class="btn btn-primary waves-effect waves-light" href="{{ env('APP_URL')) }}" >Fermer la fenetre</a>
            </div>
        </div>



        <!-- jQuery  -->
        <script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

        <script src="/vendor/xadmino/Admin/assets/js/app.js"></script>

    </body>
</html>