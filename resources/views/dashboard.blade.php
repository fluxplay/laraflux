@extends('layouts.base')

@section('header')
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

    <!-- DataTables -->
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">

<script>


function toolTipContent(e) {
	var str = "";
	var total = 0;
	var str2, str3;
	for (var i = 0; i < e.entries.length; i++){
		var  str1 = "<span style= \"color:"+e.entries[i].dataSeries.color + "\"> "+e.entries[i].dataSeries.name+"</span>: $<strong>"+e.entries[i].dataPoint.y+"</strong>bn<br/>";
		total = e.entries[i].dataPoint.y + total;
		str = str.concat(str1);
	}
	str2 = "<span style = \"color:DodgerBlue;\"><strong>"+(e.entries[0].dataPoint.x).getFullYear()+"</strong></span><br/>";
	total = Math.round(total * 100) / 100;
	str3 = "<span style = \"color:Tomato\">Total:</span><strong> $"+total+"</strong>bn<br/>";
	return (str2.concat(str)).concat(str3);
}


window.onload = function() 
    {

    var menu = document.getElementById("menu-dashboard");
    menu.className = menu.className + ' ' + 'active' ;

    var client_top5 = new CanvasJS.Chart("client_top5", {
	theme: "dark2", // "light1", "light2", "dark1", "dark2"
	exportEnabled: false,
	animationEnabled: true,
	{{-- title: {
		text: "Nombre de prestation pour les 5 premiers clients"
	}, --}}
	data: [{
		type: "pie",
		startAngle: 25,
		toolTipContent: "<b>{label}</b>: {y}",
		showInLegend: false,
		legendText: "{label}",
		indexLabelFontSize: 12,
		indexLabel: "{label} - {y}",
		dataPoints: {!! $client_top5 !!}
        }]
    });

    var artisan_top5 = new CanvasJS.Chart("artisan_top5", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        exportEnabled: false,
        animationEnabled: true,
        {{-- title: {
            text: "Nombre de prestation pour les 5 premiers artisans"
        }, --}}
        data: [{
            type: "doughnut",
            startAngle: 25,
            toolTipContent: "<b>{label}</b>: {y}",
            showInLegend: false,
            legendText: "{label}",
            indexLabelFontSize: 12,
            indexLabel: "{label} - {y}",
            dataPoints: {!! $artisan_top5 !!}
        }]
    });


    var type_travaux_annee = new CanvasJS.Chart("type_travaux_annee", {
        theme: "light2", // "light2", "dark1", "dark2"
        animationEnabled: true, // change to true		
        {{-- title:{
            text: "Nombre de prestation par type de travaux pour cette annee",
            fontSize: 18,
        }, --}}
        data: [
        {
            // Change type to "bar", "area", "spline", "pie",etc.
            type: "column",
            dataPoints: {!! $data_type_travaux_annee !!}
        }]
    });

var type_travaux_mois = new CanvasJS.Chart("type_travaux_mois", {
	theme: "dark2", // "light2", "dark1", "dark2"
	animationEnabled: true, // change to true		
	{{-- title:{
        text: "Nombre de prestation par type de travaux pour ce mois",
        fontSize: 18,
	}, --}}
	data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "column",
        dataPoints: {!! $data_type_travaux_mois !!}
	}]
});

var type_travaux_tot = new CanvasJS.Chart("type_travaux_tot", {
    animationEnabled: true,
    theme: "dark2", // "light1", "light2", "dark1", "dark2"
	title:{
		{{-- text: "Nombre de prestation part type de travaux pour les 5 dernières annees", --}}
        fontFamily: "arial black",
        fontSize: 22,
	},

	toolTip: {
		shared: true,
		content: toolTipContent
	},
	data: [
            {
                type: "stackedColumn100",
                showInLegend: true,
                name: "nettoyage",
                dataPoints: [{"label":"2014","y":892},{"label":"2015","y":1032},{"label":"2016","y":881},{"label":"2017","y":908},{"label":"2018","y":920}]
            },
            {
                type: "stackedColumn100",
                showInLegend: true,
                name: "jardinage",
                dataPoints: [{"label":"2014","y":896},{"label":"2015","y":829},{"label":"2016","y":599},{"label":"2017","y":583},{"label":"2018","y":539}]
            },
                    {        
                type: "stackedColumn100",
                showInLegend: true,
                name: "informatique",
                dataPoints: [{"label":"2014","y":603},{"label":"2015","y":501},{"label":"2016","y":469},{"label":"2017","y":469},{"label":"2018","y":404}]
            },
                    {        
                type: "stackedColumn100",
                showInLegend: true,
                name: "administratif",
                dataPoints: [{"label":"2014","y":415},{"label":"2015","y":429},{"label":"2016","y":337},{"label":"2017","y":354},{"label":"2018","y":302}]
            },
                    {        
                type: "stackedColumn100",
                showInLegend: true,
                name: "bricolage",
                dataPoints: [{"label":"2014","y":2},{"label":"2015","y":5},{"label":"2016","y":3},{"label":"2017","y":9},{"label":"2018","y":16}]
            },
                    {        
                type: "stackedColumn100",
                showInLegend: true,
                name: "cours",
                dataPoints: [{"label":"2014","y":0},{"label":"2015","y":0},{"label":"2016","y":1},{"label":"2017","y":0},{"label":"2018","y":0}]
            },
                {}]
        {{-- @foreach($data_type_travaux_tot as $type) 
            {        
                {{ $type }}
            },
        @endforeach --}}
});

client_top5.render();
artisan_top5.render();
type_travaux_annee.render();
type_travaux_mois.render();
type_travaux_tot.render();
}
</script>

@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-6 col-lg-3">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Total Client actif</h4>
            </div>
            <div class="panel-body">
                <h3 class=""><b>{{ $nb_client_actif }}</b></h3>
                <p class="text-muted"><b>48%</b> du total des clients</p>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Total Artisan actif</h4>
            </div>
            <div class="panel-body">
                <h3 class=""><b>{{ $nb_artisan_actif }}</b></h3>
                <p class="text-muted"><b>15%</b> du total des artisans</p>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Total des utilisateurs</h4>
            </div>
            <div class="panel-body">
                <h3 class=""><b>{{ $nb_user_tot }}</b></h3>
                <p class="text-muted"><b>65%</b> From Last 24 Hours</p>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-lg-3">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Total des factures du mois</h4>
            </div>
            <div class="panel-body">
                <h3 class=""><b>$2779.7</b></h3>
                <p class="text-muted"><b>31%</b> From Last 1 month</p>
            </div>
        </div>
    </div>
</div>
<!-- end --->
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Nombre de prestation par client.</h3>
            </div>
            <div class="panel-body"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                <div id="client_top5" style="height: 300px; width: 100%;"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Nombre de prestation par artisan.</h3>
            </div>
            <div class="panel-body"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                <div id="artisan_top5" style="height: 300px; width: 100%;"></div>
            </div>
        </div>
    </div>
</div> 
<!-- End Row -->
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Nombre de prestation par type de travaux cette annee.</h3>
            </div>
            <div class="panel-body"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                <div id="type_travaux_annee" style="height: 300px; width: 100%;"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Nombre de prestation par tyle de travaux pour ce mois.</h3>
            </div>
            <div class="panel-body"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                    <div id="type_travaux_mois" style="height: 300px; width: 100%;"></div>
            </div>
        </div>
    </div>
</div> 
<!-- End Row -->
<div class="row">
    <div class="col">
        <div class="panel panel-border panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Nombre de prestation part type de travaux pour les 5 dernières annees</h3>
            </div>
            <div class="panel-body"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                <div id="type_travaux_tot" style="height: 300px; width: 100%;"></div>
            </div>
        </div>
    </div>
</div> 
<!-- End Row -->

        </div> <!-- container -->
    </div> <!-- content -->
    <footer class="footer">
        2016 - 2020 © FluXplaY.
    </footer>

</div>
@endsection


@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.init.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>

@endsection