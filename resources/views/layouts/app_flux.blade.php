<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8" />
        <title>FluXplay - Outil pour les artisans du web</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        @yield('favico')

        @yield('header')

    </head>

    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            @yield('topbar')
            <!-- ========== Left Sidebar Start ========== -->
            @yield('sidemenu')
            <!-- ========== Left Sidebar Start ========== -->
            @yield('content')
        </div>
        <!-- END wrapper -->

        @yield('footer')

    </body>
</html>