@extends('layouts.app_flux')

@section('favico')
    <link rel="shortcut icon" href="/vendor/xadmino/Admin/assets/images/logo-upcv.jpg">
@endsection


@section('sidemenu')
<div class="left side-menu">
<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 79px;"><div class="sidebar-inner slimscrollleft" style="overflow: hidden; width: auto; height: 79px;">
    <div class="user-details">
        <div class="text-center">
            <img src="/vendor/xadmino/Admin/assets/images/users/avatar-7.jpg" alt="" class="img-circle">
        </div>
        <div class="user-info">
            <div class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">{{ Auth::user()->name }}</a>
                <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)"><i class="mdi mdi-face"></i>Profile</a></li>
                    <li><a href="javascript:void(0)"><i class="ti-settings"></i>Paramettres</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Deconnexion</a></li>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>

                </ul>
            </div>
            @if (Auth::user()->getType()=="adm")
                        <p class="text-muted m-0"><i class="mdi mdi mdi-information text-primary"></i><i> Administrateur</i></p>
            @elseif (Auth::user()->getType()=="artisan")
                        <p class="text-muted m-0"><i class="mdi mdi mdi-information text-primary"></i><i> Compte Artisan</i></p>
            @else
                        <p class="text-muted m-0"><i class="mdi mdi mdi-information text-primary"></i> Compte Client</p>
            @endif
        </div>
    </div>
    <!--- Divider -->
    <div id="sidebar-menu">
        <ul>
    @if (Auth::user()->getType()=="adm")
        <li><a id="menu-dashboard" href="/public/home" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Dashboard </span></a></li>
            <li><a id="menu-prestations" href="/public/prestations" class="waves-effect"><i class="mdi mdi-shape-rectangle-plus"></i><span> Prestations </span></a></li>
            <li><a id="menu-client" href="/public/clients" class="waves-effect"><i class="ion-android-contact"></i><span> Clients </span></a></li>
            <li><a id="menu-artisan" href="/public/artisans" class="waves-effect"><i class="ion-ios7-home"></i><span> Entreprises </span></a></li>
            {{-- <li><a id="menu-cfacture" href="/public/cfactures" class="waves-effect"><i class="ti-write"></i><span> Facture Client </span></a></li>
            <li><a id="menu-afacture" href="/public/afactures" class="waves-effect"><i class="ti-write"></i><span> Facture Artisan </span></a></li> --}}
            <li><a id="menu-recapitulatif" href="/public/recapitulatif" class="waves-effect"><i class="ti-menu-alt"></i><span> Recapitulatif </span></a></li>
            <li><a id="menu-attestation" href="/public/attestations" class="waves-effect"><i class="mdi mdi-certificate"></i><span> Attestations </span></a></li>
            <li><a id="menu-statistique" href="/public/statistiques" class="waves-effect"><i class="ti-pie-chart"></i><span> Statistiques </span></a></li>
            <li><a id="menu-messagerie" href="/public/messagerie" class="waves-effect"><i class="ti-calendar"></i><span> Messagerie <span class="badge badge-primary pull-right">6</span></span></a></li>
    @elseif (Auth::user()->getType()=="artisan")
        <li><a id="menu-dashboard" href="/public/home" class="waves-effect"><i class="ti-home"></i><span> Dashboard </span></a></li>
            <li><a id="menu-recapitulatif" href="/public/recapitulatif" class="waves-effect"><i class="ti-menu-alt"></i><span> Recapitulatif </span></a></li>
            <li><a id="menu-attestation" href="/public/attestations" class="waves-effect"><i class="mdi mdi-certificate"></i><span> Attestations </span></a></li>
            <li><a id="menu-messagerie" href="/public/messagerie" class="waves-effect"><i class="ti-calendar"></i><span> Messagerie <span class="badge badge-primary pull-right">6</span></span></a></li>
    @else 
            <li><a id="menu-dashboard" href="/public/home" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span> Dashboard </span></a></li>
            <li><a id="menu-facture" href="/public/factures" class="waves-effect"><i class="ion-clipboard"></i><span> Factures </span></a></li>
            <li><a id="menu-attestation" href="/public/attestations" class="waves-effect"><i class="mdi mdi-certificate"></i><span> Attestations </span></a></li>
            <li><a id="menu-messagerie" href="/public/messagerie" class="waves-effect"><i class="ti-calendar"></i><span> Messagerie <span class="badge badge-primary pull-right">6</span></span></a></li>
    @endif
        </ul>
    </div>
    <div class="clearfix"></div>
</div><div class="slimScrollBar" style="background: rgb(122, 134, 143); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 30px; visibility: visible;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div> <!-- end sidebarinner -->
</div>
@endsection

@section('topbar')
<div class="topbar">
<!-- LOGO -->
<div class="topbar-left">
    <div class="text-center">
        <a href="https://docs.google.com/spreadsheets/d/1gq4xGrapCxHwfrX3tOmHM-ftBLTaLz-6wdU2y4SKXmM/edit#gid=0" class="logo">
          @include('subviews.logolefttop')
    </div>
</div>
<!-- Button mobile view to collapse sidebar menu -->
<div class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="">
            <div class="pull-left">
                <button type="button" class="button-menu-mobile open-left waves-effect waves-light">
                    <i class="ion-navicon"></i>
                </button>
                <span class="clearfix"></span>
            </div>
            <form class="navbar-form pull-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control search-bar" placeholder="Recherche...">
                </div>
                <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
            </form>

            <ul class="nav navbar-nav navbar-right pull-right">
                <li class="dropdown hidden-xs">
                    <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                        <i class="fa fa-bell"></i> <span class="badge badge-xs badge-primary">3</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-lg">
                        <li class="text-center notifi-title">Notification <span class="badge badge-xs badge-primary">3</span></li>
                        <li class="list-group">
                            <!-- list item-->
                            <a href="javascript:void(0);" class="list-group-item">
                                <div class="media">
                                    <div class="media-heading">Your order is placed</div>
                                    <p class="m-0">
                                    <small>Dummy text of the printing and typesetting industry.</small>
                                    </p>
                                </div>
                            </a>
                            <!-- list item-->
                            <a href="javascript:void(0);" class="list-group-item">
                                <div class="media">
                                    <div class="media-body clearfix">
                                    <div class="media-heading">New Message received</div>
                                    <p class="m-0">
                                        <small>You have 87 unread messages</small>
                                    </p>
                                    </div>
                                </div>
                            </a>
                            <!-- list item-->
                            <a href="javascript:void(0);" class="list-group-item">
                                <div class="media">
                                    <div class="media-body clearfix">
                                    <div class="media-heading">Your item is shipped.</div>
                                    <p class="m-0">
                                        <small>It is a long established fact that a reader will</small>
                                    </p>
                                    </div>
                                </div>
                            </a>
                            <!-- last list item -->
                            <a href="javascript:void(0);" class="list-group-item">
                                <small class="text-primary">See all notifications</small>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="/vendor/xadmino/Admin/assets/images/users/avatar-7.jpg" alt="user-img" class="img-circle"> </a>
                    <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)"><i class="mdi mdi-face"></i>  Profile</a></li>
                    <li><a href="javascript:void(0)"><i class="ti-settings"></i> Paramettres</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Deconnexion</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>
</div>
@endsection