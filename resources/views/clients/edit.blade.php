@extends('layouts.base')
 
 {{-- Using for static function --}}
 <?php use App\Client; ?>

@section('header')
{{-- algolia --}}
<script src="https://cdn.jsdelivr.net/npm/places.js@1.19.0"></script>
<script>
window.onload = function() {
    var menu = document.getElementById("menu-client");
    menu.className = menu.className + ' ' + 'active';

        var placesAutocomplete = places({
    appId: 'plFWZL4VH8V4',
    apiKey: '96c620770dd1cfa1a062fec065241049',
    container: document.getElementById('sz_adresse1'),
    templates: {
        value: function(suggestion) {
            return suggestion.name;
        }
}
     }).configure({
        type: 'address',
        countries: ['fr']
  });

  placesAutocomplete.on('change', function resultSelected(e) {
    document.getElementById('sz_adresse1').value = e.suggestion.administrative || '';
    document.getElementById('sz_ville').value = e.suggestion.city || '';
    document.getElementById('sz_cp').value = e.suggestion.postcode || '';
  });
};
</script>
<style>
.uper {
    margin-top: 40px;
}
</style>
<!-- DataTables -->
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
    type="text/css" />
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet"
    type="text/css" />

<link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">
@endsection

@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.init.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header-title">
                        <h4 class="pull-left page-title">Bienvenue sur la page des clients</h4>
                        <ol class="breadcrumb pull-right">
                            <li><a class="btn-ajout" href="/public/clients">+ Retour á la liste des clients</a></li>
                            <li class="active">FluXplaY</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Mise à jour d'un client</h3>
                    </div>
                    <div class="panel-body">
                    
                    {{-- form status row  --}}
                    @include('subviews.formstatus')

                        <form method="post" class="form-horizontal" action="{{ route('clients.update', $client->id_client) }}">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_civilite">Civilité </label>
                                <div class="col-md-10">
                                <select class="form-control" name="se_civilite" id="se_civilite">
                                    @foreach(Client::getCiviliteOptions() as $key => $value)
                                    <option value="{{ $key }}" {{ $client->civilite == $key ? 'selected' : '' }}>
                                        {{ $value }}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="nom">Nom:</label>
                                <div class="col-md-10">
                                <input type="text" class="form-control" name="sz_nom" value="{{ $client->nom }}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="prenom">Prenom :</label>
                                <div class="col-md-10">
                                <input type="text" class="form-control" name="sz_prenom"
                                    value="{{ $client->prenom }}" />
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="ville">Adresse :</label>
                                <div class="col-md-10">
                                    <input type="search" id="sz_adresse1" name="sz_adresse1" class="form-control" value="{{ $client->adresse1 }}" />
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="adresse2">Complement :</label>
                                <div class="col-md-10">
                                <input type="text" class="form-control" name="sz_adresse2"
                                    value="{{ $client->adresse2 }}" />
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="ville">Ville :</label>
                                <div class="col-md-10">
                                <input type="text" class="form-control" id="sz_ville" name="sz_ville" value="{{ $client->ville }}" />
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="cp">CP :</label>
                                <div class="col-md-10">
                                <input type="text" class="form-control" id="sz_cp" name="sz_cp" value="{{ $client->cp }}" />
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="telephone">Téléphone :</label>
                                <div class="col-md-10">
                                <input type="text" class="form-control" name="sz_telephone"
                                    value="{{ $client->telephone }}" />
                                    </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="email">Email :</label>
                                <div class="col-md-10">
                                <input type="text" class="form-control" name="sz_email" value="{{ $client->email }}" />
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label class="col-md-2 control-label" for="se_livret">Livret d'accueil </label>
                                <div class="col-md-10">
                                <select class="form-control" name="se_livret" id="se_livret" value={{ $client->livret }}>
                                    @foreach(Client::getOuiNonOptions() as $key => $value)
                                    <option value="{{ $key }}" {{ $client->livret == $key ? 'selected' : '' }}>
                                        {{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_fpapier">Facture papier </label>
                                <div class="col-md-10">
                                <select class="form-control" name="se_fpapier" id="se_fpapier" value={{ $client->fpapier }}>
                                    @foreach(Client::getOuiNonOptions() as $key => $value)
                                    <option value="{{ $key }}" {{ $client->fpapier == $key ? 'selected' : '' }}>
                                        {{ $value }}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_efacture">Facture email </label>
                                <div class="col-md-10">
                                <select class="form-control" name="se_efacture" id="se_efacture" value={{ $client->b_efacture }}>
                                    @foreach(Client::getOuiNonOptions() as $key => $value)
                                    <option value="{{ $key }}" {{ $client->b_efacture == $key ? 'selected' : '' }}>
                                        {{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_status">Status </label>
                                <div class="col-md-10">
                                <select class="form-control" name="se_status" id="se_status" value={{ $client->actif }}>
                                    @foreach(Client::getStatusOptions() as $key => $value)
                                    <option value="{{ $key }}" {{ $client->actif == $key ? 'selected' : '' }}>
                                        {{ $value }}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div> --}}
                            <button type="submit" class="btn btn-primary">Mise à jour d'un client</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div> <!-- container -->
</div> <!-- content -->
<footer class="footer">
    2016 - 2020 © FluXplaY.
</footer>

</div>
@endsection