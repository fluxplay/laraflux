@extends('layouts.base')

@section('header')
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

    <!-- DataTables -->
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">

<script>
window.onload = function() 
{
    var menu = document.getElementById("menu-dashboard");
    menu.className = menu.className + ' ' + 'active' ;
}
</script>
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

<!-- body -->
<!-- 1er ligne -->
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Nb total de facture</h4>
            </div>
            <div class="panel-body">
                <h2 class=""><b>25</b></h2>
                <p class="text-muted"><b>25</b> facture depuis le debut de l'inscription.</p>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Nb facture cette année</h4>
            </div>
            <div class="panel-body">
                <h2 class=""><b>12</b></h2>
                <p class="text-muted"><b>12</b> factures depuis janvier cette année.</p>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Montant de votre derniere facture</h4>
            </div>
            <div class="panel-body">
                <h3 class=""><b>2779.7 €</b></h3>
                <p class="text-muted">Facture de <b>jardinage</b> editer le 05/08/2020</p>
            </div>
        </div>
    </div>
</div>

<!-- 2eme ligne -->
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-fill panel-upcv">
            <div class="panel-heading">
                <h3 class="panel-title">Consulter vos Factures</h3>
            </div>
            <div class="panel-body">
                <p>Consulter en tout temps vos factures des années precedantes. </p>
                <a href="/public/factures" class="btn btn-dark btn-block waves-effect waves-light" role="button">Mes factures</a>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-fill panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Consulter vos Attestations fiscales</h3>
            </div>
            <div class="panel-body">
                <p>Consulter en tout temps vos factures des années precedantes. </p>
                <a href="/public/attestation" class="btn btn-dark btn-block waves-effect waves-light" role="button">Mes attestations</a>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-fill panel-upcv-light">
            <div class="panel-heading">
            <h3 class="panel-title">Consulter le récapitulatif</h3>
            </div>
            <div class="panel-body">
                <p>Consulter en tout temps vos factures des années precedantes. </p>
                <a href="/public/attestation" class="btn btn-dark btn-block waves-effect waves-light" role="button">Mon récapitulatif</a>
            </div>
        </div>
    </div>
</div>

        </div> <!-- container -->
    </div> <!-- content -->
    <footer class="footer">
        2016 - 2020 © FluXplaY.
    </footer>

</div>
@endsection


@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.init.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>

@endsection