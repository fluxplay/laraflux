@extends('layouts.base')

@section('header')
{{-- algolia --}}
<script src="https://cdn.jsdelivr.net/npm/places.js@1.19.0"></script>
<script>
window.onload = function() {
    var menu = document.getElementById("menu-client");
    menu.className = menu.className + ' ' + 'active';

    var placesAutocomplete = places({
    appId: 'plFWZL4VH8V4',
    apiKey: '96c620770dd1cfa1a062fec065241049',
    container: document.getElementById('sz_adresse1'),
    templates: {
        value: function(suggestion) {
            return suggestion.name;
        }
}
     }).configure({
        type: 'address',
        countries: ['fr']
  });

  placesAutocomplete.on('change', function resultSelected(e) {
    document.getElementById('sz_adresse1').value = e.suggestion.administrative || '';
    document.getElementById('sz_ville').value = e.suggestion.city || '';
    document.getElementById('sz_cp').value = e.suggestion.postcode || '';
  });

};
</script>
<style>
.uper {
    margin-top: 40px;
}
</style>
<!-- DataTables -->
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
    type="text/css" />
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet"
    type="text/css" />

<link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">
@endsection

@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.init.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>
@endsection

@section('content')


<input type="search" id="address-input" placeholder="Where are we going?" />



<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header-title">
                        <h4 class="pull-left page-title">Bienvenue sur la page des clients</h4>
                        <ol class="breadcrumb pull-right">
                            <li><a href="/public/clients">Retour á la liste des clients</a></li>
                            <li class="active">FluXplaY</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Creer un nouveau client</h3>
                    </div>
                    
                    {{-- form status row  --}}
                    @include('subviews.formstatus')

                        <form method="post" class="form-horizontal" action="{{ route('clients.store') }}">
                        {{ csrf_field() }}
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="se_civilite">Civilité </label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="se_civilite" id="se_civilite">
                                            <option value="M">M.</option>
                                            <option value="Mlle">Mlle</option>
                                            <option value="Mme">Mme</option>
                                            <option value="MetMme">M et Mme</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sz_nom">Nom:</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="sz_nom" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sz_prenom">Prenom :</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="sz_prenom" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sz_adresse1">Adresse :</label>
                                    <div class="col-md-10">
                                        <input type="search" id="sz_adresse1" name="sz_adresse1" class="form-control" placeholder="Quel est votre adresse?" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sz_adresse2">Complement :</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="sz_adresse2" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sz_ville">Ville:</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="sz_ville" name="sz_ville" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sz_cp">CP:</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" id="sz_cp" name="sz_cp" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sz_telephone">Telephone :</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="sz_telephone" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sz_email">Email:</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="sz_email" />
                                    </div>
                                </div>
                                {{-- <div class="form-group">
                                    <label class="col-md-2 control-label" for="se_livret">Livret d'accueil </label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="se_livret" id="se_livret">
                                            @foreach($client->getOuiNonOptions() as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="se_fpapier">Facture papier </label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="se_fpapier" id="se_fpapier">
                                            @foreach($client->getOuiNonOptions() as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="se_efacture">Facture email </label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="se_efacture" id="se_efacture"
                                            value={{ $client->b_efacture }}>
                                            @foreach($client->getOuiNonOptions() as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="se_status">Status </label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="se_status" id="se_status"
                                            value={{ $client->actif }}>
                                            @foreach($client->getStatusOptions() as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> --}}
                                <button type="submit" class="btn btn-primary">Creér un nouveau client</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div> <!-- container -->
</div> <!-- content -->
<footer class="footer">
    2010 - 2020 © FluXplaY.
</footer>

</div>
@endsection