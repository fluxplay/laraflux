@extends('layouts.base')

@section('header')
    <!-- DataTables -->
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">

<script>
window.onload = function() 
{
    var menu = document.getElementById("menu-facture-c");
    menu.className = menu.className + ' ' + 'active' ;
}
</script>
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

                    <!-- body -->

<div class="row">
    <div class="col-sm-6 col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Date de la derniere facture</h4>
            </div>
            <div class="panel-body">
                <h3 class=""><b>01/01/2020</b></h3>
                <p class="text-muted"><b>48%</b> du total des clients</p>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Total Artisan actif</h4>
            </div>
            <div class="panel-body">
                <h3 class=""><b>01/01/2020</b></h3>
                <p class="text-muted"><b>15%</b> du total des artisans</p>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Total des utilisateurs</h4>
            </div>
            <div class="panel-body">
            <h3 class=""><b>01/01/2020</b></h3>
                <p class="text-muted"><b>65%</b> From Last 24 Hours</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des factures</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-responsive"
                    class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                    width="100%">
                    <thead>
                        <tr>
                            <td>N° Facture</td>
                            <td>Client</td>
                            <td>Montant</td>
                            <td>Date</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($factures as $facture)
                        <tr>
                            <td><a href="/pdf/{{$facture->id_facture}}"><b>{{$facture->id_facture}}</b></a></td>
                            <td>{{$facture->client->nom .' '.$facture->client->prenom}}</td>
                            <td>{{$facture->montantTTC}} €</td>
                            <td>{{$facture->dateFr()}}</td>
                            <td align='center'><button href="#modalAdminDelPres" class="btn btn-primary clickable btnDelPres"  data-toggle="modal" data-idpres="">
                                    {{-- <span class="glyphicon glyphicon-print"></span> --}}
                                    Imprimer
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->

        </div> <!-- container -->
    </div> <!-- content -->
    <footer class="footer">
        2016 - 2020 © FluXplaY.
    </footer>

</div>
@endsection


@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.init.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>

@endsection