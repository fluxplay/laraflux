@extends('auth.base')

@section('content')
    <!-- Begin page -->
    <div class="accountbg"></div>
    <div class="wrapper-page">
        <div class="panel panel-color panel-primary panel-pages">

            <div class="panel-body">
                <h3 class="text-center m-t-0 m-b-30">
                @include('subviews.logolefttop') 
                </h3>
                <h4 class="text-muted text-center m-t-0"><b>S'identifier</b></h4>

                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input id="email" type="email" class="form-control" placeholder="Votre Email" name="email" value="{{ old('email') }}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-primary">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="checkbox-signup">
                                    Se souvenir de moi
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">S'identifier</button>
                        </div>
                    </div>

                    <div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-7">
                            <a class="text-muted" href="{{ route('password.request') }}">
                                Mot de passe oublié?
                            </a>                            </div>
                        {{-- <div class="col-sm-5 text-right">
                            <a href="pages-register.html" class="text-muted">Creer un compte?</a>
                        </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('logo.left.top')
    <span class=""><img src="/vendor/xadmino/Admin/assets/images/logo-upcv.jpg" alt="logo" height="52">UPCV</span>
@endsection