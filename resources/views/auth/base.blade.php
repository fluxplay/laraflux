<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>UPCV</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Admin Dashboard" name="description" />
        <meta content="ThemeDesign" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/vendor/xadmino/Admin/assets/images/logo-upcv.jpg">

        <link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
        <link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">

<style>
    {{-- logo --}}
    img{
        vertical-align: text-bottom;
    }
</style>
    </head>
    <body>
        @yield('content')
        <!-- jQuery  -->
        <script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>
        <script src="/vendor/xadmino/Admin/assets/js/app.js"></script>
    </body>
</html>