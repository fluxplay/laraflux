
<!DOCTYPE html>

<html lang="fr-FR">

<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="http://www.unprochezvous.com/xmlrpc.php">

<title>UN PRO CHEZ VOUS &#8211; Réseaux de coopératives agréées services à la personne</title>
<link rel='dns-prefetch' href='//secure.gravatar.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="UN PRO CHEZ VOUS &raquo; Flux" href="http://www.unprochezvous.com/index.php/feed/" />
<link rel="alternate" type="application/rss+xml" title="UN PRO CHEZ VOUS &raquo; Flux des commentaires" href="http://www.unprochezvous.com/index.php/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.unprochezvous.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.1"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wplc-admin-style-emoji-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/vendor/wdt-emoji/wdt-emoji-bundle.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-user-pro-styles-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/wplc_styles_pro.css?ver=5.4.1' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='http://www.unprochezvous.com/wp-includes/css/dist/block-library/style.min.css?ver=5.4.1' type='text/css' media='all' />
<style id='wp-block-library-inline-css' type='text/css'>
.has-text-align-justify{text-align:justify;}
</style>
<link rel='stylesheet' id='font-awesome-5-css'  href='http://www.unprochezvous.com/wp-content/plugins/themeisle-companion/obfx_modules/gutenberg-blocks/assets/fontawesome/css/all.min.css?ver=2.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-4-shims-css'  href='http://www.unprochezvous.com/wp-content/plugins/themeisle-companion/obfx_modules/gutenberg-blocks/assets/fontawesome/css/v4-shims.min.css?ver=2.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='themeisle-block_styles-css'  href='http://www.unprochezvous.com/wp-content/plugins/themeisle-companion/vendor/codeinwp/gutenberg-blocks/build/style.css?ver=1.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-font-awesome-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/fontawesome-all.min.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-style-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/wplcstyle.css?ver=8.1.7' type='text/css' media='all' />
<style id='wplc-style-inline-css' type='text/css'>
#wp-live-chat-header { background:url('http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/images/chaticon.png') no-repeat; background-size: cover; }  #wp-live-chat-header.active { background:url('http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/images/iconCloseRetina.png') no-repeat; background-size: cover; } #wp-live-chat-4 { background:url('http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/images/bg/cloudy.jpg') repeat; background-size: cover; }
</style>
<link rel='stylesheet' id='wplc-theme-palette-default-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/themes/theme-default.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-theme-modern-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/themes/modern.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-theme-position-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/themes/position-bottom-right.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-gutenberg-template-styles-user-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/includes/blocks/wplc-chat-box/wplc_gutenberg_template_styles.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_font-css'  href='//fonts.googleapis.com/css?family=Lato%3A300%2C400%2C700%2C400italic%7CMontserrat%3A400%2C700%7CHomemade+Apple&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_font_all-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;subset=latin&#038;ver=5.4.1' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_bootstrap_style-css'  href='http://www.unprochezvous.com/wp-content/themes/zerif-lite/css/bootstrap.css?ver=5.4.1' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_fontawesome-css'  href='http://www.unprochezvous.com/wp-content/themes/zerif-lite/css/font-awesome.min.css?ver=v1' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_style-css'  href='http://www.unprochezvous.com/wp-content/themes/zerif-lite/style.css?ver=v1' type='text/css' media='all' />
<style id='zerif_style-inline-css' type='text/css'>

		.page-template-builder-fullwidth {
			overflow: hidden;
		}
		@media (min-width: 768px) {
			.page-template-builder-fullwidth-std .header > .elementor {
				padding-top: 76px;
			}
		}

</style>
<link rel='stylesheet' id='zerif_responsive_style-css'  href='http://www.unprochezvous.com/wp-content/themes/zerif-lite/css/responsive.css?ver=v1' type='text/css' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='zerif_ie_style-css'  href='http://www.unprochezvous.com/wp-content/themes/zerif-lite/css/ie.css?ver=v1' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='jetpack_css-css'  href='http://www.unprochezvous.com/wp-content/plugins/jetpack/css/jetpack.css?ver=8.3' type='text/css' media='all' />
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wplc_datetime_format = {"date_format":"j F Y","time_format":"H:i"};
var tcx_api_key = "cfca92a36c8cb2497d24393ee67a7158";
var wplc_guid = "6f58628858d9d93401be5e85c8ad00bff2523e4e";
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_server.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/vendor/wdt-emoji/wdt-emoji-concat.min.js?ver=8.1.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pf = {"spam":{"label":"Je suis humain !","value":"851b7a7e5b"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/pirate-forms/public/js/custom-spam.js?ver=5.4.1'></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/html5.js?ver=5.4.1'></script>
<![endif]-->
<link rel='https://api.w.org/' href='http://www.unprochezvous.com/index.php/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.unprochezvous.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.unprochezvous.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.4.1" />
<link rel="canonical" href="http://www.unprochezvous.com/" />
<link rel='shortlink' href='https://wp.me/P9q7Vy-1s' />
<link rel="alternate" type="application/json+oembed" href="http://www.unprochezvous.com/index.php/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.unprochezvous.com%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://www.unprochezvous.com/index.php/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.unprochezvous.com%2F&#038;format=xml" />
  <script type="text/javascript">
    var wplc_ajaxurl = 'http://www.unprochezvous.com/wp-admin/admin-ajax.php';
    var wplc_nonce = '1555f8bc35';
  </script>
  
<link rel='dns-prefetch' href='//v0.wordpress.com'/>
<link rel='dns-prefetch' href='//widgets.wp.com'/>
<link rel='dns-prefetch' href='//s0.wp.com'/>
<link rel='dns-prefetch' href='//0.gravatar.com'/>
<link rel='dns-prefetch' href='//1.gravatar.com'/>
<link rel='dns-prefetch' href='//2.gravatar.com'/>
<link rel='dns-prefetch' href='//i0.wp.com'/>
<link rel='dns-prefetch' href='//i1.wp.com'/>
<link rel='dns-prefetch' href='//i2.wp.com'/>
<style type='text/css'>img#wpstats{display:none}</style><style type="text/css" id="custom-background-css">
body.custom-background { background-image: url("http://www.unprochezvous.com/wp-content/uploads/2017/11/photo-famille-site-2.jpg"); background-position: left top; background-size: cover; background-repeat: no-repeat; background-attachment: fixed; }
</style>
	
<!-- Jetpack Open Graph Tags -->
<meta property="og:type" content="website" />
<meta property="og:title" content="UN PRO CHEZ VOUS" />
<meta property="og:description" content="Réseaux de coopératives agréées services à la personne" />
<meta property="og:url" content="http://www.unprochezvous.com/" />
<meta property="og:site_name" content="UN PRO CHEZ VOUS" />
<meta property="og:image" content="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1.jpg?fit=512%2C512" />
<meta property="og:image:width" content="512" />
<meta property="og:image:height" content="512" />
<meta property="og:locale" content="fr_FR" />
<meta name="twitter:text:title" content="Acceuil" />
<meta name="twitter:image" content="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1.jpg?fit=240%2C240" />
<meta name="twitter:card" content="summary" />

<!-- End Jetpack Open Graph Tags -->
<link rel="icon" href="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1.jpg?fit=32%2C32" sizes="32x32" />
<link rel="icon" href="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1.jpg?fit=192%2C192" sizes="192x192" />
<link rel="apple-touch-icon" href="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1.jpg?fit=180%2C180" />
<meta name="msapplication-TileImage" content="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1.jpg?fit=270%2C270" />

</head>


	<body class="home page-template-default page page-id-90 custom-background wp-custom-logo" >

<div class="preloader"><div class="status">&nbsp;</div></div>

<div id="mobilebgfix">
	<div class="mobile-bg-fix-img-wrap">
		<div class="mobile-bg-fix-img"></div>
	</div>
	<div class="mobile-bg-fix-whole-site">


<header id="home" class="header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">

	<div id="main-nav" class="navbar navbar-inverse bs-docs-nav" role="banner">

		
	@include('upcv.topbar')

		
	</div> <!-- /#main-nav -->
	<!-- / END TOP BAR -->
<div class=" home-header-wrap"><div class="header-content-wrap"><div class="container"><h1 class="intro-text">UN PRO CHEZ VOUS</h1><div class="buttons"><a href="/public/upcv/particuliers" class="btn btn-primary custom-button red-btn">pARTICULIERS</a><a href="/public/upcv/professionnels" class="btn btn-primary custom-button green-btn">Professionnels</a></div></div></div><!-- .header-content-wrap --><div class="clear"></div>
</div>

</header> <!-- / END HOME SECTION  -->
<div id="content" class="site-content">


<section class="focus" id="focus">

	
	<div class="container">

		<!-- SECTION HEADER -->

		<div class="section-header">

			<!-- SECTION TITLE AND SUBTITLE -->

			<h2 class="dark-text">Nos activités agréés </h2>
		</div>

		<div class="row">

				<span id="ctup-ads-widget-1" class="">
			<div class="col-lg-3 col-sm-3 focus-box" data-scrollreveal="enter left after 0.15s over 1s">

										<div class="service-icon" tabindex="0">
							<i class="pixeden"
							   style="background:url(http://www.unprochezvous.com/wp-content/uploads/2017/11/photo-jardinage-300x149.jpg) no-repeat center;width:100%; height:100%;"></i>
							<!-- FOCUS ICON-->
						</div>
						

					
				<h3 class="red-border-bottom">
				TRAVAUX DE JARDINAGE				</h3>
				<!-- FOCUS HEADING -->

				<p>Vous aimeriez faire entretenir votre jardin pour une de vos résidences ?
Un de nos jardiniers paysagistes qualifiés UN PRO CHEZ VOUS s&rsquo;occupe de vos extérieurs.</p>
			</div>

			</span><span id="ctup-ads-widget-2" class="">
			<div class="col-lg-3 col-sm-3 focus-box" data-scrollreveal="enter left after 0.15s over 1s">

										<div class="service-icon" tabindex="0">
							<i class="pixeden"
							   style="background:url(http://www.unprochezvous.com/wp-content/uploads/2017/11/photo-informatique-300x138.jpg) no-repeat center;width:100%; height:100%;"></i>
							<!-- FOCUS ICON-->
						</div>
						

					
				<h3 class="red-border-bottom">
				ASSISTANCE INFORMATIQUE				</h3>
				<!-- FOCUS HEADING -->

				<p>Vous souhaitez être livré de votre achat informatique, que l&rsquo;on se charge de le mettre en service ou y être formé (ordinateur, logiciels etc.).</p>
			</div>

			</span><span id="ctup-ads-widget-3" class="">
			<div class="col-lg-3 col-sm-3 focus-box" data-scrollreveal="enter left after 0.15s over 1s">

										<div class="service-icon" tabindex="0">
							<i class="pixeden"
							   style="background:url(http://www.unprochezvous.com/wp-content/uploads/2017/11/cleanliness-2799470_1920-1-300x200.jpg) no-repeat center;width:100%; height:100%;"></i>
							<!-- FOCUS ICON-->
						</div>
						

					
				<h3 class="red-border-bottom">
				ENTRETIEN ET TRAVAUX MéNAGERS				</h3>
				<!-- FOCUS HEADING -->

				<p>Vous devez faire du nettoyage à votre domicile ou dans votre résidence secondaire, comme le nettoyage de vos sols et vitrages, le grand nettoyage après un déménagement. Une de nos entreprises adhérentes s&rsquo;occupe de vous !</p>
			</div>

			</span><span id="ctup-ads-widget-4" class="">
			<div class="col-lg-3 col-sm-3 focus-box" data-scrollreveal="enter left after 0.15s over 1s">

										<div class="service-icon" tabindex="0">
							<i class="pixeden"
							   style="background:url(http://www.unprochezvous.com/wp-content/uploads/2017/11/photo-repas-à-domicile-300x152.jpg) no-repeat center;width:100%; height:100%;"></i>
							<!-- FOCUS ICON-->
						</div>
						

					
				<h3 class="red-border-bottom">
				REPAS à DOMICILE				</h3>
				<!-- FOCUS HEADING -->

				<p>Vous n&rsquo;avez pas le temps de faire les courses et de préparer vos repas ? Ou tout simplement pour une occasion festive, vous préférez faire appel à un spécialiste culinaire ?
Un de nos artisans traiteur se chargera de vous ravir.</p>
			</div>

			</span><span id="ctup-ads-widget-6" class="">
			<div class="col-lg-3 col-sm-3 focus-box" data-scrollreveal="enter left after 0.15s over 1s">

										<div class="service-icon" tabindex="0">
							<i class="pixeden"
							   style="background:url(http://www.unprochezvous.com/wp-content/uploads/2017/11/electrician-1080554_1920-300x200.jpg) no-repeat center;width:100%; height:100%;"></i>
							<!-- FOCUS ICON-->
						</div>
						

					
				<h3 class="red-border-bottom">
				PETITS TRAVAUX  DE BRICOLAGE				</h3>
				<!-- FOCUS HEADING -->

				<p>Vous avez besoin de déplacer, monter, démonter du mobilier, changer un syphon ou réparer une petite fuite d&rsquo;eau, changer une ampoule etc&#8230; un de nos professionnels du bricolage est à votre disposition !</p>
			</div>

			</span><span id="ctup-ads-widget-8" class="">
			<div class="col-lg-3 col-sm-3 focus-box" data-scrollreveal="enter left after 0.15s over 1s">

										<div class="service-icon" tabindex="0">
							<i class="pixeden"
							   style="background:url(http://www.unprochezvous.com/wp-content/uploads/2017/11/photo-cours-à-domicile-300x159.jpg) no-repeat center;width:100%; height:100%;"></i>
							<!-- FOCUS ICON-->
						</div>
						

					
				<h3 class="red-border-bottom">
				COURS à DOMICILE				</h3>
				<!-- FOCUS HEADING -->

				<p>Petits ou grands, apprenez confortable chez vous, le piano, une langue étrangère, des cours de cuisine,  la couture&#8230;tout est possible ! 
Faites appel aux experts adhérents de nos coopératives.</p>
			</div>

			</span><span id="ctup-ads-widget-9" class="">
			<div class="col-lg-3 col-sm-3 focus-box" data-scrollreveal="enter left after 0.15s over 1s">

										<div class="service-icon" tabindex="0">
							<i class="pixeden"
							   style="background:url(http://www.unprochezvous.com/wp-content/uploads/2018/01/photo-assitance-administrative.jpg) no-repeat center;width:100%; height:100%;"></i>
							<!-- FOCUS ICON-->
						</div>
						

					
				<h3 class="red-border-bottom">
				ASSISTANCE ADMINISTRATIVE				</h3>
				<!-- FOCUS HEADING -->

				<p>Vous aider à remplir un dossier auprès d&rsquo;une administration publique, obtenir un remboursement, ou encore rédiger vos lettres et classer puis archiver vos factures : contactez UN PRO CHEZ VOUS pour gagner du temps dans vos tâches administratives !</p>
			</div>

			</span>
		</div>

	</div> <!-- / END CONTAINER -->

	
</section>  <!-- / END FOCUS SECTION -->

<section class="separator-one" id="ribbon_bottom"><div class="color-overlay"><h3 class="container text" data-scrollreveal="enter left after 0s over 1s">Plus de renseignements sur nos activités et notre fonctionnement ?</h3><div data-scrollreveal="enter right after 0s over 1s"><a href="/public/upcv/particuliers" class="btn btn-primary custom-button green-btn">contactez-nous !</a></div></div></section>
<section class="about-us" id="aboutus">

	
	<div class="container">

		<!-- SECTION HEADER -->

		<div class="section-header">

			<h2 class="white-text">Qui sommes nous ?</h2>
		</div><!-- / END SECTION HEADER -->

		<!-- 3 COLUMNS OF ABOUT US-->

		<div class="row">

			<!-- COLUMN 1 - BIG MESSAGE ABOUT THE COMPANY-->

		<div class="col-lg-6 col-md-6 column zerif_about_us_center text_and_skills" data-scrollreveal="enter bottom after 0s over 1s"><p>Bienvenue sur le site d’Un Pro Chez Vous !

Les coopératives UN PRO CHEZ VOUS sont agréées services à la personne.

La 1ère coopérative a été fondée en 2008.

Les UN PRO CHEZ VOUS s’adressent :

aux entreprises qui désirent proposer l’avantage des 50%* de réductions et/ou crédit d’impôt à leurs clients particuliers,
aux particuliers eux-mêmes pour les diriger vers des entreprises qualifiées pour réaliser des prestations d’entretien de jardin, d’assistance informatique à domicile, etc. tout en bénéficiant de 50%* de réductions et/ou crédit d’impôt.
Pour en savoir plus, contactez-nous par téléphone au 3620 et dites « UN PRO CHEZ VOUS »  ou par email à contact@unprochezvoous.com.

*selon loi de finance en vigueur



 

                                                                                                                                    </p></div><div class="col-lg-6 col-md-6 column zerif-rtl-skills ">

<ul class="skills" data-scrollreveal="enter right after 0s over 1s">

<!-- SKILL ONE -->
<li class="skill skill_1"><div class="skill-count"><input role="presentation" type="text" id="professionnalisme" value="100" data-thickness=".2" class="skill1" tabindex="-1"></div><div class="section-legend"><label for="professionnalisme">Professionnalisme</label></div><p>Des entreprises  adhérentes qualifiées !</p></li>
<!-- SKILL TWO -->

<li class="skill skill_2"><div class="skill-count"><input role="presentation" type="text" id="taux-de-satsisfaction" value="100" data-thickness=".2" class="skill2" tabindex="-1"></div><div class="section-legend"><label for="taux-de-satsisfaction">taux de satsisfaction</label></div><p>Des clients satisfaits depuis 2008 !</p></li>
<!-- SKILL THREE -->

<li class="skill skill_3"><div class="skill-count"><input role="presentation" type="text" id="lequipe" value="100" data-thickness=".2" class="skill3" tabindex="-1"></div><div class="section-legend"><label for="lequipe">l'équipe</label></div><p>Des personnes compétentes à votre écoute !</p></li>
<!-- SKILL FOUR -->

<li class="skill skill_4"><div class="skill-count"><input role="presentation" type="text" id="proximite" value="100" data-thickness=".2" class="skill4" tabindex="-1"></div><div class="section-legend"><label for="proximite">Proximité</label></div><p>des coopératives présentes et à l'écoute de ses adhérents !</p></li>
			</ul>

		</div> <!-- / END SKILLS COLUMN-->

		
	</div> <!-- / END 3 COLUMNS OF ABOUT US-->

	<!-- CLIENTS -->
	
	</div> <!-- / END CONTAINER -->

	
</section> <!-- END ABOUT US SECTION -->

<section class="our-team" id="team"><div class="container"><div class="section-header"><h2 class="dark-text">NOS COOPéRATIVES</h2></div><div class="row" data-scrollreveal="enter left after 0s over 0.1s"><span id="zerif_team-widget-1" class="">
			<div class="col-lg-3 col-sm-3 team-box">

				<div class="team-member" tabindex="0">

					

						<figure class="profile-pic">

							<img src="http://www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-e1511539524799.jpg" alt=""/>

						</figure>
						
					<div class="member-details">

						
							<h3 class="dark-text red-border-bottom">UN PRO CHEZ VOUS 84</h3>

						
						
							<div
								class="position">938 ROUTE DE SORGUES
84320 ENTRAIGUES SUR LA SORGUE</div>

						
					</div>

					<div class="social-icons">

						<ul>
							
							
						</ul>

					</div>

					
				</div>

			</div>

			</span><span id="zerif_team-widget-2" class="">
			<div class="col-lg-3 col-sm-3 team-box">

				<div class="team-member" tabindex="0">

					

						<figure class="profile-pic">

							<img src="http://www.unprochezvous.com/wp-content/uploads/2017/11/cropped-cropped-LOGO-UPCV-MEKICE-1-300x300.jpg" alt=""/>

						</figure>
						
					<div class="member-details">

						
							<h3 class="dark-text red-border-bottom">UN PRO CHEZ VOUS 13</h3>

						
						
							<div
								class="position">ZI LE TUBE &#8211; 2 TRAVERSE GALILEE 13800 ISTRES</div>

						
					</div>

					<div class="social-icons">

						<ul>
							
							
						</ul>

					</div>

					
				</div>

			</div>

			</span><span id="zerif_team-widget-3" class="">
			<div class="col-lg-3 col-sm-3 team-box">

				<div class="team-member" tabindex="0">

					

						<figure class="profile-pic">

							<img src="http://www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1-300x300.jpg" alt=""/>

						</figure>
						
					<div class="member-details">

						
							<h3 class="dark-text red-border-bottom">UN PRO CHEZ VOUS 30</h3>

						
						
							<div
								class="position">21 RUE DES ECOLES  30730 MONTPEZAT</div>

						
					</div>

					<div class="social-icons">

						<ul>
							
							
						</ul>

					</div>

					
				</div>

			</div>

			</span><span id="zerif_team-widget-4" class="">
			<div class="col-lg-3 col-sm-3 team-box">

				<div class="team-member" tabindex="0">

					

						<figure class="profile-pic">

							<img src="http://www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1-300x300.jpg" alt=""/>

						</figure>
						
					<div class="member-details">

						
							<h3 class="dark-text red-border-bottom">UN PRO CHEZ VOUS 26</h3>

						
						
							<div
								class="position">ZA LA PALUN &#8211; 26170 BUIS LES BARONNIES</div>

						
					</div>

					<div class="social-icons">

						<ul>
							
							
						</ul>

					</div>

					
				</div>

			</div>

			</span><span id="zerif_team-widget-6" class="">
			<div class="col-lg-3 col-sm-3 team-box">

				<div class="team-member" tabindex="0">

					

						<figure class="profile-pic">

							<img src="http://www.unprochezvous.com/wp-content/uploads/2017/11/LOGO-UPCV-MEKICE-e1511348967385.jpg" alt=""/>

						</figure>
						
					<div class="member-details">

						
							<h3 class="dark-text red-border-bottom">UN PRO CHEZ VOUS 83</h3>

						
						
							<div
								class="position">9 RUE DE LA FERRAGE &#8211; 83136 NEOULES
</div>

						
					</div>

					<div class="social-icons">

						<ul>
							
							
						</ul>

					</div>

					
				</div>

			</div>

			</span></div> </div></section><section class="testimonial" id="testimonials"><div class="container"><div class="section-header"><h2 class="white-text">Témoignages entreprises adhérentes</h2></div><div class="row" data-scrollreveal="enter right after 0s over 1s"><div class="col-md-12"><div id="client-feedbacks" class="owl-carousel owl-theme  "><span id="zerif_testim-widget-1" class="feedback-box">

			<!-- MESSAGE OF THE CLIENT -->

							<div class="message">
					« Je suis adhérent à Un Pro Chez Vous 84 depuis 2008 et l’agrément Services à la Personne me permet d’avoir plus de clients particuliers, mais aussi de fidéliser mes clients actuels. 
Et puis, c’est proposer au particulier une prestation de qualité faite par de vrais professionnels, avec des outils professionnels, et des assurances civiles professionnelles spécifiques qui couvrent  en cas d’accident chez le client. » 				</div>
			
			<!-- CLIENT INFORMATION -->

			<div class="client">

				<div class="quote red-text">

					<i class="fa fa-quote-left"></i>

				</div>

				<div class="client-info">

					<a  target="_blank"						class="client-name" 
												>
			Yves MAYADE « Le jardinier paysagiste »</a>


					
				</div>

				<div class="client-image hidden-xs"><img src="http://www.unprochezvous.com/wp-content/uploads/2017/11/Yves-MAYADE-300x225.jpg" alt="" /></div>
			</div>
			<!-- / END CLIENT INFORMATION-->


			</span><span id="zerif_testim-widget-2" class="feedback-box">

			<!-- MESSAGE OF THE CLIENT -->

							<div class="message">
					«  Lorsqu’on est artisan traiteur on est souvent confronté à une concurrence déloyale non déclarée mais forcément moins chère. Etre membre de la coopérative Un Pro Chez Vous 13  nous a permis de rester compétitifs tout en offrant à nos clients un service de qualité et l’assurance d’un travail de professionnels » 				</div>
			
			<!-- CLIENT INFORMATION -->

			<div class="client">

				<div class="quote red-text">

					<i class="fa fa-quote-left"></i>

				</div>

				<div class="client-info">

					<a  target="_blank"						class="client-name" 
												>
			Emmanuelle WILLEMART « Une P&rsquo;tite douceur »</a>


					
				</div>

				<div class="client-image hidden-xs"><img src="http://www.unprochezvous.com/wp-content/uploads/2017/11/Emmanuelle-Willemart-300x201.jpg" alt="" /></div>
			</div>
			<!-- / END CLIENT INFORMATION-->


			</span><span id="zerif_testim-widget-3" class="feedback-box">

			<!-- MESSAGE OF THE CLIENT -->

							<div class="message">
					 « Membre depuis quelques années chez Un Pro Chez Vous 30, je suis très satisfait de  leurs services, sérieux, rapidité, etc&#8230;, cela m’a amené une clientèle fidèle  intéressée par la formule proposée »
Dépanneur Informatique				</div>
			
			<!-- CLIENT INFORMATION -->

			<div class="client">

				<div class="quote red-text">

					<i class="fa fa-quote-left"></i>

				</div>

				<div class="client-info">

					<a  target="_blank"						class="client-name" 
												>
			Mr LEVÊQUE « PC Rescousse »</a>


					
				</div>

				<div class="client-image hidden-xs"><img src="http://www.unprochezvous.com/wp-content/uploads/2017/11/Michel-Leveque-181x300.jpg" alt="" /></div>
			</div>
			<!-- / END CLIENT INFORMATION-->


			</span></div></div></div></div></section><section class="purchase-now" id="ribbon_right"><div class="container"><div class="row"><div class="col-md-9" data-scrollreveal="enter left after 0s over 1s"><h3 class="white-text">Vous êtes une Entreprise et vous souhaitez adhérer à une de nos coopératives !</h3></div><div class="col-md-3" data-scrollreveal="enter right after 0s over 1s"><a href="http://www.unprochezvous.com/index.php/professionnels/" class="btn btn-primary custom-button red-btn">contactez-nous !</a></div></div></div></section><section class="latest-news" id="latestnews"><div class="container"><div class="section-header"><h2 class="dark-text">suivez-nous sur Facebook 👍</h2></div><!-- END .section-header --><div class="clear"></div><div id="carousel-homepage-latestnews" class="carousel slide" data-ride="carousel"><div class="carousel-inner" role="listbox"></div><!-- .carousel-inner --><a class="left carousel-control" href="#carousel-homepage-latestnews" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Précédent</span>
					</a><a class="right carousel-control" href="#carousel-homepage-latestnews" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Suivant</span>
					</a></div><!-- #carousel-homepage-latestnews --></div><!-- .container --></section>
</div><!-- .site-content -->


<footer id="footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter">

	
	<div class="container">

		
		<div class="col-md-4 company-details"><div class="icon-top green-text"><a href="mailto:contact@unprochezvous.fr"><img src="http://www.unprochezvous.com/wp-content/themes/zerif-lite/images/envelope4-green.png" alt="" /></a></div><div class="zerif-footer-email">contact@unprochezvous.fr</div></div><div class="col-md-4 company-details"><div class="icon-top blue-text"><a href="tel: 3620 dîtes " UN PRO CHEZ VOUS ""><img src="http://www.unprochezvous.com/wp-content/themes/zerif-lite/images/telephone65-blue.png" alt="" /></a></div><div class="zerif-footer-phone">3620 dîtes " UN PRO CHEZ VOUS "</div></div><div class="col-md-4 copyright"><ul class="social"><li id="facebook"><a target="_blank" href="https://fr-fr.facebook.com/unprochezvous/"><span class="sr-only">Lien Facebook</span> <i class="fa fa-facebook"></i></a></li><li id="twitter"><a target="_blank" href="https://twitter.com/unprochezvous?lang=fr"><span class="sr-only">Lien Twitter</span> <i class="fa fa-twitter"></i></a></li></ul><!-- .social --><div class="zerif-copyright-box"><a class="zerif-copyright" rel="nofollow">Zerif Lite </a>developed by <a class="zerif-copyright" href="https://themeisle.com"  target="_blank" rel="nofollow">ThemeIsle</a></div></div>	</div> <!-- / END CONTAINER -->

</footer> <!-- / END FOOOTER  -->


	</div><!-- mobile-bg-fix-whole-site -->
</div><!-- .mobile-bg-fix-wrap -->


	<div style="display:none">
	</div>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/jquery-cookie.js?ver=8.1.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var config = {"baseurl":"http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/","serverurl":"https:\/\/tcx-live-chat.appspot.com","enable_typing_preview":"","ring_override":"http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/includes\/sounds\/general\/ring.wav","message_override":"http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/includes\/sounds\/general\/ding.mp3","current_wpuserid":"0","allowed_upload_extensions":"jpg|jpeg|png|gif|bmp|mp4|mp3|mpeg|ogg|ogm|ogv|webm|avi|wav|mov|doc|docx|xls|xlsx|pub|pubx|pdf|csv|txt","wplc_use_node_server":"1","wplc_localized_string_is_typing_single":" est en train d'\u00e9crire...","wplc_user_default_visitor_name":"Invit\u00e9","wplc_text_no_visitors":"Il n'y a pas de visiteurs sur votre site en ce moment","wplc_text_connection_lost":"La connexion au serveur a \u00e9t\u00e9 interrompue, en cours de reconnexion...","wplc_text_not_accepting_chats":"Agent hors ligne - n'accepte pas les discussions","wplc_text_minimizechat":"Minimiser le chat","wplc_text_endchat":"Terminer le chat","country_code":"","country_name":"","date_days":["Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi"],"date_months":["janvier","f\u00e9vrier","mars","avril","mai","juin","juillet","ao\u00fbt","septembre","octobre","novembre","d\u00e9cembre"]};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_common_node.js?ver=8.1.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wplc_restapi_enabled = {"value":"1"};
var wplc_restapi_token = "d752ceccf7e3b839cf711d6d2b557a5f";
var wplc_restapi_endpoint = "http:\/\/www.unprochezvous.com\/index.php\/wp-json\/wp_live_chat_support\/v1";
var wplc_restapi_nonce = "5ac72cd84b";
var tcx_message_override = "http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/includes\/sounds\/general\/ding.mp3";
var wplc_is_mobile = "false";
var wplc_ajaxurl = "http:\/\/www.unprochezvous.com\/wp-admin\/admin-ajax.php";
var wplc_ajaxurl_site = "http:\/\/www.unprochezvous.com\/wp-admin\/admin-ajax.php";
var wplc_nonce = "1555f8bc35";
var wplc_plugin_url = "http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/";
var wplc_preload_images = ["http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/images\/picture-for-chat-box.jpg","http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/images\/iconRetina.png","http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/images\/iconCloseRetina.png"];
var wplc_show_chat_detail = {"name":"1","avatar":"1","date":"1","time":"1"};
var wplc_agent_data = {"1":{"name":"admin6599","md5":"e3734d60504deedce4554380fa66b437","tagline":""}};
var wplc_error_messages = {"please_enter_name":"Please enter your name","please_enter_email":"Please enter your email address","please_enter_valid_email":"Please enter a valid email address","server_connection_lost":"Connexion au serveur perdue. Veuillez recharger cette page. Erreur : ","chat_ended_by_operator":"Le chat a \u00e9t\u00e9 termin\u00e9 par l'agent.","empty_message":"Veuillez entrer un message","disconnected_message":"D\u00e9connect\u00e9, en cours de tentative de reconnexion..."};
var wplc_enable_ding = {"value":"1"};
var wplc_filter_run_override = {"value":"1"};
var wplc_offline_msg = "Envoi du message...";
var wplc_offline_msg3 = "Merci pour votre message. Nous vous contacterons prochainement.";
var wplc_welcome_msg = "Veuillez attendre un agent. Envoyez votre message pendant que vous attendez.";
var wplc_pro_sst1 = "D\u00e9marrer le chat";
var wplc_pro_offline_btn_send = "Envoyer le message";
var wplc_user_default_visitor_name = "Invit\u00e9";
var wplc_localized_string_is_typing = "agent est en train d'\u00e9crire...";
var tcx_localized_strings = [" \u00e0 rejoint la discussion."," \u00e0 quitt\u00e9 la discussion."," a termin\u00e9 le chat."," s'est d\u00e9connect\u00e9.","(modifi\u00e9)","\u00c9crivez ici"];
var wplc_extra_data = {"object_switch":"1"};
var wplc_misc_strings = {"typing_enabled":"1","wplc_delay":"2000","typingimg":"http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/images\/comment.svg"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_u.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/md5.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/vendor/sockets.io/socket.io.slim.js?ver=8.1.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wplc_strings = {"restart_chat":"Restart Chat"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_node.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_u_node_events.js?ver=8.1.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wplc_user_avatars = {"1":"\/\/www.gravatar.com\/avatar\/e3734d60504deedce4554380fa66b437"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/themes/modern.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/ui/draggable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/tcx_action_events.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_pro_features.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_u_node_pro_events.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_u_editor.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/jetpack/_inc/build/photon/photon.min.js?ver=20191001'></script>
<script type='text/javascript' src='https://secure.gravatar.com/js/gprofiles.js?ver=2020Mayaa'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var WPGroHo = {"my_hash":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/jetpack/modules/wpgroho.js?ver=5.4.1'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/bootstrap.min.js?ver=20120206'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/jquery.knob.js?ver=20120206'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/smoothscroll.js?ver=20120206'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/scrollReveal.js?ver=20120206'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/zerif.js?ver=20120206'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/wp-embed.min.js?ver=5.4.1'></script>
<script type='text/javascript' src='https://stats.wp.com/e-202018.js' async='async' defer='defer'></script>
<script type='text/javascript'>
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:8.3',blog:'139214028',post:'90',tz:'2',srv:'www.unprochezvous.com'} ]);
	_stq.push([ 'clickTrackerInit', '139214028', '90' ]);
</script>


</body>

</html>
