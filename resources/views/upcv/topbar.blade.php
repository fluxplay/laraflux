<div class="container">


    <div class="navbar-header responsive-logo">

        <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">

            <span class="sr-only">Afficher/masquer la navigation</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

        </button>

        <div class="navbar-brand" itemscope itemtype="http://schema.org/Organization">

            <a href="/public/upcv" class="custom-logo-link" rel="home"><img width="271" height="271"
                    src="https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-e1511348967385.jpg?fit=271%2C271"
                    class="custom-logo" alt="UN PRO CHEZ VOUS"
                    srcset="https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-e1511348967385.jpg?w=271 271w, https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-e1511348967385.jpg?resize=150%2C150 150w, https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-e1511348967385.jpg?resize=250%2C250 250w, https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-e1511348967385.jpg?resize=174%2C174 174w"
                    sizes="(max-width: 271px) 100vw, 271px" data-attachment-id="140"
                    data-permalink="http://www.unprochezvous.com/cropped-logo-upcv-mekice-e1511348967385-jpg/"
                    data-orig-file="https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-e1511348967385.jpg?fit=271%2C271"
                    data-orig-size="271,271" data-comments-opened="1"
                    data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}"
                    data-image-title="cropped-LOGO-UPCV-MEKICE-e1511348967385.jpg" data-image-description="&lt;p&gt;http://www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-e1511348967385.jpg&lt;/p&gt;
" data-medium-file="https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-e1511348967385.jpg?fit=271%2C271"
                    data-large-file="https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-e1511348967385.jpg?fit=271%2C271" /></a>
        </div> <!-- /.navbar-brand -->

    </div> <!-- /.navbar-header -->

    <nav class="navbar-collapse bs-navbar-collapse collapse" id="site-navigation" itemscope
        itemtype="http://schema.org/SiteNavigationElement">
        <a class="screen-reader-text skip-link" href="#content">Aller au contenu principal</a>
        <ul id="menu-menu-superieur" class="nav navbar-nav navbar-right responsive-nav main-nav-list">
            <li id="menu-item-167"
                class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-167">
                <a href="/public/upcv" aria-current="page">Accueil</a></li>
            <li id="menu-item-118" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-118"><a
                    href="/public/upcv/particuliers">Particuliers</a></li>
            <li id="menu-item-119" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-119"><a
                    href="/public/upcv/professionnels">Professionnels</a></li>
            <li id="menu-item-184" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-184"><a
                    href="/public/upcv/contact">Contact</a></li>
            <li id="menu-item-226" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-226"><a
                    href="/public/upcv/monespace">Mon espace
                    Clients/Entreprises</a></li>
        </ul>
    </nav>

</div> <!-- /.container -->