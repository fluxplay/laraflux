
<!DOCTYPE html>

<html lang="fr-FR">

<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="http://www.unprochezvous.com/xmlrpc.php">

<title>Contact &#8211; UN PRO CHEZ VOUS</title>
<link rel='dns-prefetch' href='//secure.gravatar.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="UN PRO CHEZ VOUS &raquo; Flux" href="http://www.unprochezvous.com/index.php/feed/" />
<link rel="alternate" type="application/rss+xml" title="UN PRO CHEZ VOUS &raquo; Flux des commentaires" href="http://www.unprochezvous.com/index.php/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.unprochezvous.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.1"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wplc-admin-style-emoji-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/vendor/wdt-emoji/wdt-emoji-bundle.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-user-pro-styles-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/wplc_styles_pro.css?ver=5.4.1' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='http://www.unprochezvous.com/wp-includes/css/dist/block-library/style.min.css?ver=5.4.1' type='text/css' media='all' />
<style id='wp-block-library-inline-css' type='text/css'>
.has-text-align-justify{text-align:justify;}
</style>
<link rel='stylesheet' id='font-awesome-5-css'  href='http://www.unprochezvous.com/wp-content/plugins/themeisle-companion/obfx_modules/gutenberg-blocks/assets/fontawesome/css/all.min.css?ver=2.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-4-shims-css'  href='http://www.unprochezvous.com/wp-content/plugins/themeisle-companion/obfx_modules/gutenberg-blocks/assets/fontawesome/css/v4-shims.min.css?ver=2.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='themeisle-block_styles-css'  href='http://www.unprochezvous.com/wp-content/plugins/themeisle-companion/vendor/codeinwp/gutenberg-blocks/build/style.css?ver=1.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-font-awesome-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/fontawesome-all.min.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-style-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/wplcstyle.css?ver=8.1.7' type='text/css' media='all' />
<style id='wplc-style-inline-css' type='text/css'>
#wp-live-chat-header { background:url('http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/images/chaticon.png') no-repeat; background-size: cover; }  #wp-live-chat-header.active { background:url('http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/images/iconCloseRetina.png') no-repeat; background-size: cover; } #wp-live-chat-4 { background:url('http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/images/bg/cloudy.jpg') repeat; background-size: cover; }
</style>
<link rel='stylesheet' id='wplc-theme-palette-default-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/themes/theme-default.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-theme-modern-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/themes/modern.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-theme-position-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/css/themes/position-bottom-right.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='wplc-gutenberg-template-styles-user-css'  href='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/includes/blocks/wplc-chat-box/wplc_gutenberg_template_styles.css?ver=8.1.7' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_font-css'  href='//fonts.googleapis.com/css?family=Lato%3A300%2C400%2C700%2C400italic%7CMontserrat%3A400%2C700%7CHomemade+Apple&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_font_all-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;subset=latin&#038;ver=5.4.1' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_bootstrap_style-css'  href='http://www.unprochezvous.com/wp-content/themes/zerif-lite/css/bootstrap.css?ver=5.4.1' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_fontawesome-css'  href='http://www.unprochezvous.com/wp-content/themes/zerif-lite/css/font-awesome.min.css?ver=v1' type='text/css' media='all' />
<link rel='stylesheet' id='zerif_style-css'  href='http://www.unprochezvous.com/wp-content/themes/zerif-lite/style.css?ver=v1' type='text/css' media='all' />
<style id='zerif_style-inline-css' type='text/css'>

		.page-template-builder-fullwidth {
			overflow: hidden;
		}
		@media (min-width: 768px) {
			.page-template-builder-fullwidth-std .header > .elementor {
				padding-top: 76px;
			}
		}

</style>
<link rel='stylesheet' id='zerif_responsive_style-css'  href='http://www.unprochezvous.com/wp-content/themes/zerif-lite/css/responsive.css?ver=v1' type='text/css' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='zerif_ie_style-css'  href='http://www.unprochezvous.com/wp-content/themes/zerif-lite/css/ie.css?ver=v1' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='jetpack_css-css'  href='http://www.unprochezvous.com/wp-content/plugins/jetpack/css/jetpack.css?ver=8.3' type='text/css' media='all' />
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wplc_datetime_format = {"date_format":"j F Y","time_format":"H:i"};
var tcx_api_key = "cfca92a36c8cb2497d24393ee67a7158";
var wplc_guid = "6f58628858d9d93401be5e85c8ad00bff2523e4e";
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_server.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/vendor/wdt-emoji/wdt-emoji-concat.min.js?ver=8.1.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pf = {"spam":{"label":"Je suis humain !","value":"851b7a7e5b"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/pirate-forms/public/js/custom-spam.js?ver=5.4.1'></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/html5.js?ver=5.4.1'></script>
<![endif]-->
<link rel='https://api.w.org/' href='http://www.unprochezvous.com/index.php/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.unprochezvous.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.unprochezvous.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.4.1" />
<link rel="canonical" href="http://www.unprochezvous.com/index.php/contact/" />
<link rel='shortlink' href='https://wp.me/P9q7Vy-1R' />
<link rel="alternate" type="application/json+oembed" href="http://www.unprochezvous.com/index.php/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.unprochezvous.com%2Findex.php%2Fcontact%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://www.unprochezvous.com/index.php/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.unprochezvous.com%2Findex.php%2Fcontact%2F&#038;format=xml" />
  <script type="text/javascript">
    var wplc_ajaxurl = 'http://www.unprochezvous.com/wp-admin/admin-ajax.php';
    var wplc_nonce = '1555f8bc35';
  </script>
  
<link rel='dns-prefetch' href='//v0.wordpress.com'/>
<link rel='dns-prefetch' href='//widgets.wp.com'/>
<link rel='dns-prefetch' href='//s0.wp.com'/>
<link rel='dns-prefetch' href='//0.gravatar.com'/>
<link rel='dns-prefetch' href='//1.gravatar.com'/>
<link rel='dns-prefetch' href='//2.gravatar.com'/>
<link rel='dns-prefetch' href='//i0.wp.com'/>
<link rel='dns-prefetch' href='//i1.wp.com'/>
<link rel='dns-prefetch' href='//i2.wp.com'/>
<style type='text/css'>img#wpstats{display:none}</style><style type="text/css" id="custom-background-css">
body.custom-background { background-image: url("http://www.unprochezvous.com/wp-content/uploads/2017/11/photo-famille-site-2.jpg"); background-position: left top; background-size: cover; background-repeat: no-repeat; background-attachment: fixed; }
</style>
	
<!-- Jetpack Open Graph Tags -->
<meta property="og:type" content="article" />
<meta property="og:title" content="Contact" />
<meta property="og:url" content="http://www.unprochezvous.com/index.php/contact/" />
<meta property="og:description" content="&nbsp; &nbsp; &nbsp;" />
<meta property="article:published_time" content="2017-11-24T09:44:27+00:00" />
<meta property="article:modified_time" content="2017-11-24T14:45:22+00:00" />
<meta property="og:site_name" content="UN PRO CHEZ VOUS" />
<meta property="og:image" content="http://www.unprochezvous.com/wp-content/uploads/2017/11/Contactez-nous-au-3620-e1511516987326.jpg" />
<meta property="og:image:width" content="584" />
<meta property="og:image:height" content="212" />
<meta property="og:locale" content="fr_FR" />
<meta name="twitter:text:title" content="Contact" />
<meta name="twitter:card" content="summary" />

<!-- End Jetpack Open Graph Tags -->
<link rel="icon" href="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1.jpg?fit=32%2C32" sizes="32x32" />
<link rel="icon" href="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1.jpg?fit=192%2C192" sizes="192x192" />
<link rel="apple-touch-icon" href="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1.jpg?fit=180%2C180" />
<meta name="msapplication-TileImage" content="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/cropped-LOGO-UPCV-MEKICE-1.jpg?fit=270%2C270" />

</head>


	<body class="page-template-default page page-id-115 wp-custom-logo" >



<div id="mobilebgfix">
	<div class="mobile-bg-fix-img-wrap">
		<div class="mobile-bg-fix-img"></div>
	</div>
	<div class="mobile-bg-fix-whole-site">


<header id="home" class="header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">

	<div id="main-nav" class="navbar navbar-inverse bs-docs-nav" role="banner">

	@include('upcv.topbar')

		
	</div> <!-- /#main-nav -->
	<!-- / END TOP BAR -->

<div class="clear"></div>

</header> <!-- / END HOME SECTION  -->


<div id="content" class="site-content">

	<div class="container">

				<div class="content-left-wrap col-md-9">				<div id="primary" class="content-area">

			<main itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage" id="main" class="site-main">

				<article id="post-115" class="post-115 page type-page status-publish hentry">

	<header class="entry-header">

		<span class="date updated published">24 novembre 2017</span>
		<span class="vcard author byline"><a href="http://www.unprochezvous.com/index.php/author/admin6599/" class="fn">admin6599</a></span>

				<h1 class="entry-title" itemprop="headline">Contact</h1>
		
	</header><!-- .entry-header -->

	<div class="entry-content">

		<p><img data-attachment-id="23" data-permalink="http://www.unprochezvous.com/index.php/contact/contactez-nous-au-3620-2/" data-orig-file="https://i0.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/Contactez-nous-au-3620-e1511516987326.jpg?fit=584%2C212" data-orig-size="584,212" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="Contactez-nous-au-3620" data-image-description="" data-medium-file="https://i0.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/Contactez-nous-au-3620-e1511516987326.jpg?fit=300%2C109" data-large-file="https://i0.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/Contactez-nous-au-3620-e1511516987326.jpg?fit=584%2C212" class="alignnone size-full wp-image-23 aligncenter" src="https://i0.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/Contactez-nous-au-3620-e1511516987326.jpg?resize=584%2C212" alt="" width="584" height="212" data-recalc-dims="1" /></p>
<p>&nbsp;</p>
<p><img data-attachment-id="29" data-permalink="http://www.unprochezvous.com/index.php/contact/logo-services-a-la-personne/" data-orig-file="https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/Logo-Services-à-la-Personne.jpg?fit=378%2C314" data-orig-size="378,314" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;0&quot;}" data-image-title="Logo Services à la Personne" data-image-description="" data-medium-file="https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/Logo-Services-à-la-Personne.jpg?fit=300%2C249" data-large-file="https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/Logo-Services-à-la-Personne.jpg?fit=378%2C314" class="wp-image-29 aligncenter" src="https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/Logo-Services-à-la-Personne.jpg?resize=330%2C274" alt="" width="330" height="274" srcset="https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/Logo-Services-à-la-Personne.jpg?resize=300%2C249 300w, https://i1.wp.com/www.unprochezvous.com/wp-content/uploads/2017/11/Logo-Services-à-la-Personne.jpg?w=378 378w" sizes="(max-width: 330px) 100vw, 330px" data-recalc-dims="1" /></p>
<p>&nbsp;</p>
<p>&nbsp;</p>

	</div><!-- .entry-content -->

	
</article><!-- #post-## -->

			</main><!-- #main -->

		</div><!-- #primary -->

	</div>		<div class="sidebar-wrap col-md-3 content-left-wrap">
			

	<div id="secondary" class="widget-area" role="complementary">

		
		<aside id="media_image-2" class="widget widget_media_image"><img width="150" height="150" src="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?resize=150%2C150" class="image wp-image-290  attachment-thumbnail size-thumbnail" alt="" style="max-width: 100%; height: auto;" srcset="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?w=2970 2970w, https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?resize=150%2C150 150w, https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?resize=300%2C300 300w, https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?resize=768%2C770 768w, https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?resize=1022%2C1024 1022w, https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?resize=250%2C250 250w, https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?resize=174%2C174 174w, https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?w=1280 1280w, https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?w=1920 1920w" sizes="(max-width: 150px) 100vw, 150px" data-attachment-id="290" data-permalink="http://www.unprochezvous.com/vynil-50-upcv-18/" data-orig-file="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?fit=2970%2C2976" data-orig-size="2970,2976" data-comments-opened="1" data-image-meta="{&quot;aperture&quot;:&quot;0&quot;,&quot;credit&quot;:&quot;&quot;,&quot;camera&quot;:&quot;&quot;,&quot;caption&quot;:&quot;&quot;,&quot;created_timestamp&quot;:&quot;0&quot;,&quot;copyright&quot;:&quot;&quot;,&quot;focal_length&quot;:&quot;0&quot;,&quot;iso&quot;:&quot;0&quot;,&quot;shutter_speed&quot;:&quot;0&quot;,&quot;title&quot;:&quot;&quot;,&quot;orientation&quot;:&quot;1&quot;}" data-image-title="VYNIL -50% UPCV 18" data-image-description="" data-medium-file="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?fit=300%2C300" data-large-file="https://i2.wp.com/www.unprochezvous.com/wp-content/uploads/2018/10/VYNIL-50-UPCV-18.jpg?fit=640%2C641" /></aside><aside id="text-6" class="widget widget_text"><h2 class="widget-title">Retrouvez-nous</h2>			<div class="textwidget"><p><strong>Heures d’ouverture</strong><br />
Du lundi au vendredi : 9h00-12h30 et 13h30-17h30</p>
<ul>
<li><strong>Un PRO Chez Vous 13</strong></li>
<li><strong>Un PRO Chez Vous 26</strong></li>
<li><strong>Un PRO Chez Vous 30</strong></li>
<li><strong>Un PRO Chez Vous 84</strong></li>
<li><strong>Un PRO Chez Vous 83</strong></li>
</ul>
</div>
		</aside><aside id="text-7" class="widget widget_text"><h2 class="widget-title">Nous joindre</h2>			<div class="textwidget"><p><strong>3620 </strong>dîtes <strong>« <em>UN PRO CHEZ  VOUS</em></strong></p>
<p><a href="contact@unprochezvous.fr">contact@unprochezvous.fr</a></p>
</div>
		</aside><aside id="text-8" class="widget widget_text"><h2 class="widget-title">Mention Légales</h2>			<div class="textwidget"><p><a href="http://www.unprochezvous.com/wp-content/uploads/2017/11/Mentions-légales-unprochezvous.com_-1.pdf">Mentions légales UN PRO CHEZ VOUS</a></p>
</div>
		</aside>
		
	</div><!-- #secondary -->

			</div><!-- .sidebar-wrap -->
			</div><!-- .container -->


</div><!-- .site-content -->


<footer id="footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter">

	
	<div class="container">

		
		<div class="col-md-4 company-details"><div class="icon-top green-text"><a href="mailto:contact@unprochezvous.fr"><img src="http://www.unprochezvous.com/wp-content/themes/zerif-lite/images/envelope4-green.png" alt="" /></a></div><div class="zerif-footer-email">contact@unprochezvous.fr</div></div><div class="col-md-4 company-details"><div class="icon-top blue-text"><a href="tel: 3620 dîtes " UN PRO CHEZ VOUS ""><img src="http://www.unprochezvous.com/wp-content/themes/zerif-lite/images/telephone65-blue.png" alt="" /></a></div><div class="zerif-footer-phone">3620 dîtes " UN PRO CHEZ VOUS "</div></div><div class="col-md-4 copyright"><ul class="social"><li id="facebook"><a target="_blank" href="https://fr-fr.facebook.com/unprochezvous/"><span class="sr-only">Lien Facebook</span> <i class="fa fa-facebook"></i></a></li><li id="twitter"><a target="_blank" href="https://twitter.com/unprochezvous?lang=fr"><span class="sr-only">Lien Twitter</span> <i class="fa fa-twitter"></i></a></li></ul><!-- .social --><div class="zerif-copyright-box"><a class="zerif-copyright" rel="nofollow">Zerif Lite </a>developed by <a class="zerif-copyright" href="https://themeisle.com"  target="_blank" rel="nofollow">ThemeIsle</a></div></div>	</div> <!-- / END CONTAINER -->

</footer> <!-- / END FOOOTER  -->


	</div><!-- mobile-bg-fix-whole-site -->
</div><!-- .mobile-bg-fix-wrap -->


	<div style="display:none">
	</div>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/jquery-cookie.js?ver=8.1.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var config = {"baseurl":"http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/","serverurl":"https:\/\/tcx-live-chat.appspot.com","enable_typing_preview":"","ring_override":"http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/includes\/sounds\/general\/ring.wav","message_override":"http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/includes\/sounds\/general\/ding.mp3","current_wpuserid":"0","allowed_upload_extensions":"jpg|jpeg|png|gif|bmp|mp4|mp3|mpeg|ogg|ogm|ogv|webm|avi|wav|mov|doc|docx|xls|xlsx|pub|pubx|pdf|csv|txt","wplc_use_node_server":"1","wplc_localized_string_is_typing_single":" est en train d'\u00e9crire...","wplc_user_default_visitor_name":"Invit\u00e9","wplc_text_no_visitors":"Il n'y a pas de visiteurs sur votre site en ce moment","wplc_text_connection_lost":"La connexion au serveur a \u00e9t\u00e9 interrompue, en cours de reconnexion...","wplc_text_not_accepting_chats":"Agent hors ligne - n'accepte pas les discussions","wplc_text_minimizechat":"Minimiser le chat","wplc_text_endchat":"Terminer le chat","country_code":"","country_name":"","date_days":["Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi"],"date_months":["janvier","f\u00e9vrier","mars","avril","mai","juin","juillet","ao\u00fbt","septembre","octobre","novembre","d\u00e9cembre"]};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_common_node.js?ver=8.1.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wplc_restapi_enabled = {"value":"1"};
var wplc_restapi_token = "d752ceccf7e3b839cf711d6d2b557a5f";
var wplc_restapi_endpoint = "http:\/\/www.unprochezvous.com\/index.php\/wp-json\/wp_live_chat_support\/v1";
var wplc_restapi_nonce = "5ac72cd84b";
var tcx_message_override = "http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/includes\/sounds\/general\/ding.mp3";
var wplc_is_mobile = "false";
var wplc_ajaxurl = "http:\/\/www.unprochezvous.com\/wp-admin\/admin-ajax.php";
var wplc_ajaxurl_site = "http:\/\/www.unprochezvous.com\/wp-admin\/admin-ajax.php";
var wplc_nonce = "1555f8bc35";
var wplc_plugin_url = "http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/";
var wplc_preload_images = ["http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/images\/picture-for-chat-box.jpg","http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/images\/iconRetina.png","http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/images\/iconCloseRetina.png"];
var wplc_show_chat_detail = {"name":"1","avatar":"1","date":"1","time":"1"};
var wplc_agent_data = {"1":{"name":"admin6599","md5":"e3734d60504deedce4554380fa66b437","tagline":""}};
var wplc_error_messages = {"please_enter_name":"Please enter your name","please_enter_email":"Please enter your email address","please_enter_valid_email":"Please enter a valid email address","server_connection_lost":"Connexion au serveur perdue. Veuillez recharger cette page. Erreur : ","chat_ended_by_operator":"Le chat a \u00e9t\u00e9 termin\u00e9 par l'agent.","empty_message":"Veuillez entrer un message","disconnected_message":"D\u00e9connect\u00e9, en cours de tentative de reconnexion..."};
var wplc_enable_ding = {"value":"1"};
var wplc_filter_run_override = {"value":"1"};
var wplc_offline_msg = "Envoi du message...";
var wplc_offline_msg3 = "Merci pour votre message. Nous vous contacterons prochainement.";
var wplc_welcome_msg = "Veuillez attendre un agent. Envoyez votre message pendant que vous attendez.";
var wplc_pro_sst1 = "D\u00e9marrer le chat";
var wplc_pro_offline_btn_send = "Envoyer le message";
var wplc_user_default_visitor_name = "Invit\u00e9";
var wplc_localized_string_is_typing = "agent est en train d'\u00e9crire...";
var tcx_localized_strings = [" \u00e0 rejoint la discussion."," \u00e0 quitt\u00e9 la discussion."," a termin\u00e9 le chat."," s'est d\u00e9connect\u00e9.","(modifi\u00e9)","\u00c9crivez ici"];
var wplc_extra_data = {"object_switch":"1"};
var wplc_misc_strings = {"typing_enabled":"1","wplc_delay":"2000","typingimg":"http:\/\/www.unprochezvous.com\/wp-content\/plugins\/wp-live-chat-support\/images\/comment.svg"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_u.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/md5.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/vendor/sockets.io/socket.io.slim.js?ver=8.1.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wplc_strings = {"restart_chat":"Restart Chat"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_node.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_u_node_events.js?ver=8.1.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wplc_user_avatars = {"1":"\/\/www.gravatar.com\/avatar\/e3734d60504deedce4554380fa66b437"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/themes/modern.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/jquery/ui/draggable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/tcx_action_events.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_pro_features.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_u_node_pro_events.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/wp-live-chat-support/js/wplc_u_editor.js?ver=8.1.7'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/jetpack/_inc/build/photon/photon.min.js?ver=20191001'></script>
<script type='text/javascript' src='https://secure.gravatar.com/js/gprofiles.js?ver=2020Mayaa'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var WPGroHo = {"my_hash":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/jetpack/modules/wpgroho.js?ver=5.4.1'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/bootstrap.min.js?ver=20120206'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/jquery.knob.js?ver=20120206'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/smoothscroll.js?ver=20120206'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/scrollReveal.js?ver=20120206'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/themes/zerif-lite/js/zerif.js?ver=20120206'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-includes/js/wp-embed.min.js?ver=5.4.1'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/jetpack/_inc/build/spin.min.js?ver=1.3'></script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/jetpack/_inc/build/jquery.spin.min.js?ver=1.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var jetpackCarouselStrings = {"widths":[370,700,1000,1200,1400,2000],"is_logged_in":"","lang":"fr","ajaxurl":"http:\/\/www.unprochezvous.com\/wp-admin\/admin-ajax.php","nonce":"f044524bb2","display_exif":"1","display_geo":"1","single_image_gallery":"1","single_image_gallery_media_file":"","background_color":"black","comment":"Commentaire","post_comment":"Laisser un commentaire","write_comment":"\u00c9crire un commentaire...","loading_comments":"Chargement des commentaires\u2026","download_original":"Afficher dans sa taille r\u00e9elle <span class=\"photo-size\">{0}<span class=\"photo-size-times\">\u00d7<\/span>{1}<\/span>.","no_comment_text":"Veuillez ajouter du contenu \u00e0 votre commentaire.","no_comment_email":"Merci de renseigner une adresse e-mail.","no_comment_author":"Merci de renseigner votre nom.","comment_post_error":"Une erreur s'est produite \u00e0 la publication de votre commentaire. Veuillez nous en excuser, et r\u00e9essayer dans quelques instants.","comment_approved":"Votre commentaire a \u00e9t\u00e9 approuv\u00e9.","comment_unapproved":"Votre commentaire est en attente de validation.","camera":"Appareil photo","aperture":"Ouverture","shutter_speed":"Vitesse d'obturation","focal_length":"Focale","copyright":"Copyright","comment_registration":"0","require_name_email":"1","login_url":"http:\/\/www.unprochezvous.com\/wp-login.php?redirect_to=http%3A%2F%2Fwww.unprochezvous.com%2Findex.php%2Fcontact%2F","blog_id":"1","meta_data":["camera","aperture","shutter_speed","focal_length","copyright"],"local_comments_commenting_as":"<fieldset><label for=\"email\">E-mail (requis)<\/label> <input type=\"text\" name=\"email\" class=\"jp-carousel-comment-form-field jp-carousel-comment-form-text-field\" id=\"jp-carousel-comment-form-email-field\" \/><\/fieldset><fieldset><label for=\"author\">Nom (requis)<\/label> <input type=\"text\" name=\"author\" class=\"jp-carousel-comment-form-field jp-carousel-comment-form-text-field\" id=\"jp-carousel-comment-form-author-field\" \/><\/fieldset><fieldset><label for=\"url\">Site web<\/label> <input type=\"text\" name=\"url\" class=\"jp-carousel-comment-form-field jp-carousel-comment-form-text-field\" id=\"jp-carousel-comment-form-url-field\" \/><\/fieldset>"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.unprochezvous.com/wp-content/plugins/jetpack/_inc/build/carousel/jetpack-carousel.min.js?ver=20190102'></script>
<script type='text/javascript' src='https://stats.wp.com/e-202018.js' async='async' defer='defer'></script>
<script type='text/javascript'>
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:8.3',blog:'139214028',post:'115',tz:'2',srv:'www.unprochezvous.com'} ]);
	_stq.push([ 'clickTrackerInit', '139214028', '115' ]);
</script>


</body>

</html>
