@extends('layouts.base')

@section('header')
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

    <!-- DataTables -->
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">

<script>
window.onload = function() 
{
    var menu = document.getElementById("menu-dashboard");
    menu.className = menu.className + ' ' + 'active' ;
}
</script>
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

                    <!-- body -->

<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-fill panel-upcv">
            <div class="panel-heading">
                <h3 class="panel-title">Consulter vos Factures</h3>
            </div>
            <div class="panel-body">
                <p>Consulter en tout temps vos factures des années precedantes. </p>
                <a href="/public/recapitulatif" class="btn btn-dark btn-block waves-effect waves-light" role="button">Mes factures</a>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-fill panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Consulter vos Attestations fiscales</h3>
            </div>
            <div class="panel-body">
                <p>Consulter en tout temps vos factures des années precedantes. </p>
                <a href="/public/recapitulatif" class="btn btn-dark btn-block waves-effect waves-light" role="button">Mes attestations</a>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-fill panel-upcv-light">
            <div class="panel-heading">
            <h3 class="panel-title">Consulter le récapitulatif</h3>
            </div>
            <div class="panel-body">
                <p>Consulter en tout temps vos factures des années precedantes. </p>
                <a href="/public/recapitulatif" class="btn btn-dark btn-block waves-effect waves-light" role="button">Mon récapitulatif</a>
            </div>
        </div>
    </div>
</div>



        </div> <!-- container -->
    </div> <!-- content -->
    <footer class="footer">
        2016 - 2020 © FluXplaY.
    </footer>

</div>
@endsection


@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.init.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>

@endsection