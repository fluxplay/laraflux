@extends('layouts.base')

 {{-- Using for static function --}}
 <?php use App\Artisan; ?>

@section('header')
<script>
window.onload = function() {
    var menu = document.getElementById("menu-artisan");
    menu.className = menu.className + ' ' + 'active';
};
</script>

<!-- DataTables -->
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
    type="text/css" />
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet"
    type="text/css" />

<link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">
@endsection

@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.init.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header-title">
                        <h4 class="pull-left page-title">Bienvenue sur la page des entreprises</h4>&nbsp;
                        <!-- Modal pour ajout artisan -->
                        {{-- <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal"
                            data-target="#myModal">+</button>
                        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                            aria-hidden="true" style="display: none;">
                            @include('artisans.formNewArtisan')
                        </div> --}}
                        <ol class="breadcrumb pull-right">
                            <li><a class="btn-ajout" href="/public/artisans/create">+ Nouveau adhérent</a></li>
                            <li class="active">FluXplaY</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            {{-- form status row  --}}
            @include('subviews.formstatus')
            
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                            <h4 class="panel-title">Entreprises actives</h4>
                        </div>
                        <div class="panel-body">
                            <h3 class=""><b>2568</b></h3>
                            <p class="text-muted"><b>48%</b> Actif dans la base de données</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-6">
                    <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                            <h4 class="panel-title">Total des entreprises </h4>
                        </div>
                        <div class="panel-body">
                            <h3 class=""><b>3685</b></h3>
                            <p class="text-muted">Actives et inactives dans la base de données.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-3">
                    <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                            <h4 class="panel-title">Entreprises inactives</h4>
                        </div>
                        <div class="panel-body">
                            <h3 class=""><b>25487</b></h3>
                            <p class="text-muted"><b>65%</b> Inactives dans la base de données</p>
                        </div>
                    </div>
                </div>
            </div><!-- first line stats -->
            <div class="row">
                <div class="col-sm-12">

                    @isset($status)
                    <div class="alert alert-{{ $status['style'] }} alert-dismissible fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                        {{ $status['text'] }}
                    </div>
                    @endisset
                    @isset($status['nom'])
                    <script>
                    window.onload = function() {
                        var table = $('#datatable-responsive').DataTable();
                        table.search("{{ $status['nom'] }}").draw();
                    }
                    </script>
                    @endisset


                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Liste des entreprises</h3>
                        </div>
                        <div class="panel-body">
                            <table id="datatable-responsive"
                                class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <td>Type</td>
                                        <td>Societé</td>
                                        <td>Dirigeant</td>
                                        <td>Adresse</td>
                                        <td>CP</td>
                                        <td>Ville</td>
                                        <td>Siret</td>
                                        <td>Tva</td>
                                        <td>Nature principale</td>
                                        {{-- <td>Natures secondnaires</td> --}}
                                        <td>Taux</td>
                                        <td>Téléphone</td>
                                        <td>Email</td>
                                        {{-- <td>Site Web</td>
                                        <td>Facebook</td>
                                        <td>Linkedin</td>
                                        <td>Logo</td> --}}
                                        <td>Statut</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($artisans as $artisan)
                                    <tr>
                                        <td>{{strtoupper($artisan->type)}}</td>
                                        <td>{{$artisan->nom}}</td>
                                        <td>{{$artisan->contact}}</td>
                                        <td>{{$artisan->adresse}}</td>
                                        <td>{{$artisan->cp}}</td>
                                        <td>{{$artisan->ville}}</td>
                                        <td>{{$artisan->siret}}</td>
                                        <td>{{ number_format($artisan->tva,2) }}</td>
                                        <td>{{$artisan->nature}}</td>
                                        <td>{{ number_format($artisan->taux,2) }}</td>
                                        <td>{{$artisan->telephone}}</td>
                                        <td>{{$artisan->mail}}</td>
                                        {{-- <td>{{$artisan->siteweb}}</td>
                                        <td>{{$artisan->facebook}}</td>
                                        <td>{{$artisan->linkedin}}</td>
                                        <td>{{$artisan->logo}}</td> --}}
                                        <td>{{ Artisan::getStatusLabel($artisan->actif)}}</td>
                                        <td><a href="{{ route('artisans.edit',$artisan->id_artisans)}}"
                                                class="btn btn-primary">Edit</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div> <!-- End Row -->


        </div> <!-- container -->
    </div> <!-- content -->
    <footer class="footer">
        2016 - 2020 © FluXplaY.
    </footer>

</div>
@endsection