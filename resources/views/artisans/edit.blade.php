@extends('layouts.base')

{{-- Using for static function --}}
<?php use App\Artisan; ?>
<?php use App\Prestation; ?>

@section('header')
<script>
window.onload = function() {
    var menu = document.getElementById("menu-artisan");
    menu.className = menu.className + ' ' + 'active';
};
</script>
<style>
.uper {
    margin-top: 40px;
}
</style>
<!-- DataTables -->
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
    type="text/css" />
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet"
    type="text/css" />

<link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">
@endsection

@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.init.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header-title">
                        <h4 class="pull-left page-title">Bienvenue sur la page des adhérans</h4>
                        <ol class="breadcrumb pull-right">
                        <li><a class="btn-ajout" href="/public/artisans">+ Retour á la liste des adhérans</a></li>
                            <li class="active">FluXplaY</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Mise a jour d'un adhérans</h3>
                    </div>
                    
                    {{-- form status row  --}}
                    @include('subviews.formstatus')

                        <form method="post" class="form-horizontal" action="{{ route('artisans.update', $artisan->id_artisans) }}">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_type">Type</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="se_type" id="se_type">
                                        @foreach(Artisan::getTypeSociete() as $key => $value)
                                            <option value="{{ $key }}" {{ $artisan->type == $key ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_nom">Societé</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_nom" required value=" {{ $artisan->nom }}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_contact">Contact</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_contact" required value=" {{ $artisan->contact }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_adresse">Adresse</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_adresse" required value=" {{ $artisan->adresse }}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_cp">CP</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_cp" required value=" {{ $artisan->cp }}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_ville">Ville</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_ville" required value=" {{ $artisan->ville }}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_siret">Siret</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_siret" required value=" {{ $artisan->siret }}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_tva">TVA</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="se_tva" id="se_tva">
                                        @foreach(Artisan::getTvaOptions() as $key => $value)
                                            <option value="{{ $key }}" {{ $artisan->tva == $key ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="pri_nature">Nature pricipale</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="pri_nature" id="pri_nature"
                                        required>
                                        @foreach(Prestation::getNatureOptions() as $key => $value)
                                        <option value="{{ $key }}"  {{ $artisan->nature == $key ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sec_nature">Natures secondaires</label>
                                <div class="col-md-10">
                                    <select multiple size="5" class="form-control" name="sec_nature[]" id="sec_nature"
                                        required>
                                        @foreach(Prestation::getNatureOptions() as $key => $value)
                                        <option value="{{ $key }}"  {{ in_array($key, Artisan::getSecNaturesArray($artisan->nature_sec)) ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_taux">Taux</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="se_taux" id="se_taux">
                                        @foreach(Artisan::getTauxOptions() as $key => $value)
                                        <option value="{{ $key }}" {{ $artisan->taux == $key ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_tel">Téléphone</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_tel" value=" {{ $artisan->telephone }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_email">Email</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_email" value=" {{ $artisan->mail }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_siteweb">Site Web</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_siteweb" value=" {{ $artisan->siteweb }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_facebook">Facebook</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_facebook" value=" {{ $artisan->facebook }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_linkedin">LinkedIn</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_linkedin" value=" {{ $artisan->linkedin }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_logo">Logo</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_logo" value=" {{ $artisan->logo }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_status">Status </label>
                                <div class="col-md-10">
                                    <select class="form-control" name="se_status" id="se_status"
                                        value={{ $artisan->actif }}>
                                        @foreach(Artisan::getStatusOptions() as $key => $value)
                                        <option value="{{ $key }}" {{ $artisan->actif == $key ? 'selected' : '' }} >{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Mise à jour d'un adhéran</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div> <!-- container -->
</div> <!-- content -->
<footer class="footer">
    2016 - 2020 © FluXplaY.
</footer>

</div>
@endsection