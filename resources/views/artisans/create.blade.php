@extends('layouts.base')

 {{-- Using for static function --}}
  <?php use App\Prestation; ?>
  <?php use App\Artisan; ?>


@section('header')
<script>
window.onload = function() {
    var menu = document.getElementById("menu-artisan");
    menu.className = menu.className + ' ' + 'active';
};
</script>
<style>
.uper {
    margin-top: 40px;
}
</style>
<!-- DataTables -->
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
    type="text/css" />
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet"
    type="text/css" />

<link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">
@endsection

@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.init.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header-title">
                        <h4 class="pull-left page-title">Bienvenue sur la page des adhérents</h4>
                        <ol class="breadcrumb pull-right">
                            <li><a href="/public/artisans">Retour á la liste des adhérents</a></li>
                            <li class="active">FluXplaY</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Creer un nouvel adhérent</h3>
                    </div>
                    
                    {{-- form status row  --}}
                    @include('subviews.formstatus')
                        <form method="post" class="form-horizontal" action="{{ route('artisans.store') }}">
                            {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_type">Type</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="se_type" id="se_type">
                                        @foreach(Artisan::getTypeSociete() as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_nom">Societé</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_nom"  />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_contact">Dirigeant</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_contact"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_adresse">Adresse</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_adresse"   />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_cp">CP</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_cp"   />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_ville">Ville</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_ville"   />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_siret">Siret</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_siret"   />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_tva">Tva</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="se_tva" id="se_tva">
                                        @foreach(Artisan::getTvaOptions() as $key => $value)
                                        <option value="{{ $key }}" >{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="pri_nature">Nature principale</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="pri_nature" id="pri_nature">
                                        @foreach(Prestation::getNatureOptions() as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sec_nature">Natures secondnaires</label>
                                <div class="col-md-10">
                                    <select multiple size="5" class="form-control" name="sec_nature[]" id="sec_nature">
                                        @foreach(Prestation::getNatureOptions() as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="se_taux">Taux</label>
                                <div class="col-md-10">
                                    <select class="form-control" name="se_taux" id="se_taux">
                                        @foreach(Artisan::getTauxOptions() as $key => $value)
                                        <option value="{{ $key }}" >{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_tel">Telephone</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_tel" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_email">Email</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_email" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_siteweb">Site Web</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_siteweb" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_facebook">Facebook</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_facebook" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_linkedin">Linkedin</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_linkedin" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sz_logo">Logo</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="sz_logo" />
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label class="col-md-2 control-label" for="se_status">Status </label>
                                <div class="col-md-10">
                                    <select class="form-control" name="se_status" id="se_status">
                                        @foreach(Artisan::getStatusOptions() as $key => $value)
                                        <option value="{{ $key }}" >{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div> --}}
                            <button type="submit" class="btn btn-primary">Ajouter</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>



        </div>
    </div>


</div> <!-- container -->
</div> <!-- content -->
<footer class="footer">
    2016 - 2020 © FluXplaY.
</footer>

</div>
@endsection