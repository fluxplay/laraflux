@extends('layouts.base')

@section('header')
    <!-- DataTables -->
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">

    <link href="/vendor/xadmino/Admin/assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="/vendor/xadmino/Admin/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="/vendor/xadmino/Admin/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/vendor/xadmino/Admin/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

<script>
window.onload = function() 
{
    var menu = document.getElementById("menu-recapitulatif");
    menu.className = menu.className + ' ' + 'active' ;

    jQuery("#datepicker").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months"
    });

    @include('subjs.autoClient')
}

function jsSetYear(idYear)
{	
	ListBox = document.getElementsByName('sz_year') ;
	if(ListBox[0] != null)
	{
    for(j=0; j<ListBox[0].length; j++)
        {//alert(idYear+"=("+l_name+")="+ListBox[0].options[j].value);
            if(idYear == ListBox[0].options[j].value)
            {
                ListBox[0].options[j].selected=true;
                //alert("c la bonne selection pour :"+idYear+" j:"+j+" nb:"+ListBox[0].options[j].value);
                break;
            }
        }
	}
}
function setLineLight(id){
    var gh = document.getElementById(id);
    if(gh){ gh.className='success';}
}
function jsSetMonth(idMonth)
{	idMonth--;
	var forumulaire = document.forms['form_mois'].elements ;
	forumulaire.sz_mois.options[idMonth].selected=true;
}

</script>

<style type="text/css">

.results {
    display: none;
    width: 228px;
    position: fixed;
    z-index: 100;
    border: 1px solid #AAA;
    border-top-width: 0;
    background-color: #FFF;
}

/*surcharge le message dans le bloc 'choix du client'*/
.alert{
margin-bottom: 8px;
}

.form-control{
    text-transform: uppercase; 
}

.btn-upcv{
    background-color: #ef9303;
    color: #ffffff;
}
{{-- overload Bootstrap --}}
.label{
    color:#333;
}

.green_capsule {
    background-color: greenyellow;
}
</style>
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

                    <!-- body -->

<div class="row">
    <div class="col-sm-6 col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Choix de la période</h4>
            </div>
            <div class="panel-body">
            <form class="form-horizontal" id="form_mois" name="form_mois" method="POST" action="{{ action('FactureController@getRecapitulatifs') }}" >
                {{ csrf_field() }}
                    <select class="form-control" name="sz_mois" id="sz_mois">
                        @foreach($o_period->a_mois as $key => $value )
                            <option value={{ $key }}  {{ $value['selected']==true ? 'selected' : '' }} >{{ $value['label'] }}</option>
                        @endforeach
                    </select>
                    <br>
                    <select class="form-control" name="sz_year" id="sz_year">
                        @foreach($o_period->a_year as $key => $value )
                            <option value={{ $key }}  {{ $value['selected']==true ? 'selected' : '' }} >{{ $value['label'] }}</option>
                        @endforeach
                    </select>
                        <br><button type="submit" class="btn btn-upcv form-control">Valider</button>
                </form>
            </div>
        </div>
    </div>


    <div class="col-sm-6 col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Choix du client</h4>
            </div>
            <div class="panel-body">
            <p class="alert alert-success"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Chercher par client independement de la periode</p>
            <form class="form-horizontal" id="form_client" name="form_client" method="POST" action="{{ action('FactureController@getRecapitulatifs') }}" >
                {{ csrf_field() }}
                    <input id="search_client" type="text" class="form-control" autocomplete="off" placeholder="Client">
                    <input type="hidden" id="id_client" name="id_client" value="2">
                    <div id="results_client" class="results"></div>
                        <br><button type="submit" class="btn btn-upcv form-control">Valider</button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-lg-4">
        <div class="panel panel-primary text-center">
            <div class="panel-heading">
                <h4 class="panel-title">Nombre de Factures</h4>
            </div>
            <div class="panel-body">
            <h3 class=""><span class="glyphicon glyphicon-file"></span><b>52</b></h3>
                <p class="text-muted"><span class="glyphicon glyphicon-file"></span><b>25</b> Factures le meme mois l'année derniére</p>
            </div>
        </div>
    </div>
</div>

{{-- <div class="input-append date" id="datepicker" data-date="02-2012" data-date-format="mm-yyyy"></div> --}}

{{-- for user status row  --}}
@include('subviews.status')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Liste des clients</h3>
            </div>
            <div class="panel-body">
                <table id="datatable-responsive"
                    class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                    width="100%">
                    <thead>
                        <tr>
                            <td>N° Facture</td>
                            <td>Entreprise</td>
                            <td>Montant TTC</td>
                            <td>N° Facture</td>
                            <td>Client</td>
                            <td>Montant TTC</td>
                            <td>Date</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tab_recapitulatif as $key => $tab)
                            <tr id='{{ $tab['id_facture'] }}'>
                                <td><b><a href='{{$key}}' onclick='PopupFacture("{{ env('APP_URL') }}", {{ $key }});return false;' >{{ $tab['id_facture'] }}</a></b></td>
                                {{--artisan--}}
                                <td><span class="glyphicon glyphicon-user" aria-hidden="true">
                                    </span>
                                    <b><a href="{{env('APP_URL')}}/public/artisans/{{ $tab['id_artisans'] }}/edit" >{{ $tab['artisan'] }}</a><b></td>
                                <td>{{ $tab['amontantTTC'] }} €</td>
                                <td><b><a href='{{ $tab['cnum_facture'] }}' onclick='PopupFacture("{{ env('APP_URL') }}", {{ $tab['cnum_facture'] }});return false;' >{{ $tab['cid_facture'] }}</a></b></td>
                                {{--client--}}
                                <td><span class="glyphicon glyphicon-user" aria-hidden="true">
                                    </span>
                                    <b><a href="{{env('APP_URL')}}/public/clients/{{ $tab['id_client'] }}/edit" >{{ $tab['client'] }}</a><b></td>
                                <td>{{ $tab['cmontantTTC'] }} €</td>
                                {{-- <td>{{ $tab['date'] }}</td> --}}

                                @if(empty($tab['mail']))
                                    <td><span class="glyphicon glyphicon-file"></span><span class="label label-date">Créée le: {{ $tab['date'] }}</span>
                                    <br><span class="glyphicon glyphicon-envelope"></span><span class="label label-date green_capsule"><s>Dernier envoi le: {{ $tab['date'] }}</s></span></td>
                                    <td style="text-align:center">

                                    <button href="" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="left" title="ce client n'a pas de mail">
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                    </button>
                                @else
                                    <td><span class="glyphicon glyphicon-file"></span><span class="label label-date">Créée le: {{ $tab['date'] }}</span>
                                    <br><span class="glyphicon glyphicon-envelope"></span><span class="label label-date green_capsule">Dernier envoi le: {{ $tab['date'] }}</span></td>
                                    <td style="text-align:center">
                                    <button href="#modalAdminSendFacture" class="btn btn-success btn-sm btnSendFacture"
                                            data-toggle="modal" data-idfact="{{ $tab['cid_facture'] }}"
                                            data-idclient="{{ $tab['id_client'] }}"
                                            data-nomartisan="{{ $tab['artisan'] }}">
                                        <span class="glyphicon glyphicon-envelope"></span>
                                    </button>
                                @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!-- End Row -->

    <!-- Modal box -->
    <div id='modalAdminSendFacture' class='modal fade' role='dialog'>
    <div class='modal-dialog'>
    <div class='modal-content'>
    <div class='modal-header'>
    <button type='button' class='close' data-dismiss='modal'>&times;</button>
    <h4 class='modal-title'>Envoi de facture</h4>
    </div>
    <div class='modal-body'>
    <form method="post" class="form-horizontal" action="{{ route('factures.sendfacture') }}">
    {{ csrf_field() }}
    <input type='hidden' name='idfact' id='idfact'>
    <input type='hidden' name='idclient' id='idclient'>
    <input type='hidden' name='nomartisan' id='nomartisan'>
    <input type='hidden' name='mois' id='mois'>
    <input type='hidden' name='year' id='year'>
    <div class='form-group'>
    <label class='control-label col-sm-10' for=''><h4>Voulez-vous envoyer la facture par email ?</h4></label>
    </div>
    <div class='form-group'>
    <div class='col-sm-offset-2 col-sm-10'>
    </div>
    </div>
    </div>
    <div class='modal-footer'>
    <button name='btn_valider' type='submit' class='btn btn-default'>Oui</button>
    <button type='button' class='btn btn-default' data-dismiss='modal'>Non</button>
    </div>
    </div>
    </form> 
    </div>
    </div>
    </div>
    <!-- End Modal -->

        </div> <!-- container -->
    </div> <!-- content -->
    <footer class="footer">
        2016 - 2020 © FluXplaY.
    </footer>

</div>
@endsection


@section('footer')
<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Plugins js -->
<script src="/vendor/xadmino/Admin/assets/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.recapitulatif.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fluxbase.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>



<!-- Jquery for modal box -->
<script>
<!-- jquery for modal -->
$('.btnSendFacture').click(function() {   
    $('#modalAdminSendFacture #idfact').val($(this).data('idfact'));
    $('#modalAdminSendFacture #idclient').val($(this).data('idclient'));
    $('#modalAdminSendFacture #nomartisan').val($(this).data('nomartisan'));
    $('#modalAdminSendFacture #mois').val(document.getElementById('sz_mois').value);
    $('#modalAdminSendFacture #year').val(document.getElementById('sz_year').value);
});

@isset($highlights)
    @foreach ($highlights as $highlight)
        setLineLight('{{$highlight}}');
    @endforeach
@endisset
</script>

@endsection