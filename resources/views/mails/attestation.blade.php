@component('mail::facture')
{{$client}},

Par ce mail, nous avons le plaisir de vous faire parvenir, grâce au lien ci-dessous, votre attestation de l'annee **{{$annee}}**.

Vous en souhaitant bonne réception, nous restons à votre disposition.

Cordialement,
@component('mail::button', ['url' => $link])
Consulter votre attestation sur notre site
@endcomponent
@endcomponent



