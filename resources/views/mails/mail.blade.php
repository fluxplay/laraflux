@component('mail::facture')
Madame, Monsieur,

Par ce mail, nous avons le plaisir de vous faire parvenir, grâce au lien ci-dessous, votre facture de la prestation effectuée par notre associé **{{$artisan}}**.

Vous en souhaitant bonne réception, nous restons à votre disposition.

Cordialement,
@component('mail::button', ['url' => $link])
Consulter votre facture
@endcomponent
@endcomponent



