@extends('layouts.base')

@section('header')
<script>
    function setLineLight(id) {
        var gh = document.getElementById(id);
        if (gh) {
            gh.className = 'success';
        }
    }


    window.onload = function() {
        var menu = document.getElementById("menu-attestations");
        menu.className = menu.className + ' ' + 'active';
    };
</script>
<!-- DataTables -->
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />

<link href="/vendor/xadmino/Admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/icons.css" rel="stylesheet" type="text/css">
<link href="/vendor/xadmino/Admin/assets/css/style.css" rel="stylesheet" type="text/css">


<style type="text/css">
    .form-control {
        text-transform: uppercase;
    }

    .btn-upcv {
        background-color: #ef9303;
        color: #ffffff;
    }

    .label {
        color: #333;
    }

    .green_capsule {
        background-color: greenyellow;
    }
</style>
@endsection

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-header-title">
                        <h4 class="pull-left page-title">Bienvenue sur la page des attestations</h4>&nbsp;
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                            <h4 class="panel-title">Choix de la période</h4>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" id="" name="" method="POST" action="{{ route('attestations') }}">
                                {{ csrf_field() }}
                                <select class="form-control" name="sz_year" id="sz_year">
                                    @foreach($o_period->a_year as $key => $value )
                                    <option value={{ $key }} {{ (bool) $value['selected'] === true ? 'selected' : '' }}>{{ $value['label'] }}</option>
                                    @endforeach
                                </select>
                                <br><button type="submit" class="btn btn-upcv form-control">Valider</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                            <h4 class="panel-title">Chiffre d'Affaires</h4>
                        </div>
                        <div class="panel-body">
                            <h3 class=""><b>1250.55€</b></h3>
                            <p class="text-muted"><b>1000.55€</b> le meme mois l'année dernière</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="panel panel-primary text-center">
                        <div class="panel-heading">
                            <h4 class="panel-title">Nombre de Factures</h4>
                        </div>
                        <div class="panel-body">
                            <h3 class=""><span class="glyphicon glyphicon-file"></span><b>52</b></h3>
                            <p class="text-muted"><span class="glyphicon glyphicon-file"></span><b>25</b> Factures le meme mois l'année derniére</p>
                        </div>
                    </div>
                </div>
            </div>


{{-- for user status row  --}}
@include('subviews.status')

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Liste des clients</h3>
                        </div>
                        <div class="panel-body">
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <td>Client</td>
                                        <td>Adresse</td>
                                        <td>Ville</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($attestations as $attestation)
                                    <tr>
                                        <td><b><a href='{{$key}}' onclick='PopupAttestation("{{ env('APP_URL') }}", {{ $attestation->client->id_client }}, {{ $o_period->getYear() }});return false;' >{{ $attestation->client->civilite." ".$attestation->client->nom." ".$attestation->client->prenom }}</a></b></td>
                                        <td>{{ $attestation->client->adresse1 }} </td>
                                        <td>{{ $attestation->client->ville }}</td>

                                        @if(empty($attestation->client->email))
                                        <td>1
                                            <button type="button" class="btn btn-dangerult btn-sm">
                                                <span class="glyphicon glyphicon-ban-circle"></span>
                                            </button>
                                        </td>
                                        @else
                                        <td>0
                                            <button href='#modalMail' class="btn btn-success btn-sm btnSendMail" data-toggle="modal" data-idc="{{$attestation->client->id_client}}" data-year="{{$o_period->getYear()}}">
                                                <span class="glyphicon glyphicon-envelope"></span>
                                            </button>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div> <!-- End Row -->


        </div> <!-- container -->
    </div> <!-- content -->
    <footer class="footer">
        2016 - 2020 © FluXplaY.
    </footer>
</div>
@endsection


@section('footer')

<!-- Modal box -->
<div id='modalMail' class='modal fade' role='dialog'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                <h4 class='modal-title'>Envoi de l'attestation fiscale</h4>
            </div>
            <div class='modal-body'>
                <form method="post" class="form-horizontal" action="{{ route('attestations.sendmail') }}">
                    {{ csrf_field() }}
                    <input type='hidden' name='idc' id='idc'>
                    <input type='hidden' name='year' id='year'>
                    <div class='form-group'>
                        <label class='control-label col-sm-10' for=''>
                            <h4>Voulez-vous envoyer l'attestaion par email ?</h4>
                        </label>
                    </div>
                    <div class='form-group'>
                        <div class='col-sm-offset-2 col-sm-10'>
                        </div>
                    </div>
            </div>
            <div class='modal-footer'>
                <button name='btn_valider' type='submit' class='btn btn-default'>Oui</button>
                <button type='button' class='btn btn-default' data-dismiss='modal'>Non</button>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
<!-- End Modal -->

<!-- jQuery  -->
<script src="/vendor/xadmino/Admin/assets/js/jquery.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/bootstrap.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/modernizr.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/detect.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fastclick.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.slimscroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.blockUI.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/waves.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/wow.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.nicescroll.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/vendor/xadmino/Admin/assets/plugins/datatables/responsive.bootstrap.min.js"></script>

<!-- Datatable init js -->
<script src="/vendor/xadmino/Admin/assets/pages/datatables.attestation.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/fluxbase.js"></script>
<script src="/vendor/xadmino/Admin/assets/js/app.js"></script>
<script>

//jquery for modal
$('.btnSendMail').click(function() {   
    $('#modalMail #idc').val($(this).data('idc'));
    $('#modalMail #year').val($(this).data('year'));
});

//hightlight the selected line
@isset($highlights)
@foreach($highlights as $highlight)
setLineLight('{{$highlight}}');
@endforeach
@endisset
</script>
@endsection