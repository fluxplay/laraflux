/**
 * Theme: Xadmino
 * Datatable
 */

!function($) {
    "use strict";

    var DataTable = function() {
        this.$dataTableButtons = $("#datatable-buttons")
    };
    DataTable.prototype.createDataTableButtons = function() {
        0 !== this.$dataTableButtons.length && this.$dataTableButtons.DataTable({
            dom: "Bfrtip",
            buttons: [{
                extend: "copy",
                className: "btn-success"
            }, {
                extend: "csv"
            }, {
                extend: "excel"
            }, {
                extend: "pdf"
            }, {
                extend: "print"
            }],
            responsive: !0
        });
    },
    DataTable.prototype.init = function() {
        //creating demo tabels
        //$('#datatable').dataTable();
        //$('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable({
            "oLanguage": {
                "sSearch": "Recherche...",
                "sLengthMenu": "Affiche _MENU_ enregistrement(s)",
              },
              "ordering": false,
              "pageLength": 100,
            //   "columnDefs": [ {
            //     "orderable": false,
            //     "className": 'select-checkbox',
            //     "targets":   0
            // } ],
            // "select": {
            //     "style":    'multi',
            //     "selector": 'td:first-child'
            // },
            // "order": [[ 1, 'asc' ]]
        } );
        // $('#datatable-scroller').DataTable({
        //     ajax: "assets/plugins/datatables/json/scroller-demo.json",
        //     deferRender: true,
        //     scrollY: 380,
        //     scrollCollapse: true,
        //     scroller: true
        // });
        //var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});

        //creating table with button
        //this.createDataTableButtons();
    },
    //init
    $.DataTable = new DataTable, $.DataTable.Constructor = DataTable
}(window.jQuery),

//initializing
function ($) {
    "use strict";
    $.DataTable.init();
}(window.jQuery);