<?php

namespace App;

class Period
{
    
    public $a_mois = array();
    public $a_year = array();
    private $lastM ;
    private $lasty ;
    
    function __construct()
    {
        $this->initMois();
        $this->initYear();
        $this->setMois(date('m'));
        $this->setYear(date('Y'));
    }

    public function getMois(){
        return $this->lastM;
    }
    public function getYear(){
        return $this->lastY;
    }
    public function setMois($mois)
    {
        if(!empty($mois)){
            $this->initMois();
            $this->lastM = $mois;
            $this->a_mois[$mois]['selected'] = true ;
        }
    }
    public function setYear($year)
    {
        if(!empty($year)){
            $this->initYear();
            $this->lastY = $year;
            $this->a_year[$year]['selected'] = true ;
        }
    }
    private function initMois()
    {
        $this->a_mois['01']['label'] = "JANVIER";
        $this->a_mois['01']['selected'] = 0 ;
        $this->a_mois['02']['label'] = "FEVRIER";
        $this->a_mois['02']['selected'] = 0 ;
        $this->a_mois['03']['label'] = "MARS";
        $this->a_mois['03']['selected'] = 0 ;
        $this->a_mois['04']['label'] = "AVRIL";
        $this->a_mois['04']['selected'] = 0 ;
        $this->a_mois['05']['label'] = "MAI";
        $this->a_mois['05']['selected'] = 0 ;
        $this->a_mois['06']['label'] = "JUIN";
        $this->a_mois['06']['selected'] = 0 ;
        $this->a_mois['07']['label'] = "JUILLET";
        $this->a_mois['07']['selected'] = 0 ;
        $this->a_mois['08']['label'] = "AOUT";
        $this->a_mois['08']['selected'] = 0 ;
        $this->a_mois['09']['label'] = "SEPTEMBRE";
        $this->a_mois['09']['selected'] = 0 ;
        $this->a_mois['10']['label'] = "OCTOBRE";
        $this->a_mois['10']['selected'] = 0 ;
        $this->a_mois['11']['label'] = "NOVEMBRE";
        $this->a_mois['11']['selected'] = 0 ;
        $this->a_mois['12']['label'] = "DECEMBRE";
        $this->a_mois['12']['selected'] = 0 ;
    }

    private function initYear()
    {
        $this->a_year['2010']['label'] = "2010";
        $this->a_year['2010']['selected'] = 0;
        $this->a_year['2011']['label'] = "2011";
        $this->a_year['2011']['selected'] = 0;
        $this->a_year['2012']['label'] = "2012";
        $this->a_year['2012']['selected'] = 0;
        $this->a_year['2013']['label'] = "2013";
        $this->a_year['2013']['selected'] = 0;
        $this->a_year['2014']['label'] = "2014";
        $this->a_year['2014']['selected'] = 0;
        $this->a_year['2015']['label'] = "2015";
        $this->a_year['2015']['selected'] = 0;
        $this->a_year['2016']['label'] = "2016";
        $this->a_year['2016']['selected'] = 0;
        $this->a_year['2017']['label'] = "2017";
        $this->a_year['2017']['selected'] = 0;
        $this->a_year['2018']['label'] = "2018";
        $this->a_year['2018']['selected'] = 0;
        $this->a_year['2019']['label'] = "2019";
        $this->a_year['2019']['selected'] = 0;
        $this->a_year['2020']['label'] = "2020";
        $this->a_year['2020']['selected'] = 0;
    }


}
