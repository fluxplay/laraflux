<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Artisan;

class Facture extends Model
{
    protected $primaryKey = 'num_facture'; // version 5.8
    protected $guarded = ['num_facture', 'id_facture'];
    public $timestamps = false;

    public function dateFr()
    {
        return implode('/',array_reverse  (explode('-',$this->date)));
    }
    public function getFactureArtisan()
    {
        if(!empty($this->factureprestation[0]->prestation->num_factureArtisan)){
            return  Facture::where('num_facture',$this->factureprestation[0]->prestation->num_factureArtisan)->first();
        }
    }
    public function getFactureClient()
    {
        if(!empty($this->factureprestation[0]->prestation->num_factureClient)){
            return  Facture::where('num_facture',$this->factureprestation[0]->prestation->num_factureClient)->first();
        }
    }
    public function associatedArtisan()
    {
        if(!empty($this->facturePrestation[0]->prestation->id_artisans)){
            return Artisan::where('id_artisans',$this->facturePrestation[0]->prestation->id_artisans)->first();
        }
    }
    public function associatedClient()
    {
        if(!empty($this->facturePrestation[0]->prestation->id_client)){
            return Client::where('id_client',$this->facturePrestation[0]->prestation->id_client)->first();
        }
    }

     /* - RELATIONSHIP - */
    public function client()
    {
        return $this->belongsTo('App\Client','id_client', 'id_client');
    }
    public function artisan()
    {
        return $this->belongsTo('App\Artisan','id_artisans', 'id_artisans');
    }
    public function facturePrestation()
    {
        return $this->hasMany('App\Factureprestation','num_facture', 'num_facture');
    }
}
