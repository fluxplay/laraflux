<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    public function __construct(array $attributes = [])
    {
        // $this->type = "client";
        // $this->level = "10";
        // $this->coops = "84";
        // $this->id_user = "11";
        parent::__construct($attributes);
    }

    public function getType(){
        return $this->type;
    }
    public function getIdUser(){
        return $this->id_user;
    }
    public function getLevel(){
        return $this->level;
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
