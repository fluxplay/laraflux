<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailtrapFacture extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($artisan_name, $id_facture)
    {
        $this->artisan_nom = $artisan_name;
        $this->id_facture = $id_facture;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('mail@example.com', 'Mailtrap')
            ->subject('Mailtrap Confirmation')
            ->markdown('mails.mail')
            ->with([
                'name' => 'New Mailtrap User',
                'link' => 'http://localhost:8082/public/pdf/'.$this->id_facture,
                'artisan' => $this->artisan_nom
            ]);
    }

}
