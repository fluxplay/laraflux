<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailAttestation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($clientName, $year)
    {
        //info
        // $this->to       = $info->to ; 
        // $this->from     = $info->from ; 
        // $this->subject  = $info->subject ; 
        // $this->title    = $info->title ; 
        //data
        $this->client = $clientName;
        $this->year = $year;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('attestation@upcv.com', 'UPCV')
            ->subject('Attestation '.$this->year)
            ->markdown('mails.attestation')
            ->with([
                'annee' => $this->year,
                'link' => env('APP_URL').'/public/attestations',
                'client' => $this->client
            ]);
    }

}
