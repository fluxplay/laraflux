<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $primaryKey = 'id_client'; // version 5.8
    protected $guarded = ['id_client'];
    public $timestamps = false;

    public function getClientName()
    {
      return $this->civilite." ".$this->nom." ".$this->prenom;
    }

    /* - RELATIONSHIP - */
    public function facture()
    {
      return $this->hasOne('App\Facture','id_client', 'id_client');
    }

    /* - STATIC - */
    public static function getStatusOptions(){
      return array('1' => 'Activé','0' => 'Désactivé');
    }
    public static function getOuiNonOptions(){
      return array('non' => 'Non', 'oui' => 'Oui');
    }
    public static function getCiviliteOptions(){
      return array('M' => 'M.', 'Mlle' => 'Mlle', 'Mme'=>'Mme', 'MetMme'=>'M et Mme');
    }    
}