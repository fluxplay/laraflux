<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestation extends Model
{
    protected $primaryKey = 'id_prestation'; // version 5.8
    protected $guarded = ['id_prestation'];
    public $timestamps = false;

    public function dateFr()
    {
        return implode('/',array_reverse  (explode('-',$this->date_prestation)));
    }

// +------------------------------------------------------+
// +----------------RELASHIONSHIP-------------------------+
// +------------------------------------------------------+
    public function client()
    {
        return $this->belongsTo('App\Client','id_client', 'id_client');
    }

    public function artisan()
    {
        return $this->belongsTo('App\Artisan','id_artisans', 'id_artisans');
    }

    public function factureprestation()
    {
        return $this->hasMany('App\Factureprestation','id_prestation', 'id_prestation');
    }

// +-------------------------------------------------+
// +---------------- STATIC -------------------------+
// +-------------------------------------------------+

    public static function getTypePaiementOptions()
    {
        return array('Chèque'=>'Cheque',
        'CB'=>'CB',
        'Izettle'=>'Izettle',
        // 'Numeraire'=>'Numeraire',
        'Virement'=>'Virement bancaire');
    }

    public static function getTvaNatureOptions()
    {
        return array('20.0'=>'20%',
        '10.0'=>'10%');
    }

    public static function getNatureOptions()
    {
        return array('jardinage'=>'Petits travaux de jardinage',
        'traiteur'=>'Préparation/Livraison de repas ',
        'informatique'=>'Assistance informatique',
        'bricolage'=>'Travaux de petit bricolage',
        'nettoyage'=>'Entretien de la maison et travaux ménagers',
        'administratif'=>'Assistance administrative ',
        'accompagnement'=>'Accompagnement d\'enfants + de 3 ans',
        'cours'=>'Cours à domicile');
    }

    public static function getClientMontantHT(Prestation $p, $montantTTC)
    {
        $i_montantHTClient=0;
        if(!empty($p->sz_tva)) // tva nature
        {
            $i_tva = (float) $p->sz_tva / 100 ;
            $i_tva = $i_tva + 1 ;
            $i_montantHTClient = (float) $montantTTC / $i_tva ;
        }else{
            $i_montantHTClient = (float) $montantTTC / 1.055 ;
        }
        return $i_montantHTClient;
    }
    
    public static function getTauxAndCommission(Prestation $p, $factureTTC)
    {
        $result = array();
        $i_taux =  floatval($p->sz_tva_artisan);
        $i_taux = $i_taux / 100 ;
        $i_taux = $i_taux + 1;
        
        // Recuperation du taux de commission pour cette prestation 
        $i_TauxPrestation   = (float)	$p->coeff / 100 ;
        $i_TauxPrestation   = $i_TauxPrestation + 1 ;

        $i_montantTTCArtisan = (float)	$factureTTC / $i_TauxPrestation ;
        if($p->typePayement=="Izettle" || $p->typePayement=="CB+1.75%")
        {
            $i_montantTTCArtisan 	= (float)	$i_montantTTCArtisan * 0.9825 ; //(1-0.0175)
        }
        $i_montantHTArtisan 	= (float)	$i_montantTTCArtisan / $i_taux ;
        $i_gainCoop 			= (float)	$factureTTC - $i_montantTTCArtisan  ;
        
        $result["i_montantTTCArtisan"] 	= $i_montantTTCArtisan;
        $result["i_montantHTArtisan"] 	= $i_montantHTArtisan;
        $result["i_gainCoop"] 	        = $i_gainCoop;
        return $result;
    }

}
