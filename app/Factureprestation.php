<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factureprestation extends Model
{
    protected $guarded = ['num_facture', 'id_prestation', 'type'];
    public $timestamps = false;
    
    /* - RELATIONSHIP - */
    public function prestation()
    {
        return $this->belongsTo('App\Prestation','id_prestation', 'id_prestation');
    }
    public function facture()
    {
        return $this->belongsTo('App\Facture','num_facture', 'num_facture');
    }

}
