<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artisan extends Model
{
    protected $primaryKey = 'id_artisans'; // version 5.8
    protected $guarded = [];
    public $timestamps = false;

// +------------------------------------------------------+
// +----------------RELASHIONSHIP-------------------------+
// +------------------------------------------------------+
    public function facture(){
        return $this->hasOne('App\Facture','id_artisans', 'id_artisans');
    }

// +-------------------------------------------------+
// +---------------- STATIC -------------------------+
// +-------------------------------------------------+
    public static function getSecNaturesArray($secNatures)
    {
        if(!empty($secNatures)){
            return explode(',', $secNatures);
        }else{ return array(); }
    }
    public static function getTvaOptions()
    {
        return array(
         '0.0'=>'0%',
        '20.0'=>'20%');
    }

    public static function getTauxOptions()
    {
        return array(
        '3.0'       =>'3.0%',
        '5.0'       =>'5.0%',
        '6.0'       =>'6.0%',
        '7.0'       =>'7.0%',
        '8.0'       =>'8.0%',
        '9.0'       =>'9.0%',
        '10.0'      =>'10.0%',
        '11.0'      =>'11.0%',
        '12.0'      =>'12.0%',
        '15.0'      =>'15.0%',
        '18.0'      =>'18.0%',
        '20.0'      =>'20.0%',
        '25.0'      =>'25.0%',
        '30.0'      =>'30.0%'
    );
    }

    public static function getTypeSociete()
    {
        return array(
            'sarl'=>'SARL',
            'eiae'=>'EI / AE',
            'eurl'=>'EURL', 
            'sas'=>'SAS');
    }
    public static function getStatusOptions(){
        
        return array('1' => 'Activé','0' => 'Désactivé');
    }
    public static function getStatusLabel($value=0){
        $tab = array('1' => 'Activé','0' => 'Désactivé');
        if($value==1)
        {
            return $tab[1];
        }else{
            return $tab[0];
        } 
    }

}
