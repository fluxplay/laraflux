<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
	function dateUStoFR($date,$separator='-')
	{
        $result = "";
        $a_date = explode($separator,$date);
        if(count($a_date)>2)
        {
            $result = $a_date[2].$separator.$a_date[1].$separator.$a_date[0];
        }
        return $result;
	}
	function dateFRtoUS($date,$separator='-')
	{
        $result = "";
        $a_date = explode($separator,$date);
        if(count($a_date)>2)
        {
            $result = $a_date[2].$separator.$a_date[1].$separator.$a_date[0];
        } 
        return $result;
	}
}
