<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artisan;

class ArtisanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artisans = Artisan::all();
        return view('artisans.index', compact('artisans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('artisans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->check($request);

        $artisan = new Artisan([
            'type'           =>  $request->get('se_type'),
            'nom'            =>  $request->get('sz_nom'),
            'contact'        => $request->get('sz_contact'),
            'adresse'        => $request->get('sz_adresse'),
            'cp'             => $request->get('sz_cp'),
            'ville'          => $request->get('sz_ville'),
            'siret'          => $request->get('sz_siret'),
            'tva'            => $request->get('se_tva'),
            'nature'         => $request->get('pri_nature'),
            'nature_sec'     => implode(",", $request->get('sec_nature')),
            'taux'           => $request->get('se_taux'),
            'telephone'      => $request->get('sz_tel'),
            'mail'           => $request->get('sz_email'),
            'adhesion'       => date("Y-m-d"),
            'logo'           => $request->get('sz_logo'),
            'facebook'       => $request->get('sz_facebook'),
            'linkedin'       => $request->get('sz_linkedin'),
            'siteweb'        => $request->get('sz_siteweb'),
            'actif'          => 1
        ]);

        if($artisan->save()){
            $status['text'] = "Felicitation!! l'artisan ".$artisan->nom." vient d'etre crée";
            $status['style'] = "success";
            $status['nom'] = $artisan->nom;
        }else{
            $status['text'] = "Une erreur est survenu et le nouvel artisan n\'a pas pu etre ajouté!";
            $status['style'] = "danger";
        }
        $artisans = Artisan::all();
        return view('artisans.index', compact('artisans', 'artisan'), ['login'=>'Gregory PEREZ', 'status' => $status]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artisan = Artisan::find($id);
        return view('artisans.edit', compact('artisan'));
    }

    public function search($search_nom)
    {
        $option="";
        $artisans = Artisan::where('nom', 'like', $search_nom . '%')->limit(10)->get();
        foreach($artisans as $artisan)
        {
            $option .= $artisan->id_artisans.":".$artisan->tva.":".$artisan->taux.":".$artisan->nom."@";
        }
        return $option;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->check($request);

        $artisan = Artisan::find($id);

        if(!empty($artisan->id_artisans) && $artisan->id_artisans>0){

            $artisan->type         = $request->get('se_type');
            $artisan->nom          = $request->get('sz_nom');
            $artisan->contact      = $request->get('sz_contact');
            $artisan->adresse      = $request->get('sz_adresse');
            $artisan->cp           = $request->get('sz_cp');
            $artisan->ville        = $request->get('sz_ville');
            $artisan->siret        = $request->get('sz_siret');
            $artisan->tva          = $request->get('se_tva');
            $artisan->telephone    = $request->get('sz_tel');
            $artisan->mail         = $request->get('sz_email');
            $artisan->nature       = $request->get('pri_nature');
            $artisan->nature_sec   = implode(",", $request->get('sec_nature'));
            $artisan->taux        = $request->get('se_taux');
            $artisan->siteweb     = $request->get('sz_siteweb');
            $artisan->facebook    = $request->get('sz_facebook');
            $artisan->linkedin    = $request->get('sz_linkedin');
            $artisan->logo        = $request->get('sz_logo');
            $artisan->actif       = $request->get('se_status');
            $artisan->save();
        }

        $artisans = Artisan::all();
        return view('artisans.index', compact('artisans'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function check(Request $request)
    {
        $request->validate([
            'se_type'=>'required',
            'sz_nom'=>'required',
            'sz_siret'=> 'required',
            'se_tva' => 'required',
            'se_taux' => 'required',
            'sz_adresse' => 'required',
            'sz_cp' => 'required',
            'sz_ville' => 'required',
            'sz_contact' => 'required',
            'pri_nature' => 'required'
        ],[
            'se_type.required'=>'Vous devez fournir un type de sociéte valide.',
            'sz_nom.required'=>'Vous devez fournir un nom de sociéte valide.',
            'sz_siret.required'=>'Vous devez fournir un numéro de SIRET valide.',
            'se_tva.required'=>'Une TVA valide doit etre appliquée.',
            'se_taux.required'=>'Un taux valide doit etre appliquée.',
            'sz_adresse.required'=>'Vous devez fournir une adresse de sociéte.',
            'sz_cp.required'=>'Vous devez fournir un code postal á la sociéte.',
            'sz_ville.required'=>'Vous devez fournir une ville valide á la sociéte.',
            'sz_contact.required'=>'Vous devez fournir un contact pour cette sociéte.',
            'pri_nature.required'=>'Au moins une nature de travaux doit etre appliqué.'
        ]);
    }

}