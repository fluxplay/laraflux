<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use PDO;

class DashboardController extends Controller
{
       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->connect();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::user()->getType()=="adm"){
            $nb_client    = $this->get_nbClient();
            $nb_artisan   = $this->get_nbArtisan();
            $client_top5  = $this->get_clientTop5(date('m'), '2019');
            $artisan_top5 = $this->get_artisanTop5(date('m'), '2019');
            $data_type_travaux_mois  = $this->get_typeTravaux_mois(date('m'), '2019');
            $data_type_travaux_annee = $this->get_typeTravaux_annee('2019');
            $data_type_travaux_tot   = $this->get_typeTravaux_tot('2019');
    
            return view('dashboard')
                ->with('nb_client_actif', json_encode($nb_client, JSON_NUMERIC_CHECK))
                ->with('nb_artisan_actif', json_encode($nb_artisan, JSON_NUMERIC_CHECK))
                ->with('nb_user_tot', json_encode($nb_artisan + $nb_client, JSON_NUMERIC_CHECK))
                ->with('client_top5', json_encode($client_top5, JSON_NUMERIC_CHECK))  
                ->with('artisan_top5', json_encode($artisan_top5, JSON_NUMERIC_CHECK)) 
                ->with('data_type_travaux_annee', json_encode($data_type_travaux_annee, JSON_NUMERIC_CHECK))  
                ->with('data_type_travaux_mois', json_encode($data_type_travaux_mois, JSON_NUMERIC_CHECK))
                ->with('data_type_travaux_tot', json_encode($data_type_travaux_tot, JSON_NUMERIC_CHECK));

        }else if(Auth::user()->getType()=="artisan"){
            return view('artisans.dashboard');
        }else if(Auth::user()->getType()=="client"){
            return view('clients.dashboard');
        }else{
            return view('404');
        }
    }


    private function connect()
    {
        $dbame = "fluxplayweb";
        $host = '172.16.1.57';
        $user = 'root';
        $pass = 'myp';
        $charset = 'utf8mb4';

        $dsn = "mysql:host=$host;dbname=$dbame;charset=$charset";

        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $nb = array();
        try {
            $this->db = new PDO($dsn, $user, $pass, $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    public function get_nbClient()
    {
        $stmt = $this->db->query("select count(*) as nb_client from client where actif='1'");
        $row = $stmt->fetch();
        return $row['nb_client'];
    }
    
    public function get_nbArtisan()
    {
        $stmt = $this->db->query("select count(*) as nb_artisan from artisans where actif='1'");
        $row = $stmt->fetch();
        return $row['nb_artisan'];
    }

    public function get_artisanTop5($mois, $year)
    {
        $first=true;
        $artisan_top5 = array();
        $dto = date("Y-m-d",mktime(0, 0, 0, $mois+1,1,$year));
        $dfr = date("Y-m-d",mktime(0, 0, 0, $mois,1,$year));

        $query = "select artisans.nom as artisan, count(id_prestation) as nb_prestation from prestation 
        left join artisans on prestation.id_artisans=artisans.id_artisans 
        where date_prestation>'$dfr' and date_prestation<'$dto'
        group by prestation.id_artisans order by nb_prestation DESC limit 5";

        try {

            $stmt = $this->db->query($query);
            while ($row = $stmt->fetch())
            {
                if($first){
                    array_push($artisan_top5, 
                        array(
                            'label'=>$row['artisan'], 
                            'y'=>$row['nb_prestation'],
                            'exploded'=>true,
                            ));
                    $first=false;
                }else{
                    array_push($artisan_top5, 
                        array(
                            'label'=>$row['artisan'], 
                            'y'=>$row['nb_prestation'],
                            ));
                }
            }
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
        return $artisan_top5;
    }

    public function get_clientTop5($mois, $year)
    {
        $first=true;
        $client_top5 = array();
        $dto = date("Y-m-d",mktime(0, 0, 0, $mois+1,1,$year));
        $dfr = date("Y-m-d",mktime(0, 0, 0, $mois,1,$year));
        $query = "select CONCAT(client.nom,' ',client.prenom) as client, count(id_prestation) as nb_prestation from prestation 
        left join client on prestation.id_client=client.id_client 
        where date_prestation>'$dfr' and date_prestation<'$dto'
        group by prestation.id_client order by nb_prestation DESC limit 5";

        try {

            $stmt = $this->db->query($query);
            while ($row = $stmt->fetch())
            {
                    if($first){
                        array_push($client_top5, 
                            array(
                                'label'=>$row['client'], 
                                'y'=>$row['nb_prestation'],
                                'exploded'=>true,
                            ));
                            $first=false;
                    }else{
                        array_push($client_top5, 
                            array(
                                'label'=>$row['client'], 
                                'y'=>$row['nb_prestation']
                            ));
                    }
            }

        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
        return $client_top5;
    }
/*$data_type_travaux_annee = array(
	array("label"=> "jardiage", "y"=> 50),
	array("label"=> "nettoyage", "y"=> 25),
    array("label"=> "traiteur", "y"=> 15),
    array("label"=> "administratif", "y"=> 10),
    array("label"=> "cours", "y"=> 5)
);*/

    public function get_typeTravaux_mois($mois,$year)
    {
        $result = array();
        $dto = date("Y-m-d",mktime(0, 0, 0, $mois+1,1,$year));
        $dfr = date("Y-m-d",mktime(0, 0, 0, $mois,1,$year));
        $query = "select type, count(*) nb_prestation from prestation 
        where date_prestation>'$dfr' and date_prestation<'$dto'
        group by type;";
        try {

            $stmt = $this->db->query($query);
            while ($row = $stmt->fetch())
            {
                array_push($result, 
                    array(
                        'label'=>$row['type'], 
                        'y'=>$row['nb_prestation']
                    ));
            }

        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
        return $result;
    }

    public function get_typeTravaux_annee($year)
    {
            $result = array();
            $dto = date("Y-m-d",mktime(0, 0, 0, 1, 1, $year+1));
            $dfr = date("Y-m-d",mktime(0, 0, 0, 1, 1, $year));
            $query = "select type, count(*) nb_prestation from prestation 
            where date_prestation>'$dfr' and date_prestation<'$dto'
            group by type;";
            try {
    
                $stmt = $this->db->query($query);
                while ($row = $stmt->fetch())
                {
                    array_push($result, 
                        array(
                            'label'=>$row['type'], 
                            'y'=>$row['nb_prestation']
                        ));
                }
    
            } catch (\PDOException $e) {
                throw new \PDOException($e->getMessage(), (int)$e->getCode());
            }
            return $result;
    }

    public function get_typeTravaux_tot($year)
    {
        $types = array();
        $dto = date("Y-m-d",mktime(0, 0, 0, 1, 1, $year));
        $dfr = date("Y-m-d",mktime(0, 0, 0, 1, 1, $year-5));

        $years = array();
        for($i=intval($year-5); $i<intval($year); $i++){
            $years[]=$i;
        }
        
        $result = array();
        $query = "select type, count(*) nb_prestation, DATE_FORMAT(date_prestation, '%Y') as prest_year from prestation 
        where date_prestation>'$dfr' and date_prestation<'$dto'
        group by type, prest_year order by prest_year;";
        try 
        {
            $stmt = $this->db->query($query);
            while ($row = $stmt->fetch())
            {
                $types[$row['type']][$row['prest_year']] = $row['nb_prestation'];
            }

            foreach($types as $key => $type)
            {
                foreach($years as $year)
                {
                    if(!isset($result[$key]))
                    {
                        $result[$key] = array();
                    }
                    if(isset($type[$year])){
                        array_push($result[$key], array(
                            'label'=>"$year", 
                            'y'=>$type[$year]
                        ));
                    }else{
                        array_push($result[$key], array(
                            'label'=>"$year", 
                            'y'=>0
                        ));
                    }
                }
            }
        } catch (\PDOException $e) 
        {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
        return $result;
    }

    public function getgh()
    {
        $nb_client    = $this->get_nbClient();
		$nb_artisan   = $this->get_nbArtisan();
		$client_top5  = $this->get_clientTop5(date('m'), '2019');
		$artisan_top5 = $this->get_artisanTop5(date('m'), '2019');
        $data_type_travaux_mois  = $this->get_typeTravaux_mois(date('m'), '2019');
		$data_type_travaux_annee = $this->get_typeTravaux_annee('2019');
        $data_type_travaux_tot   = $this->get_typeTravaux_tot('2019');

        return view('gh')
            ->with('nb_client_actif', json_encode($nb_client, JSON_NUMERIC_CHECK))
            ->with('nb_artisan_actif', json_encode($nb_artisan, JSON_NUMERIC_CHECK))
            ->with('nb_user_tot', json_encode($nb_artisan + $nb_client, JSON_NUMERIC_CHECK))
            ->with('client_top5', json_encode($client_top5, JSON_NUMERIC_CHECK))  
            ->with('artisan_top5', json_encode($artisan_top5, JSON_NUMERIC_CHECK)) 
            ->with('data_type_travaux_annee', json_encode($data_type_travaux_annee, JSON_NUMERIC_CHECK))  
            ->with('data_type_travaux_mois', json_encode($data_type_travaux_mois, JSON_NUMERIC_CHECK))
            ->with('data_type_travaux_tot', json_encode($data_type_travaux_tot, JSON_NUMERIC_CHECK));

    }








    
}
