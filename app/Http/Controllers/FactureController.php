<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Facture;
use App\Factureprestation;
use App\Period;
use App\Client;
use Illuminate\Support\Facades\DB;
// Mail
use App\Mail\MailtrapFacture;
use Illuminate\Support\Facades\Mail;

class FactureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getFactures()
    {
        if(Auth::user()->getType()=="artisan")
        {
            $factures = Facture::where('typeFacture','Artisan')->where('id_artisans', Auth::user()->id_user)->get();
            return view('artisans.recapitulatif', compact('factures'));

        }else if(Auth::user()->getType()=="client"){
            $factures = Facture::where('typeFacture','Client')->where('id_client', Auth::user()->id_user)->get();
            return view('clients.factures', compact('factures'));
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getFacturesArtisan()
    {
        if(Auth::user()->getType()=="adm"){
            $factures = Facture::where('typeFacture','Artisan')->get();
            return view('artisans.factures', compact('factures'));
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getFacturesClient()
    {
        if(Auth::user()->getType()=="adm"){
            $factures = Facture::where('typeFacture','Client')->get();
            return view('clients.factures', compact('factures'));
        }
    }

    public function getRecapitulatifs(Request $request)
    {
        $status   = array();
        $o_period = new Period();

        if(!empty($request->get('sz_mois') && !empty($request->get('sz_year'))))
        {
            $o_period->setYear($request->get('sz_year'));
            $o_period->setMois($request->get('sz_mois'));
        }

        if( Auth::user()->getType()=="adm" )
        {
            if(empty($request->get('id_client')) === false)
            {
                $client  = Client::find($request->get('id_client'));
                if(empty($client->id_client) === false)
                {
                    // Fetch facture using the id_client.
                    $factures = Facture::where('typeFacture','Client')
                    ->where('id_client', $client->id_client)
                    ->orderBy('id_client','DESC')
                    ->get();

                }else{
                    
                    $status['text'] = array();
                    $status['text'][]   = "Le client avec l'identifiant <b>".$request->get('id_client')."</b> n'existe pas!";
                    $status['style']    = "warning";
                }

            }else{
                
                // Fetch facture using the current month, year.
                $factures = Facture::where('typeFacture','Client')
                ->whereMonth('date', $o_period->getMois())
                ->whereYear('date',  $o_period->getYear())
                ->orderBy('id_client','DESC')
                ->get();
            }
            
            // Organise the data to display
            $tab_recapitulatif = $this->createRecapitulatifFetchArtisan($factures);

            return view('clients.recapitulatif')->with('tab_recapitulatif', $tab_recapitulatif)
            ->with('status',$status)
            ->with('o_period', $o_period);

        }else if(Auth::user()->getType()=="artisan")
        {
            $id_artisan = Auth::user()->getIdUser();
        
            if(empty($id_artisan) === false )
            {
                if(empty($request->get('id_client')) === false )
                {
                    $client = Client::find($request->get('id_client'));
                    if(empty($client->id_client) === false)
                    {
                        // Fetch facture using the id_client and fir the corresponding artisan.
                        $factures = Facture::where('typeFacture','Client')
                        ->where('id_client', $client->id_client)
                        ->where('id_artisan', $id_artisan)
                        ->orderBy('id_client','DESC')
                        ->get();
    
                    }else{
                        $status['text'] = array();
                        $status['text'][]   = "Le client avec l'identifiant <b>".$request->get('id_client')."</b> n'existe pas!";
                        $status['style']    = "warning";
                    }
    
                }else{
                    // Fetch facture using the current month, year for the artisan.
                    $factures = Facture::where('typeFacture','Artisan')
                    ->where('id_artisans', $id_artisan)
                    ->whereMonth('date', $o_period->getMois())
                    ->whereYear('date',  $o_period->getYear())
                    ->orderBy('id_client','DESC')
                    ->limit(25)
                    ->get();
                }
                // Organise the data to display
                $tab_recapitulatif = $this->createRecapitulatifFetchClient($factures);

            }else{
                $status['text'] = array();
                $status['text'][]   = "L'identifiant de l'artisan <b>".$id_artisan."</b> n'existe pas!";
                $status['style']    = "danger";

                $tab_recapitulatif = array();
            }
            
            return view('artisans.recapitulatif')
                ->with('tab_recapitulatif', $tab_recapitulatif)
                ->with('status',$status)
                ->with('o_period', $o_period);
        
        }else if(Auth::user()->getType()=="Client")
        {
            
        }
    }


    public function fMapPrestationToFacture($id_facture, $id_prestation)
    {
		$sz_query = "UPDATE prestation SET num_factureArtisan='".$aa_NumFacture['artisan']."', num_factureClient='".$aa_NumFacture['client']."' ";
        $sz_query .= "WHERE id_prestation='".$i_prestation."'" ;
        

    }

    public function sendfacture(Request $request)
    {
        $status = array();
        $status['text'] = array();

        $o_period = new Period();

        if(Auth::user()->getType()=="adm")
        {
            if(!empty($request->get('idfact'))
            && !empty($request->get('idclient')) 
            && !empty($request->get('nomartisan'))
            && !empty($request->get('mois'))
            && !empty($request->get('year')))
            {
                $client  = Client::find($request->get('idclient'));
                if(!empty($client->id_client) && !empty($client->email))
                {
                    Mail::to($client->email)->send(new MailtrapFacture($request->get('nomartisan'), $request->get('idfact'))); 
                    $status['text'][]   = "La facture ".$request->get('idfact')." a bien été envoyé par mail au client";
                    $status['style']    = "success";
                }else{
                    $status['text'][] = "Le client n'a pas fournit d'adresse mail, <a href='".env('APP_URL')."/public/pdf/".$request->get('idfact')."'>veuillez imprimer la facture.</a>";
                    $status['style']  = "danger";
                }
            }else{
                $status['text'][]   = "La facture n'a pas pu etre envoyée car des données sont manquantes!";
                $status['style']    = "danger";
            }
            //Enregistrement de la nouvelle date d'edition.
            $facture = Facture::find($request->get('idfact'));
            if(!empty($facture->date_edition)){
                $facture->date_edition = date('Y-m-d');
                $facture->save();
            }

            $o_period = new Period();
            $o_period->setYear($request->get('year'));
            $o_period->setMois($request->get('mois'));

            $factures = Facture::where('typeFacture','Client')
            ->whereMonth('date', $o_period->getMois())
            ->whereYear('date',  $o_period->getYear())
            ->orderBy('id_client','DESC')
            ->get();

            // factorisation de la creation du tableau recapitulatif a afficher
            $tab_recapitulatif = $this->createRecapitulatifFetchArtisan($factures);

            $highlights = array();
            $highlights[] = $request->get('idfact');

            return view('clients.recapitulatif')->with('tab_recapitulatif', $tab_recapitulatif)
                ->with('o_period', $o_period)
                ->with('status',$status)
                ->with('highlights',$highlights);

        }else if(Auth::user()->getType()=="client")
        {

        }else if(Auth::user()->getType()=="artisan")
        {

        }else{

        }

    }
    public function search($num_facture)
    {
        if(!empty($num_facture))
        {
            $facture = Facture::where('typeFacture','Client')
            ->where('num_facture', $num_facture)
            ->first();

            $tab = array();
            if(!empty($facture->num_facture))
            {
                foreach($facture->facturePrestation as $fp){
                    if(!empty($fp->id_prestation))
                    {
                        $tab[] = $fp->prestation;
                    }
                }
            }
            return json_encode($tab,JSON_PRETTY_PRINT);
        }
    }

    public function createRecapitulatifFetchArtisan($factures)
    {
        $tab_recapitulatif = array();
        foreach($factures as $facture)
        {
            $tab_recapitulatif[$facture->num_facture]['id_facture']     = $facture->id_facture;
            $tab_recapitulatif[$facture->num_facture]['num_facture']    = $facture->num_facture;
            $tab_recapitulatif[$facture->num_facture]['mail']           = $facture->client->email; 
            $tab_recapitulatif[$facture->num_facture]['client']         = $facture->client->nom . " " . $facture->client->prenom ;
            $tab_recapitulatif[$facture->num_facture]['id_client']      = $facture->client->id_client;
            $tab_recapitulatif[$facture->num_facture]['cmontantTTC']    = $facture->montantTTC;
            $tab_recapitulatif[$facture->num_facture]['date']           = $facture->dateFr();

            if(!empty($facture->facturePrestation))
            {
                $tab_recapitulatif[$facture->num_facture]['aid_facture']    = "N/A";
                $tab_recapitulatif[$facture->num_facture]['anum_facture']   = "N/A";
                $tab_recapitulatif[$facture->num_facture]['id_artisans']    = "N/A";
                $tab_recapitulatif[$facture->num_facture]['artisan']        = "N/A";
                $tab_recapitulatif[$facture->num_facture]['amontantTTC']    = "N/A";

                $artisan_facture = $facture->getFactureArtisan(); 
                if(!empty($artisan_facture)){
                    $tab_recapitulatif[$facture->num_facture]['aid_facture']    = $artisan_facture->id_facture;
                    $tab_recapitulatif[$facture->num_facture]['anum_facture']   = $artisan_facture->num_facture;
                    $tab_recapitulatif[$facture->num_facture]['id_artisans']    = $artisan_facture->artisan->id_artisans;
                    $tab_recapitulatif[$facture->num_facture]['artisan']        = $artisan_facture->artisan->nom;
                    $tab_recapitulatif[$facture->num_facture]['amontantTTC']    = $artisan_facture->montantTTC;
                }
            }
        }
        return $tab_recapitulatif;
    }

    public function createRecapitulatifFetchClient($factures)
    {
        $tab_recapitulatif = array();
        foreach($factures as $facture)
        {
            $tab_recapitulatif[$facture->num_facture]['id_facture']     = $facture->id_facture;
            $tab_recapitulatif[$facture->num_facture]['num_facture']    = $facture->num_facture;
            $tab_recapitulatif[$facture->num_facture]['mail']           = $facture->artisan->mail;
            $tab_recapitulatif[$facture->num_facture]['artisan']        = $facture->artisan->nom;
            $tab_recapitulatif[$facture->num_facture]['id_artisans']    = $facture->artisan->id_artisans;
            $tab_recapitulatif[$facture->num_facture]['amontantTTC']    = $facture->montantTTC;
            $tab_recapitulatif[$facture->num_facture]['date']           = $facture->dateFr();

            if(!empty($facture->facturePrestation))
            {
                $tab_recapitulatif[$facture->num_facture]['cid_facture']    = "N/A";
                $tab_recapitulatif[$facture->num_facture]['cnum_facture']   = "N/A";
                $tab_recapitulatif[$facture->num_facture]['id_client']      = "N/A";
                $tab_recapitulatif[$facture->num_facture]['client']         = "N/A";
                $tab_recapitulatif[$facture->num_facture]['cmontantTTC']    = "N/A";

                $client_facture = $facture->getFactureClient();
                if(!empty($client_facture)){
                    $tab_recapitulatif[$facture->num_facture]['cid_facture']  = $client_facture->id_facture;
                    $tab_recapitulatif[$facture->num_facture]['cnum_facture'] = $client_facture->num_facture;
                    $tab_recapitulatif[$facture->num_facture]['id_client']    = $client_facture->client->id_client;
                    $tab_recapitulatif[$facture->num_facture]['client']       = $client_facture->client->nom . " " . $client_facture->client->prenom;
                    $tab_recapitulatif[$facture->num_facture]['cmontantTTC']  = $client_facture->montantTTC;
                }
            }
        }
        return $tab_recapitulatif;
    }

    public function fGetLastNumFacture($type="client")
	{
		//Récuperation du nombre de facture"
        $facture = Facture::where('typeFacture',$type)
        ->orderBy('num_facture','DESC')
        ->first();

        $i_idFacture = 1 ;
        if(!empty($facture->id_facture)){
            $a_idFacture = explode('/',$facture->id_facture);
            $i_idFacture = intval($a_idFacture[1]);
            $i_idFacture++ ;
        }
        return $i_idFacture ;
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
