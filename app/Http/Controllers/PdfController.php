<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Prestation;
use App\Facture;

include( app_path() . '/Libraries/fpdf/fpdf.php' );
include( app_path() . '/Libraries/fpdf/fpdi.php' );

class PdfController extends Controller
{
    private $pdf;
    private $mode;
    private $a_cooperative = array();
    private $docs_folder="/Docs";

    public function __construct()
    {
        $this->middleware('auth');
        $this->o_pdf = new \FPDI();
        
    }
    public function fSetMode($l_mode)
    {
        $this->mode=$l_mode;
    }

    public function readAttestation($idClient, $year=null)
    {
        if($year===null){$year = date('Y');}
        $this->fSetFolder(Auth::user()->coops);

        $lYear      = $year;
        $lYearPlus  = $lYear + 1;
        $dateFrom   = $lYear.'-01-01';
        $dateTo     = $lYearPlus.'-01-01';
        if(Auth::user()->getType()==="adm")
        {
            $prestations = Prestation::where('id_client', $idClient)
            ->whereBetween('date_prestation', [$dateFrom, $dateTo])
            ->orderBy('date_prestation','DESC')
            ->get();

            $this->fGeneAttestation($prestations, $lYear);

        }else if(Auth::user()->getType()==="client"){

        }else if(Auth::user()->getType()==="artisan"){

        }
    }


    public function read($num_facture)
    {
        //Authorization 
//        if(Auth::user()->getType()!="adm")
//        {
//            return view('errors.404',['txt' => 'Vous n\'avez pas access a cette page!']);
//        }
        //Configuration and type of facture
        $this->fSetFolder(Auth::user()->coops);
        if(!empty($num_facture) && is_numeric($num_facture))
        {
            $facture = Facture::where('num_facture',$num_facture)->first();
            if(!empty($facture->typeFacture))
            {
                if($facture->typeFacture=="Client")
                {
                    $this->fSetMode("client");
                }else if($facture->typeFacture=="Artisan")
                {
                    $this->fSetMode("artisan");
                }else{
                    return view('errors.404',['txt' => 'Ce numero de facture n\'est pas valide']);
                }
                // facture generation for client and artisan type of facture. 
                $this->fGenereFacture($facture);
            }
        }else{
            return view('errors.404',['txt' => 'Ce numero de facture n\'est pas valide']);
        }
    }

    public function fGeneAttestation($prestations, $year)
    {
        $cesu		 				= false ;
		$nb_prestation_total 		= 0 	;
        $Montant_total		 		= 0 	;
		foreach($prestations as $prestation)
		{
			$total = number_format($prestation->montant + $prestation->montantCESU,2,'.','') ;
			$date = date("d/m/Y", strtotime($prestation->date_prestation)) ;
			
			if($prestation->montantCESU>0) $aa_client[$prestation->client->id_client]['cesu'] = true ;
			else $aa_client[$prestation->client->id_client]['cesu'] = false ;

			$aa_preststaion[$prestation->client->id_client][$nb_prestation_total]['typePayement']	= $prestation->typePayement;
			$aa_preststaion[$prestation->client->id_client][$nb_prestation_total]['cesu'] 			= $prestation->montantCESU;
			$aa_preststaion[$prestation->client->id_client][$nb_prestation_total]['montant'] 		= $prestation->montant;
			$aa_preststaion[$prestation->client->id_client][$nb_prestation_total]['date'] 			= $prestation->date_prestation;
			$aa_preststaion[$prestation->client->id_client][$nb_prestation_total]['id_prestation']	= $prestation->id_prestation;
			$aa_preststaion[$prestation->client->id_client][$nb_prestation_total]['type'] 			= $prestation->type;
			$aa_preststaion[$prestation->client->id_client][$nb_prestation_total]['date'] 			= $date;
			$aa_preststaion[$prestation->client->id_client][$nb_prestation_total]['total'] 			= $total;
			//Récupere les données du client
			$aa_client[$prestation->client->id_client]['civilite'] 		= $prestation->client->civilite;
			$aa_client[$prestation->client->id_client]['nom'] 			= $prestation->client->nom;
			$aa_client[$prestation->client->id_client]['prenom'] 		= $prestation->client->prenom;
			$aa_client[$prestation->client->id_client]['ville'] 		= $prestation->client->ville;
			$aa_client[$prestation->client->id_client]['cp'] 			= $prestation->client->cp;
			$aa_client[$prestation->client->id_client]['complement'] 	= $prestation->client->adresse2;
			$aa_client[$prestation->client->id_client]['adresse'] 		= $prestation->client->adresse1;
			$aa_client[$prestation->client->id_client]['id'] 			= $prestation->client->id_client;
			//D'autres données 
            $aa_client[$prestation->client->id_client]['attest_edition'] 	= $prestation->client->attest_edition;
            if(!isset($aa_client[$prestation->client->id_client]['total'])){
                $aa_client[$prestation->client->id_client]['total'] = 0;
            }else{
                $aa_client[$prestation->client->id_client]['total'] 			+= $prestation->montant;
            }
			$nb_prestation_total++;
        }//foreach

        foreach($aa_client as $id_client)
        {
            if($id_client['cesu'])
            {
                $nb_prestation_max	= 8 ;
            }else{
                $nb_prestation_max 	= 10 ;
            }
            
            $nb_current_prestation 		= 0 ;
            $id_client['nb_prestation']	= 0 ;
            $b_need_new_page 			= true ;
            
            foreach ($aa_preststaion[$id_client['id']] as $a_pres)
            {
                if($b_need_new_page===true)
                {
                    $this->o_pdf->AddPage();
                    $this->o_pdf->setSourceFile(app_path().$this->docs_folder."/attestation_".$this->idFolder.".pdf"); 
                    $tplIdx = $this->o_pdf->importPage(1); 
                    $this->o_pdf->useTemplate($tplIdx); 
                
                    $this->o_pdf->Cell(80);
                    $this->o_pdf->Ln(20);
                    $this->o_pdf->SetFont('Arial','B',10);
                    $this->o_pdf->Write(5,'Coopérative Un PRO Chez Vous '.$this->idFolder);
                    $this->o_pdf->Ln();
                    $this->o_pdf->SetFont('Arial','',10);

                    //Création de l'entête de la cooperative.
                    $tabCoop = array($this->a_cooperative['adresse'],
                    $this->a_cooperative['ville'],
                    "Siret: ".$this->a_cooperative['siret'],
                    "Agrément : ".$this->a_cooperative['agrement']
                    );
                    $this->BasicCadre($tabCoop,10,0,5);
                    $this->o_pdf->SetFont('Arial','B',10);
                    $this->o_pdf->Write(5,'3620 dites "UN PRO CHEZ VOUS"'); $this->o_pdf->Ln();
                    $this->o_pdf->Write(5,'http://www.unprochezvous.com'); $this->o_pdf->Ln();
                    $this->o_pdf->SetFont('Arial','',10);
                    // Adresse du client
                    $civilite = !empty($id_client['civilite']) ? $id_client['civilite'] : "M.";
                    $ville = $id_client['cp']." ".$id_client['ville'] ;
                    if(empty($id_client['complement']))
                    {
                        $tabClient = array($civilite." ".$id_client['nom']." ".$id_client['prenom'],
                                            $id_client['adresse'],
                                            $ville
                                            ) ;
                    }else{
                        $tabClient = array($civilite." ".$id_client['nom']." ".$id_client['prenom'],
                                            $id_client['adresse'],$id_client['complement'],
                                            $ville
                                            ) ;
                    }
                    //utilisé pour le nom du fichier attestation.
                    $l_nom 		= $id_client['nom'] ;
                    $l_prenom 	= $id_client['prenom'];
                    $this->BasicCadre($tabClient,120,0,5);
                    $this->o_pdf->Write(5,"Attestation éditée le 31/01/".date("Y")  );
                    $this->o_pdf->SetFont('Arial','B',14);
                    $this->o_pdf->Ln(15);
                    $this->o_pdf->Cell(35);
                    $this->o_pdf->write(8,"Attestation fiscale pour la déclaration de l'année ".$year);$this->o_pdf->Ln();
                    $this->o_pdf->Ln();
                    //nature des services fournie
                    $this->o_pdf->SetFont('Arial','U',10);
                    $this->o_pdf->write(8,"Nature, montant et modalités de règlements des services fournis :");
                    $this->o_pdf->Ln();
                    $this->o_pdf->SetFont('Arial','',10);
                    $b_need_new_page = false ;
                }//b_need_new_page
                if($a_pres['montant']>0 || $a_pres['cesu']>0)
                {
                    $mention  =  $this->sz_getTypePrestation($a_pres['type']);
                    $mention .=  " du ".$a_pres['date']." : ".$a_pres['total']." ".utf8_encode(chr(128))." ";
                
                    //patch pour franck
                    if( $this->idFolder == "84" && $a_pres['typePayement'] == "CB")
                    {
                        $a_pres['typePayement'] = "Cheque";
                    }
                
                    if($a_pres['cesu']>0 && $a_pres['montant']>0)
                    {
                        $mention .= " (".$a_pres['typePayement']." : ".$a_pres['montant'];
                        $mention .= " + CESU : ".$a_pres['cesu']." ".utf8_encode(chr(128));
                        $cesu=true;
                    }elseif ($a_pres['cesu']>0 && $a_pres['montant']==0){// CESU uniquement
                        $mention .= " (CESU : ".$a_pres['cesu']." ".utf8_encode(chr(128)).")";
                        $cesu=true;
                    }elseif ($a_pres['cesu']==0 && $a_pres['montant']>0){
                        $mention .= " (".$a_pres['typePayement']." : ".$a_pres['montant']." ".utf8_encode(chr(128)).")";}
                }else{
                    $this->o_pdf->write(5,"[ERROR]--->id_prestation : ".$a_pres['id_prestation']);
                }
                $this->o_pdf->write(4,$mention);
                $this->o_pdf->Ln();
                $nb_current_prestation++;
                $id_client['nb_prestation']++;
                if($nb_current_prestation >= $nb_prestation_max)
                {
                    $b_need_new_page 		= true ;
                    $nb_current_prestation	= 0 ;			
                }
                $this->o_pdf->write(3,'');
                $this->o_pdf->Ln(2);	
            }//foreach prestation
            if($id_client['cesu'])
            {
                $this->o_pdf->SetFont('Arial','',8);
                $this->o_pdf->write(4,"La partie du CESU,  préfinancée par l'employeur, est exonérée d'impôt. Seule la partie autofinancée par le bénéficiaire du CESU ouvre droit à la réduction d'impôt de l'article 199  sexdecies du Code général des impôts (cf. article L129-15 du Code du travail). La distinction des montants sera portée sur l'attestation émise par l'employeur à son salarié bénéficiaire en vue de la déclaration fiscale annuelle.");
                $this->o_pdf->Ln(4);
                $this->o_pdf->SetFont('Arial','',8);
            }
            //Nombre totale d’intervention
            if($nb_current_prestation>0 && ($nb_prestation_max - $nb_current_prestation) > 0)
            {
                $this->o_pdf->Ln($nb_prestation_max - $nb_current_prestation);
            }
            $this->o_pdf->SetFont('Arial','U',8);
            $this->o_pdf->write(8,"Nombre totale d'interventions :");
            $this->o_pdf->SetFont('Arial','',8);
            $this->o_pdf->write(8,"    ".$id_client['nb_prestation']);
            $this->o_pdf->Ln();	
            //Montant total des prestations
            $this->o_pdf->SetFont('Arial','BU',10);
            $mention_finale  = "Montant total des prestations ouvrant droit à crédit d'impôt* : ";
            $mention_finale .= number_format($id_client['total'],2,'.','')." ".utf8_encode(chr(128));
            $this->o_pdf->write(8, $mention_finale);
            $this->o_pdf->Ln();
            $this->o_pdf->write(8,"");
        }//foreachclient
        // mot de passe bloc
        $this->o_pdf->SetFont('Arial','',10);
		//$this->o_pdf->Ln();
		$this->o_pdf->Write(5,"Retrouvez vos factures et attestations directement sur notre site: ");
		$this->o_pdf->SetFont('Arial','B',10);
		$this->o_pdf->Write(5,"http://fluxplay.fr/".$this->idFolder."/");
		$this->o_pdf->SetFont('Arial','',10);
		$this->o_pdf->Ln();
		$this->o_pdf->Write(5,"Votre nom d'utilisateur est: ");
		$this->o_pdf->SetFont('Arial','B',10);
		$this->o_pdf->Write(5,'fake login');
		$this->o_pdf->SetFont('Arial','',10);
		$this->o_pdf->Write(5," et votre mot de passe est: ");
		$this->o_pdf->SetFont('Arial','B',10);
		$this->o_pdf->Write(5,'fake password'); $this->o_pdf->Ln();
        $this->o_pdf->SetFont('Arial','',10);
        

        $this->o_pdf->Output();
        exit;
    }

    private function fGenereFacture(Facture $facture) 
    {
        switch($this->mode)
        {
            case "client":      $this->fShowFactureClient($facture);break;
            case "artisan":     $this->fShowFactureArtisan($facture);break;
            case "mailclient":  $this->fSendFactureClientByEmail($facture);break;
        }
    }
    
    public function fSendFactureClientByEmail(Facture $facture)
    {
    }
    
    public function fShowFactureArtisan(Facture $facture_artisan)
    {
        $this->o_pdf->AliasNbPages();
        $this->o_pdf->AddPage();
        $this->o_pdf->SetFont('Arial','B',20);
        $this->o_pdf->Cell(80);
        $this->o_pdf->Ln(15);
        $this->o_pdf->SetFont('Times','',10);
        $this->o_pdf->Ln();
        //Création de l'entête de l'artisan.
        if(empty($facture_artisan->artisan->complement))
        {
            $tabArtisan = array( $facture_artisan->artisan->nom,
                                $facture_artisan->artisan->adresse,
                                $facture_artisan->artisan->ville
                                ) ;
        }else{
            $tabArtisan = array( $facture_artisan->artisan->nom,
                                $facture_artisan->artisan->adresse,
                                $facture_artisan->artisan->complement,
                                $facture_artisan->artisan->ville
                                ) ;
        }
        //Création de l'entête du client.
        $associated_client = $facture_artisan->associatedClient();
        if(!empty($associated_client->civilite))
        {
            $sz_civilite = $associated_client->civilite;
        }else{
            $sz_civilite = "M.";
        }

        if(empty($associated_client->adresse2))
        {
            $tabClient = array(     $sz_civilite." ".$associated_client->nom." ".$associated_client->prenom,
                                    $associated_client->adresse1,
                                    $associated_client->cp." ".strtoupper($associated_client->ville)
                                ) ;
        }else{
            $tabClient = array(     $sz_civilite." ".$associated_client->nom." ".$associated_client->prenom,
                                    $associated_client->adresse1,
                                    $associated_client->adresse2,
                                    $associated_client->cp." ".strtoupper($associated_client->ville)
                                ) ;
        }
        //Création de l'entête de la cooperative.
        $tabCoop = array(   
            $this->a_cooperative['title'],
            $this->a_cooperative['adresse'],
            $this->a_cooperative['ville']
    );

	$this->BasicCadre($tabArtisan,10,0,5);
	// details client
	$this->o_pdf->Ln();
	$this->BasicCadre($tabCoop,110,0,5);
	$this->o_pdf->Ln(1);
	// les données de ma facture
	$this->o_pdf->Ln();
	$this->o_pdf->Ln();
	$this->o_pdf->Ln();
    $this->o_pdf->Ln();
    
    //
    // must be factorised!!!
    //
    $a_prestations = array();
    $l_cesu=0;
    $l_ttc=0;
    // Recupere les données de la prestations cliente.
    foreach($facture_artisan->facturePrestation as $fp){
        
        $id_prestation = $fp->id_prestation;
        //-- Prestation
        $a_prestations[$id_prestation]['typePayement']  = $fp->prestation->typePayement;
        $a_prestations[$id_prestation]['num_cheque'] 	= $fp->prestation->num_cheque;
        $a_prestations[$id_prestation]['montantCESU']   = $fp->prestation->montantCESU;
        $a_prestations[$id_prestation]['montant'] 	    = $fp->prestation->montant;
        $a_prestations[$id_prestation]['tva'] 		    = $fp->prestation->sz_tva;
        $a_prestations[$id_prestation]['tva_artisan']   = $fp->prestation->sz_tva_artisan;
        $a_prestations[$id_prestation]['temps'] 		= $fp->prestation->nb_heure;
        $a_prestations[$id_prestation]['type'] 		    = $fp->prestation->type;
        $a_prestations[$id_prestation]['id'] 			= $fp->prestation->id_prestation;
        $a_prestations[$id_prestation]['artisan'] 	    = $fp->prestation->id_artisans;	
        $a_prestations[$id_prestation]['date'] 		    = $fp->prestation->date_prestation;
        $a_prestations[$id_prestation]['date_vire'] 	= $fp->prestation->date_virement;
        //-- Facture
        if(count($facture_artisan->facturePrestation)>1)
        {
            $a_prestations[$id_prestation]['montantHT'] 	= $facture_artisan->montantHT/2 ;
        }else{
            $a_prestations[$id_prestation]['montantHT'] 	= $facture_artisan->montantHT;
        }
        $a_prestations[$id_prestation]['montantTTC'] 	= $facture_artisan->montantTTC;
        $date_edition	                                = $facture_artisan->date;

        $l_cesu+=$fp->prestation->montantCESU;
        $l_ttc+=$fp->prestation->montant;

        // Info sur le moyen de paiement.
        if(!empty($fp->prestation->num_cheque))
        {
            $moyenPaiement = "Chèque N° ".$fp->prestation->num_cheque ;
        }else if(!empty($fp->prestation->typePayement))
        {
            $moyenPaiement = $fp->prestation->typePayement ;
            if($moyenPaiement=="Virement" && !empty($fp->prestation->date_virement)){
                $date_virement = date("d/m/Y", strtotime($fp->prestation->date_virement));
                $moyenPaiement = $moyenPaiement . " (".$date_virement.") ";
            }
        }
    }//foreach
    //
    // must be factorised!!!
    //

    $date_edition   = date("d/m/Y", strtotime($date_edition));
	$a_date_edition = explode('/',$date_edition);
	$this->o_pdf->Write(5,"Editée le ".$date_edition);
	$this->o_pdf->Ln();
	$this->o_pdf->Write(5,"Facture n°: ".$facture_artisan->id_facture);
	$this->o_pdf->Ln();
	$this->o_pdf->Ln();

    //taux de l'artisan
	if($a_date_edition[2]=="2014" && $facture_artisan->tva=="19.6")
	{
		$this->generTabArtisan($a_prestations,"20");
	}else{
		$this->generTabArtisan($a_prestations,$facture_artisan->artisan->tva);
	}

    // info type de payement
	$this->o_pdf->SetFont('Times','',12);
	$this->o_pdf->Write(5,"Condition de virement: ");
	$this->o_pdf->Ln();
	$this->o_pdf->SetFont('Times','',12);
	$this->o_pdf->Write(5,"Paiement effectué par virement");
	// sondage
	$this->o_pdf->Ln();
	$this->o_pdf->Ln();
	$this->o_pdf->Write(5,"Prestation effectué pour:");
	$this->o_pdf->Ln();
	$this->o_pdf->Ln();
	$this->BasicCadre($tabClient,10,0,5,false,true);
	$this->o_pdf->Ln();
	$this->o_pdf->Ln();
    $this->o_pdf->Ln();
    
    // access 
    $this->o_pdf->Write(5,"Accedez à votre compte directement sur le site: http://fluxplay.fr'");
    $this->o_pdf->Ln();
    $this->o_pdf->Write(5,"Votre nom d'utilisateur : ");
    $this->o_pdf->Write(5,"toto");
    $this->o_pdf->Ln();
    $this->o_pdf->Write(5,"votre mot de passe : ");
    $this->o_pdf->Write(5,"toto");

    $this->o_pdf->Output();
    exit;
    }
    
    public function fShowFactureClient(Facture $facture_client)
    {
        $this->o_pdf->AliasNbPages();
        $this->o_pdf->AddPage();
        $this->o_pdf->setSourceFile(app_path().$this->docs_folder."/facture_".$this->idFolder.".pdf"); 
        $tplIdx = $this->o_pdf->importPage(1); 
        $this->o_pdf->useTemplate($tplIdx); 
        $this->o_pdf->Ln(20);

        //Création de l'entête de la cooperative.
        $this->o_pdf->SetFont('Arial','B',10);
        $this->o_pdf->Write(5,'Coopérative Un PRO Chez Vous '.$this->idFolder);$this->o_pdf->Ln();
        $this->o_pdf->SetFont('Arial','',10);
        $this->o_pdf->Write(5,$this->a_cooperative['adresse']); $this->o_pdf->Ln();
        $this->o_pdf->Write(5,$this->a_cooperative['ville']); $this->o_pdf->Ln();
        $this->o_pdf->SetFont('Arial','B',10);
        $this->o_pdf->Write(5,'3620 dites "UN PRO CHEZ VOUS"'); $this->o_pdf->Ln();
        $this->o_pdf->Write(5,$this->a_cooperative['site']); 
        $this->o_pdf->SetFont('Arial','',10);
    

        //Création de l'entête du client.
        if(!empty($facture_client->client->civilite))
        {
            $sz_civilite = $facture_client->client->civilite;
        }else{
            $sz_civilite = "M.";
        }

        if(empty($facture_client->client->adresse2))
        {
            $tabClient = array(     $sz_civilite." ".$facture_client->client->nom." ".$facture_client->client->prenom,
                                    $facture_client->client->adresse1,
                                    $facture_client->client->cp." ".strtoupper($facture_client->client->ville)
                                ) ;
        }else{
            $tabClient = array(     $sz_civilite." ".$facture_client->client->nom." ".$facture_client->client->prenom,
                                    $facture_client->client->adresse1,
                                    $facture_client->client->adresse2,
                                    $facture_client->client->cp." ".strtoupper($facture_client->client->ville)
                                ) ;
        }
        $this->BasicCadre($tabClient,130,0,5);
        $this->o_pdf->Ln();
        $this->o_pdf->Ln();
        $l_cesu =0;
        $l_ttc =0;
        $a_prestations = array();
        // Recupere les données de la prestations cliente.
        foreach($facture_client->facturePrestation as $fp){
            
            $id_prestation = $fp->id_prestation;
            //-- Prestation
            $a_prestations[$id_prestation]['typePayement']  = $fp->prestation->typePayement;
            $a_prestations[$id_prestation]['num_cheque'] 	= $fp->prestation->num_cheque;
            $a_prestations[$id_prestation]['montantCESU']   = $fp->prestation->montantCESU;
            $a_prestations[$id_prestation]['montant'] 	    = $fp->prestation->montant;
            $a_prestations[$id_prestation]['tva'] 		    = $fp->prestation->sz_tva;
            $a_prestations[$id_prestation]['temps'] 		= $fp->prestation->nb_heure;
            $a_prestations[$id_prestation]['type'] 		    = $fp->prestation->type;
            $a_prestations[$id_prestation]['id'] 			= $fp->prestation->id_prestation;
            $a_prestations[$id_prestation]['artisan'] 	    = $fp->prestation->id_artisans;	
            $a_prestations[$id_prestation]['date'] 		    = $fp->prestation->date_prestation;
            $a_prestations[$id_prestation]['date_vire'] 	= $fp->prestation->date_virement;
            //-- Facture
            $a_prestations[$id_prestation]['montantHT'] 	= $facture_client->montantHT;
            $a_prestations[$id_prestation]['montantTTC'] 	= $facture_client->montantTTC;
            $date_edition	                                = $facture_client->date;

            $l_cesu+=$fp->prestation->montantCESU;
            $l_ttc+=$fp->prestation->montant;

            // Info sur le moyen de paiement.
			if(!empty($fp->prestation->num_cheque))
			{
				$moyenPaiement = "Chèque N° ".$fp->prestation->num_cheque ;
			}else if(!empty($fp->prestation->typePayement))
			{
				$moyenPaiement = $fp->prestation->typePayement ;
				if($moyenPaiement=="Virement" && !empty($fp->prestation->date_virement)){
					$date_virement = date("d/m/Y", strtotime($fp->prestation->date_virement));
					$moyenPaiement = $moyenPaiement . " (".$date_virement.") ";
				}
			}
        }//foreach

        $this->o_pdf->SetFont('Arial','B',15);
        $this->o_pdf->SetX(38);
        $this->o_pdf->write(8,"Facture d'intervention Services à la Personne");$this->o_pdf->Ln();
        $this->o_pdf->SetFont('Arial','',10);

        $this->o_pdf->Ln();
        $this->o_pdf->Write(5,"Facture N°: ".$facture_client->id_facture);

        //Si il y a une date dedition on l'utilise sinon c'est la date du jour.
        if($date_edition !==null && strlen($date_edition)>1)
        {
            $this->o_pdf->Write(5," - Editée le ".date("d/m/Y", strtotime($date_edition)));
        }else{
            $this->o_pdf->Write(5," - Editée le ".date("d/m/Y"));
        }
    
        $this->o_pdf->Ln();
        $this->o_pdf->Ln();

        $this->generTab($a_prestations,"7%",
                        $facture_client->reference_prestation,
                        $facture_client->txt_complement);
	
        $this->o_pdf->SetY(150);
        $this->o_pdf->SetFont('Arial','U',10);
        $this->o_pdf->Ln();
        $this->o_pdf->Ln();
		if(!empty($facture_client->client->adresse2)) $this->o_pdf->Ln();
		$this->o_pdf->Write(5,"Conditions de paiement:");
		$this->o_pdf->SetFont('Arial','',10);
        $this->o_pdf->Ln();
        
        if($l_cesu>0)
		{
			$this->o_pdf->SetX(20);
			$this->o_pdf->Write(5,"Paiement en tickets CESU: ".$l_cesu." ".utf8_encode(chr(128))); 
			$this->o_pdf->Ln();
		}
		if($l_ttc>0)
		{
			$this->o_pdf->SetX(20);
			$this->o_pdf->Write(5,"Paiement par ".$moyenPaiement." : ".$l_ttc." ".utf8_encode(chr(128))); 
			$this->o_pdf->Ln();
		}
		$this->o_pdf->Write(3,"");
        $this->o_pdf->Ln();
        
        // Infos intervenant.
        $this->o_pdf->SetFont('Arial','U',10);
        $this->o_pdf->Write(5,"Intervenant:");
        $this->o_pdf->SetFont('Arial','',10);
        $this->o_pdf->Ln();
        $this->o_pdf->SetX(20);
        $associated_artisan = $facture_client->associatedArtisan();
        $this->o_pdf->Write(5,$associated_artisan->nom);
        $this->o_pdf->Ln();			
        $this->o_pdf->SetX(20);
        $this->o_pdf->Write(5,$associated_artisan->cp." ".$associated_artisan->ville);
        $this->o_pdf->Ln();

        $data=array();
        $data['sz_login']="login";
        $data['sz_password']="password";

        // Access user 
        $this->o_pdf->SetFont('Arial','',12);
		$this->o_pdf->Ln();
		// $this->o_pdf->Write(5,"Accedez à votre compte directement sur le site: ");
		$this->o_pdf->Write(5,"Retrouvez vos factures et attestations directement sur notre site: ");
		$this->o_pdf->SetFont('Arial','B',12);
		$this->o_pdf->Write(5,"http://fluxplay.fr/".$this->idFolder."/");
		$this->o_pdf->SetFont('Arial','',12);
		$this->o_pdf->Ln();
		$this->o_pdf->Write(5,"Votre nom d'utilisateur est: ");
		$this->o_pdf->SetFont('Arial','B',12);
		$this->o_pdf->Write(5,$data['sz_login']);
		$this->o_pdf->SetFont('Arial','',12);
		$this->o_pdf->Write(5," et votre mot de passe est: ");
		$this->o_pdf->SetFont('Arial','B',12);
		$this->o_pdf->Write(5,$data['sz_password']); $this->o_pdf->Ln();
        $this->o_pdf->SetFont('Arial','',12);
        
        $this->o_pdf->SetFont('Times','',8);
        $this->o_pdf->Ln();

        $this->o_pdf->Output();
        exit;
    }

    

public function test(){
    $this->o_pdf->AddPage();
    $this->o_pdf->SetFont('Arial','B',16);
    $this->o_pdf->Cell(40,10,'Hello World!!');
    $this->o_pdf->Output();
    exit;
    }


    private function generTab($a_data,$i_tva,$reference,$complement)
    {
		$headerFacture = array("Travaux réalisés", "Nombre", "Montant HT", "TVA","Montant TVA","Montant TTC");
		$w=array(83,16,23,13,25,25);
		$this->o_pdf->SetFont('Arial','B',10);
		for($i=0;$i<count($headerFacture);$i++)
		{
			$this->o_pdf->Cell($w[$i],7,$headerFacture[$i],"LRTB",0);
		}
		$this->o_pdf->SetFont('Arial','',10);
        $this->o_pdf->Ln();
        $i_montant_tva_TOTAL=0;
		foreach($a_data as $a_prestation)
		{
			//evite les warnings
			if(isset($a_prestation["type"]) && isset($a_prestation["tva"]) 
			&& isset($a_prestation["montantTTC"]) && isset($a_prestation["montantCESU"])
			&& isset($a_prestation["montant"]) && isset($a_prestation["montantHT"]))
			{
				$alias_type = $this->sz_getTypePrestation($a_prestation['type']);
				$this->o_pdf->Cell($w[0],7,$alias_type,"LR",0);
				$this->o_pdf->Cell($w[1],7,'1',"LR",0); 
				//Calcul des montants
				$i_taux =  floatval($a_prestation['tva']);
				$i_taux = $i_taux / 100 ;
				$i_taux = $i_taux + 1 ;
				
				$i_montantTTC	= (float)$a_prestation["montantTTC"] ;
				$i_montantCESU 	= (float)$a_prestation["montantCESU"] ;
				$i_montant		= (float)$a_prestation["montant"] ;
				$i_montant 		+= $i_montantCESU ;
				$i_montantHT 	= (float)$a_prestation["montantHT"] ;
				$i_montant_HT_prestation = (float) $i_montant / $i_taux ;
				$i_montant_tva	= (float)$i_montant - $i_montant_HT_prestation;
				$i_montant_tva_TOTAL +=  $i_montant_tva ;

				$this->o_pdf->Cell($w[2],7,number_format(round($i_montant_HT_prestation,2),2,'.',''),"LR",0); 
				$this->o_pdf->Cell($w[3],7,$a_prestation['tva']."%","LR",0); 
				$this->o_pdf->Cell($w[4],7,round($i_montant_tva,2),"LR",0); 
				$this->o_pdf->Cell($w[5],7,number_format(round($i_montant,2),2,'.',''),"LR",0); 
				$this->o_pdf->Ln();			
			}
		}
		//Reference de la prestation - a ajouter seulement sur la facture client.
		$this->o_pdf->SetFont('Arial','I',10);
		if(!empty($reference)){
			$this->o_pdf->Cell($w[0],7,"Référence de la prestation: ".$reference,"LR",0);
		}else{
			$this->o_pdf->Cell($w[0],7,"","LR",0);
		}
		$this->o_pdf->SetFont('Arial','',10);
		$this->o_pdf->Cell($w[1],7,"","LR",0);
		$this->o_pdf->Cell($w[2],7,"","LR",0);
		$this->o_pdf->Cell($w[3],7,"","LR",0);
		$this->o_pdf->Cell($w[4],7,"","LR",0);
		$this->o_pdf->Cell($w[5],7,"","LR",0);

		if(strlen($complement)>0)
		{
			$sz_ligne = "";
			$nbCaracteresParLigne = 55 ;
			$nbCRestant = $nbCaracteresParLigne ;
			$a_Mots 	= explode(" ",$complement);
			$nbMots 	= count($a_Mots);
			for($i=0;$i<$nbMots;$i++)
			{
			 if($nbCRestant>strlen($a_Mots[$i])+1)
			 {
				$sz_ligne .= $a_Mots[$i]." " ;
				$nbCRestant = $nbCRestant - (strlen($a_Mots[$i])+1);
			 }else{
				$a_lignes[] = $sz_ligne;
				$sz_ligne = $a_Mots[$i]." " ;
				$nbCRestant = $nbCaracteresParLigne ;
			 }
			}//for
			$a_lignes[] = $sz_ligne;
			foreach($a_lignes as $ligne)
			{	$this->o_pdf->Ln();
				$this->o_pdf->Cell($w[0],5,$ligne,"LR",0);		
				$this->o_pdf->Cell($w[1],5,"","LR",0);
				$this->o_pdf->Cell($w[2],5,"","LR",0);
				$this->o_pdf->Cell($w[3],5,"","LR",0);
				$this->o_pdf->Cell($w[4],5,"","LR",0);
				$this->o_pdf->Cell($w[5],5,"","LR",0);
			}//foreach
		}else{
			$this->o_pdf->Ln();
			$this->o_pdf->Cell($w[0],7,"","LR",0);
			$this->o_pdf->Cell($w[1],7,"","LR",0);
			$this->o_pdf->Cell($w[2],7,"","LR",0);
			$this->o_pdf->Cell($w[3],7,"","LR",0);
			$this->o_pdf->Cell($w[4],7,"","LR",0);
			$this->o_pdf->Cell($w[5],7,"","LR",0);
		}

		$this->o_pdf->Ln();
		$this->o_pdf->Cell($w[0],7,"","LR",0);
		$this->o_pdf->Cell($w[1],7,"","LR",0);
		$this->o_pdf->Cell($w[2],7,"","LR",0);
		$this->o_pdf->Cell($w[3],7,"","LR",0);
		$this->o_pdf->Cell($w[4],7,"","LR",0);
		$this->o_pdf->Cell($w[5],7,"","LR",0);
		
		$this->o_pdf->Ln();
		$this->o_pdf->Cell($w[0],7,"","LR",0);
		$this->o_pdf->Cell($w[1],7,"","LR",0);
		$this->o_pdf->Cell($w[2],7,"","LR",0);
		$this->o_pdf->Cell($w[3],7,"","LR",0);
		$this->o_pdf->Cell($w[4],7,"","LR",0);
		$this->o_pdf->Cell($w[5],7,"","LR",0);

		$this->o_pdf->Ln();
		$this->o_pdf->Cell($w[0],7,"50 % déductibles de vos impôts sur les revenus*","T",0);

		$this->o_pdf->Cell($w[1],7,"","T",0);
		$this->o_pdf->Cell($w[2],7,"","T",0);
		$this->o_pdf->Cell($w[3],7,"","T",0);
		$this->o_pdf->SetFont('Arial','B',10);
		$this->o_pdf->Cell($w[4],7,"Total HT","LRT",0);
		$this->o_pdf->SetFont('Arial','',10);
		$this->o_pdf->Cell($w[5],7,number_format(round($i_montantHT,2),2,'.','').utf8_encode(chr(128)),"LRT",0);

		$this->o_pdf->Ln();
		$this->o_pdf->Cell($w[0],7,"l'attestation fiscale vous sera envoyée au cours de l'année suivante.","",0);

		$this->o_pdf->Cell($w[1],7,"","",0);
		$this->o_pdf->Cell($w[2],7,"","",0);
		$this->o_pdf->Cell($w[3],7,"","",0);
		$this->o_pdf->SetFont('Arial','B',10);
		$this->o_pdf->Cell($w[4],7,"Total TVA","LRT",0);
		$this->o_pdf->SetFont('Arial','',10);
		$this->o_pdf->Cell($w[5],7,number_format(round($i_montant_tva_TOTAL,2),2,'.','').utf8_encode(chr(128)),"LRT",0);

		$this->o_pdf->Ln();
		
		$this->o_pdf->Cell($w[0],7,"","",0);
		$this->o_pdf->Cell($w[1],7,"","",0);
		$this->o_pdf->Cell($w[2],7,"","",0);
		$this->o_pdf->Cell($w[3],7,"","",0);
		$this->o_pdf->SetFont('Arial','B',10);
		$this->o_pdf->Cell($w[4],7,"Total TTC","LRTB",0);
		$this->o_pdf->SetFont('Arial','',10);
		$this->o_pdf->Cell($w[5],7,number_format(round($i_montantTTC,2),2,'.','').utf8_encode(chr(128)),"LRTB",0);
		$this->o_pdf->Ln();
		$this->o_pdf->Ln();
    }
    
    private function BasicCadre($a_data,$pos=0,$border=1,$height=7)
    {
            $width = 0 ;
            for($i=0;$i<sizeof($a_data);$i++)
            {
                if(strlen($a_data[$i])>$width)
                    $width = strlen($a_data[$i]);
            }
            // on met a l'echelle la longueur
            $width = $width * 3.5 ;
            if(sizeof($a_data)>1)
            {
                if($pos>0) $this->o_pdf->SetX($pos) ;
                $border?$this->o_pdf->Cell($width,$heightw,$a_data[0],"LRT",1):$this->o_pdf->Cell($width,$height,$a_data[0],"",1);
                for($i=1; $i<(sizeof($a_data)-1); $i++)
                {
                    if($pos>0) $this->o_pdf->SetX($pos) ;
                    $border?$this->o_pdf->Cell($width,$height,$a_data[$i],"LR",1):$this->o_pdf->Cell($width,$height,$a_data[$i],"",1);
                }
                if($pos>0) $this->o_pdf->SetX($pos) ;
                $border?$this->o_pdf->Cell($width,$height,$a_data[(sizeof($a_data)-1)],"LRB",1):$this->o_pdf->Cell($width,$height,$a_data[(sizeof($a_data)-1)],"",1);

            }else{
                if($pos>0) $this->o_pdf->SetX($pos) ;
                $border?$this->o_pdf->Cell($width,$height,$a_data[0],"LRTB",1):$this->o_pdf->Cell($width,$height,$a_data[0],"",1);
            }
    }

    public function fSetFolder($idFolder)
    {
        if(!empty($idFolder))
        {
            $this->idFolder = $idFolder;
            switch($this->idFolder)
            {
                case"84":
                    $this->a_cooperative['title'] = "Coopérative UN PRO CHEZ VOUS 84";
                    $this->a_cooperative['adresse'] = "938 route de Sorgues";
                    $this->a_cooperative['ville'] = "84320 Entraigues sur la Sorgue";
                    $this->a_cooperative['site'] = "www.unprochezvous.com";
                    $this->a_cooperative['siret'] = "50771580300015";
                    $this->a_cooperative['agrement'] = "SAP507715803 du 16/11/2012";
                    $this->a_cooperative['idtva'] = "FR71507715803";
                    $this->a_cooperative['mail'] = "upcv84@unprochezvous.fr";
                    break;
                case"30":
                    $this->a_cooperative['title'] = "Coopérative UN PRO CHEZ VOUS 30";
                    $this->a_cooperative['adresse'] = "21, rue des Ecoles";
                    $this->a_cooperative['ville'] = "30730 Montpezat";
                    $this->a_cooperative['site'] = "www.unprochezvous.com";
                    $this->a_cooperative['siret'] = "51201732800018";
                    $this->a_cooperative['agrement'] = "SAP512017328 du 16/06/2013";
                    $this->a_cooperative['idtva'] = "FR57512017328";
                    $this->a_cooperative['mail'] = "upcv30@unprochezvous.fr";
                    break;
                case"13":
                    $this->a_cooperative['title'] = "Coopérative UN PRO CHEZ VOUS 13";
                    $this->a_cooperative['adresse'] = "28, boulevard Paul Painleve";
                    $this->a_cooperative['ville'] = "13800 Istrest";
                    $this->a_cooperative['site'] = "www.unprochezvous.com";
                    $this->a_cooperative['siret'] = "51011754200017";
                    $this->a_cooperative['agrement'] = "SAP510117542 du 15/09/2014";
                    $this->a_cooperative['idtva'] = "FR31510117542";
                    $this->a_cooperative['mail'] = "upcv13@unprochezvous.fr";
                    break;
                case"83":
                    $this->a_cooperative['title'] = "Coopérative UN PRO CHEZ VOUS 83";
                    $this->a_cooperative['adresse'] = "9, rue de la ferrage";
                    $this->a_cooperative['ville'] = "83136 Néoules";
                    $this->a_cooperative['site'] = "www.unprochezvous.com";
                    $this->a_cooperative['siret'] = "53427574800018";
                    $this->a_cooperative['agrement'] = "N/280911/F/083/S/0644 du 28/09/2011";
                    $this->a_cooperative['idtva'] = "FR32534275748";
                    $this->a_cooperative['mail'] = "upcv83@unprochezvous.fr";
                    break;
                case"26":
                    $this->a_cooperative['title'] = "Coopérative UN PRO CHEZ VOUS 26";
                    $this->a_cooperative['adresse'] = "ZA LA PALUN - PÔLE D'ACTIVITÉ";
                    $this->a_cooperative['ville'] = "26170 BUIS-LES-BARONNIES";
                    $this->a_cooperative['site'] = "www.unprochezvous.com";
                    $this->a_cooperative['siret'] = "83046665200013";
                    $this->a_cooperative['agrement'] = "N/280911/F/083/S/0644 du 28/09/2011";
                    $this->a_cooperative['idtva'] = "FR32534275748";
                    $this->a_cooperative['mail'] = "upcv26@unprochezvous.fr";
                    break;
                case"34":
                    $this->a_cooperative['title'] = "Coopérative UN PRO CHEZ VOUS 34";
                    $this->a_cooperative['adresse'] = "73 Allée KLEBER";
                    $this->a_cooperative['ville'] = "34000 MONTPELLIER";
                    $this->a_cooperative['site'] = "www.unprochezvous.com";
                    $this->a_cooperative['siret'] = "84949920700017";
                    $this->a_cooperative['agrement'] = "SAP84499207";
                    $this->a_cooperative['idtva'] = "FR46849499207";
                    $this->a_cooperative['mail'] = "upcv84@unprochezvous.fr";
                    break;
            }
        }
    }
    public function sz_getTypePrestation($label)
    {
        $alias_type = "";
        switch($label)
            {
            case "jardinage" 	  : $alias_type="Petits travaux de jardinage";	    break ;
            case "traiteur" 	  : $alias_type="Préparation/Livraison de repas";	break ;
            case "informatique"   : $alias_type="Assistance informatique";			break ;
            case "bricolage" 	  : $alias_type="Travaux de petit bricolage";		break ;
            case "nettoyage" 	  : $alias_type="Entretien de la maison et travaux ménagers";	break ;
            case "administratif"  : $alias_type="Assistance informatique";		    	break ;
            case "cours"  		  : $alias_type="Cours à domicile";				    	break ;
            case "accompagnement" : $alias_type="Accompagnement d'enfants + de 3 ans";	break ;
            }
        return $alias_type ;
    }

    public function generTabArtisan($a_data,$i_tva)
    {
    $headerFacture = array("Travaux réalisés", "Nombre", "Montant HT");
    $w=array(125,25,25);
    for($i=0;$i<count($headerFacture);$i++)
    {
        $this->o_pdf->Cell($w[$i],7,$headerFacture[$i],"LRTB",0);
    }
    $this->o_pdf->Ln();
    $i_montantTHT=0;
    foreach($a_data as $a_prestation)
    {
        $alias_type = $this->sz_getTypePrestation($a_prestation['type']);
        $this->o_pdf->Cell($w[0],7,$alias_type,"LR",0); 
        $this->o_pdf->Cell($w[1],7,'1',"LR",0); 
        //Calcul des montants
        $i_montantTTC	= (float)$a_prestation['montantTTC'] ;
        $i_montantHT 	=  (float)$a_prestation['montantHT'] ;
        $i_montantTHT  += $i_montantHT ;
        $this->o_pdf->Cell($w[2],7,number_format(round($i_montantHT,2),2,'.',''),"LR",0); 
        $this->o_pdf->Ln();

    }//foreach

    //pour completé 
    $this->o_pdf->Cell($w[0],7,"","LR",0);
    $this->o_pdf->Cell($w[1],7,"","LR",0);
    $this->o_pdf->Cell($w[2],7,"","LR",0);

    $this->o_pdf->Ln();
    $this->o_pdf->Cell($w[0],7,"","LR",0);
    $this->o_pdf->Cell($w[1],7,"","LR",0);
    $this->o_pdf->Cell($w[2],7,"","LR",0);

    $this->o_pdf->Ln();
    $this->o_pdf->Cell($w[0],7,"","LR",0);
    $this->o_pdf->Cell($w[1],7,"","LR",0);
    $this->o_pdf->Cell($w[2],7,"","LR",0);

    $this->o_pdf->Ln();
    $this->o_pdf->Cell($w[0],7,"","LR",0);
    $this->o_pdf->Cell($w[1],7,"","LR",0);
    $this->o_pdf->Cell($w[2],7,"","LR",0);

    $this->o_pdf->Ln();
    $this->o_pdf->Cell($w[0],7,"","LRB",0);
    $this->o_pdf->Cell($w[1],7,"","LR",0);
    $this->o_pdf->Cell($w[2],7,"","LR",0);

    $this->o_pdf->Ln();
    $this->o_pdf->Cell($w[0],7,"","LRB",0);
    $this->o_pdf->Cell($w[1],7,"","LRB",0);
    $this->o_pdf->Cell($w[2],7,"","LRB",0);

    $this->o_pdf->Ln();
    $this->o_pdf->Cell($w[0],7,"","",0);
    $this->o_pdf->Cell($w[1],7,"Total HT","LRT",0);
    $this->o_pdf->Cell($w[2],7,$i_montantTHT.utf8_encode(chr(128)),"LRT",0);

    $this->o_pdf->Ln();
    $this->o_pdf->Cell($w[0],7,"","",0);
    $this->o_pdf->Cell($w[1],7,"TVA ".$i_tva."%","LRT",0);

    $this->o_pdf->Cell($w[2],7,$i_montantTTC-$i_montantTHT.utf8_encode(chr(128)),"LRT",0);

    //$this->o_pdf->Cell($w[3],7,Round($i_montantTTTC-$i_montantTHT,2)."€","LRT",0);
    //$this->o_pdf->Cell($w[3],7,$i_montantTTTC-$i_montantTHT."€","LRT",0);
    $this->o_pdf->Ln();

    $this->o_pdf->Cell($w[0],7,"","",0);
    $this->o_pdf->SetFont('Arial','B',10);
    $this->o_pdf->Cell($w[1],7,"Total TTC","LRTB",0);
    $this->o_pdf->Cell($w[2],7,$i_montantTTC.utf8_encode(chr(128)),"LRTB",0);
    $this->o_pdf->SetFont('Arial','',10);
    //$this->o_pdf->Cell($w[3],7,Round($i_montantTTTC,2)."€","LRTB",0);
    //$this->o_pdf->Cell($w[3],7,$i_montantTTTC."€","LRTB",0);
    $this->o_pdf->Ln();
    $this->o_pdf->Ln();
	}
}
