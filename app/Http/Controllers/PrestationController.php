<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\FactureController;
use App\Prestation;
use App\Factureprestation;
use App\Facture;
use App\Client;
use App\Artisan;
use App\Period;
// Mail
use App\Mail\MailtrapFacture;
use Illuminate\Support\Facades\Mail;

class PrestationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit($id)
    {
        $status = array();
        $status['text']     = array();
        $status['text'][]   = "Vous pouvez editer la prestation sans modifier la facture.";
        $status['style']    = "success";
        $prestation = Prestation::find($id);
        
        return view('prestations/edit', compact('prestation')) 
            ->with('status',$status);
    }

    public function update(Request $request)
    {
        $status = array();
        $status['text'] = array();

        $request->validate([
            'id_client'=>'required|numeric',
            'sz_workType'=>'required',
            'id_artisans'=>'required|numeric',
            'se_coeff'=>'required',
            'sz_date'=> 'required',
            'id_prestation'=> 'required|numeric',
            'i_price'=> 'required|numeric',
            'sz_tva_nature'=> 'required',
            'sz_tva_artisan'=> 'required'
        ],
        [
            'id_prestation.required'=>'Vous devez fournir numero de prestation.',
            'id_client.required'=>'Vous devez fournir un nom de client valide.',
            'sz_workType.required'=>'Vous devez fournir une nature de travail valide.',
            'id_artisans.required'=>'Vous devez fournir un nom d\'artisan valide.',
            'se_coeff.required'=>'Vous devez fournir un coefficient valide.',
            'sz_date.required'=>'Vous devez fournir une date valide.',
            'i_price.required'=>'Vous devez fournir un montant valide.,',
            'sz_tva_nature.required'=>'La TVA de la nature n\'est pas correct.',
            'sz_tva_artisan.required'=>'La TVA de la prestation n\'est pas correct.,'
        ]);

        $prestation = Prestation::find($request->get('id_prestation'));
        if(empty($prestation->id_prestation))
        {
            $status['text'][]   = "Une erreur est survenu, la prestation n'a pas pu etre recuperée!";
            $status['style']    = "danger";
            $prestations = Prestation::orderBy('id_prestation','DESC')->limit(20)->get();
            return view('prestations/index', compact('prestations'))
                ->with('APP_URL',env('APP_URL'))
                ->with('status',$status);

        }
        // data validation, check client and sartisan
        $client  = Client::find($request->get('id_client'));
        $artisan = Artisan::find($request->get('id_artisans'));
        if(!empty($client->id_client) && !empty($artisan->id_artisans))
        {
            $sz_date_pres = $request->get('sz_date');
            if(empty($request->get('sz_date_vire')))
            {
                $sz_date_vire = $sz_date_pres;
            }else{
                $sz_date_vire = $request->get('sz_date_vire');
            }
            if($request->get('sz_nbTime')==="FORFAIT")
            {
                $sz_nbTime = 1;
            }else{
                $sz_nbTime = $request->get('sz_nbTime');
            }
            // Verification de montant CESU
            if($request->get('i_priceCESU') !== null)
            {
                $i_priceCESU = str_replace(',','.',$request->get('i_priceCESU'));
            }else{
                $i_priceCESU = 0;
            }
            // Verification de montant
            $montant = str_replace(',','.',$request->get('i_price') );
            if(($montant + $i_priceCESU > 0)===false)
            {
                $status['text'][]   = "Une erreur est suvenu pour le montant de cette prestation!";
                $status['style']    = "danger";
                $prestation = Prestation::find($prestation->id_prestation);
                return view('prestations/edit', compact('prestation')) 
                    ->with('status',$status);
            }

            // If montant has been update then re-calcultation
            if($prestation->montant!=$montant || $prestation->montantCESU!=$i_priceCESU)
            {
                // check the coop, client, artisan factures
                $fps = Factureprestation::where('id_prestation',$prestation->id_prestation)->get();
                if(count($fps)>0)
                {
                    foreach($fps as $fp)
                    {
                        if($fp->type==="client"){
                            $fClient = Facture::where('num_facture',$fp->num_facture)->first();
                        }else if($fp->type==="artisan"){
                            $fArtisan = Facture::where('num_facture',$fp->num_facture)->first();
                        }else if($fp->type==="coop"){
                            $fCoop = Facture::where('num_facture',$fp->num_facture)->first();
                        }   
                    }
                    // Substract the old HT from the facture client after to re-compute it.
                    $old_montantTTC     = (float) ($prestation->montant + $prestation->montantCESU);
                    $old_montantHT      = Prestation::getClientMontantHT($prestation, $old_montantTTC);
                    $fClient->montantHT = $fClient->montantHT - $old_montantHT;
                    //substract the TTC, CESU from the facture client.
                    $fClient->montantTTC  = (float)  $fClient->montantTTC - ($prestation->montant + $prestation->montantCESU);
                    $fClient->montantCESU = (float) $fClient->montantCESU - $prestation->montantCESU;
                    
                    // update the new tva used into getClientMontantHT.
                    $prestation->sz_tva     = $request->get('sz_tva_nature');
                    //affect the new value of the prestation to the facture client.
                    $new_montantTTC         = (float) ($montant + $i_priceCESU);
                    $fClient->montantTTC    += $new_montantTTC;
                    $fClient->montantCESU   += (float) $i_priceCESU;
                    $fClient->montantHT     += Prestation::getClientMontantHT($prestation, $new_montantTTC);
                    $fClient->save();

                    // Substract the old HT from the facture artisan after to re-compute it.
                    $old_result = Prestation::getTauxAndCommission($prestation,  $old_montantTTC);
                    $fArtisan->montantTTC =  $fArtisan->montantTTC - $old_result["i_montantTTCArtisan"];
                    $fArtisan->montantHT =  $fArtisan->montantHT - $old_result["i_montantHTArtisan"] ;
                    
                    // Substract the old TTC from the facture coop.
                    $fCoop->montantTTC = $fCoop->montantTTC - $old_result["i_gainCoop"];

                    // update the new tva used into getTauxAndCommission.
                    $prestation->coeff              = $request->get('se_coeff');
                    $prestation->sz_tva_artisan     = $request->get('sz_tva_artisan');
                    $result = Prestation::getTauxAndCommission($prestation,  $new_montantTTC);
                    $fArtisan->montantTTC +=  $result["i_montantTTCArtisan"];
                    $fArtisan->montantHT  +=  $result["i_montantHTArtisan"] ;
                    $fArtisan->save();

                    $fCoop->montantTTC += $result["i_gainCoop"];
                    $fCoop->montantHT = 0;
                    $fCoop->save();
                }
            }//montant changed
            // update prestation
            $prestation->id_client          = $request->get('id_client');
            $prestation->id_artisans        = $request->get('id_artisans');
            $prestation->typePayement       = $request->get('sz_typePayement');
            $prestation->num_cheque         = $request->get('i_numCheque');
            $prestation->date_prestation    = $sz_date_pres;
            $prestation->date_virement      = $sz_date_vire;
            $prestation->type               = $request->get('sz_workType');
            $prestation->nb_heure           = $sz_nbTime;
            // $prestation->num_factureClient  = "NA";
            // $prestation->num_factureArtisan = "NA";
            $prestation->montant            = $montant;
            $prestation->montantCESU        = $i_priceCESU;
            // $prestation->coeff              = $request->get('se_coeff');
            // $prestation->sz_tva             = $request->get('sz_tva_nature');
            // $prestation->sz_tva_artisan     = $request->get('sz_tva_artisan');
            $prestation->save();

            $status['text'][]   = "La prestation ".$prestation->id_prestation." a été mise à jour!";
            $status['style']    = "success";

            $o_facture = new FactureController();
            $prestations = $o_facture->createRecapitulatifFetchArtisan(null);

            $highlights = array();
            $highlights[] = $prestation->id_prestation;

            return view('prestations/index', compact('prestations'))
                ->with('status',$status)
                ->with('highlights',$highlights);

        }else{
            $status['text'][]   = "Une erreur est suvenu soit dans l'artisan soit dans le client de cette prestation!";
            $status['style']    = "danger";
            $prestation = Prestation::find($prestation->id_prestation);
            return view('prestations/edit', compact('prestation')) 
                ->with('status',$status);
        }
    }


    public function delete2($id)
    {
        // en suppriment un prestation est ce quon veux aussi supprimer la seconde prestation ?
        $prestation = Prestation::find($id);
        $factureprestations = $prestation->factureprestation;
        foreach($factureprestations as $factureprestation)
        {
            $facture = $factureprestation->facture;
            $factureprestation->delete();
            if($facture !== false)
            {
                $facture->delete();
            }
        }
        $prestation->delete();
    }
    public function delete(Request $request)
    {
        $prestation = Prestation::find($request['idpres']);
        if($prestation !== false){
            $factureprestations = $prestation->factureprestation;
            foreach($factureprestations as $factureprestation)
            {
                $factureprestation->delete();
            }
            //$prestation->delete();
        }

        $status['text'] = array();
        if($affectedRows>0)
        {
            $status['text'][]   = "Felicitation!! le prestation ".$request['idpres']." a été supprimé!";
            $status['style']    = "success";
        }else{
            $status['text'][]   = "Une erreur est survenu lors de la suppression de la prestation ".$request['idpres']."!";
            $status['style']    = "danger";
        }
        $prestations = Prestation::orderBy('id_prestation','DESC')->limit(25)->get();
        return view('prestations', compact('prestations')) 
            ->with('APP_URL',env('APP_URL'))
            ->with('status',$status);
    }
    public function index()
    {   
        $factures = Facture::where('typeFacture','Client')
        ->orderBy('date','DESC')
        ->limit(25)
        ->get();

        $o_facture = new FactureController();
        $prestations = $o_facture->createRecapitulatifFetchArtisan($factures);

        return view('prestations/index', compact('prestations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $status = array();

        $request->validate([
            'id_client'=>'required',
            'sz_workType'=>'required',
            'id_artisans'=>'required',
            'se_taux'=>'required',
            'sz_tva_artisan'=>'required',
            'sz_date'=> 'required',
            'i_price'=> 'required'
        ],
        [
            'id_client.required'=>'Vous devez fournir un nom de client valide.',
            'sz_workType.required'=>'Vous devez fournir une nature de travail valide.',
            'id_artisans.required'=>'Vous devez fournir un nom d\'artisan valide.',
            'se_taux.required'=>'Vous devez fournir un taux valide.',
            'sz_tva_artisan.required'=>'Vous devez fournir une tva valide.',
            'sz_date.required'=>'Vous devez fournir une date valide.',
            'i_price.required'=>'Vous devez fournir un montant valide.,'
        ]
        );

        // data validation, check client and sartisan
        $client  = Client::find($request->get('id_client'));
        $artisan = Artisan::find($request->get('id_artisans'));
        if(!empty($client->id_client) && !empty($artisan->id_artisans))
        {
            // add first prestation
            $base = new BaseController();
            $sz_date_pres = $request->get('sz_date');
            if(empty($request->get('sz_date_vire')))
            {
                $sz_date_vire = $request->get('sz_date');
            }else{
                $sz_date_vire = $request->get('sz_date_vire');
            }

            if($request->get('sz_nbTime')=="FORFAIT")
            {
                $sz_nbTime = 1;
            }else{
                $sz_nbTime = $request->get('sz_nbTime');
            }
            //todo why it's not by default modele level
            if($request->get('i_priceCESU') !== null)
            {
                $i_priceCESU = str_replace(',','.',$request->get('i_priceCESU'));
            }else{
                $i_priceCESU = 0;
            }

            $montant = str_replace(',','.',$request->get('i_price') );

            $p1 = new Prestation([
                'id_client'            => $request->get('id_client'),
                'id_artisans'          => $request->get('id_artisans'),
                'typePayement'         =>  $request->get('sz_typePayement'),
                'num_cheque'           => $request->get('i_numCheque'),
                'date_prestation'      => $sz_date_pres,
                'date_virement'        => $sz_date_vire,
                'type'                 => $request->get('sz_workType'),
                'nb_heure'             => $sz_nbTime,
                'num_factureClient'    => "NA",
                'num_factureArtisan'   => "NA",
                'montant'              => $montant,
                'montantCESU'          => $i_priceCESU,
                'coeff'                => $request->get('se_taux'),
                'sz_tva'               => $request->get('sz_tva_nature'),
                'sz_tva_artisan'       => $request->get('sz_tva_artisan'),
                'ttc_artisan'          => 0.0,
            ]);

            $status = $this->fAddPrestation($p1);
            
            //highligh the prestation when created
            $highlights = array();
            $highlights[] = $p1->id_prestation;

            // Append facture data
            $facture_client = new Facture();
            $facture_client->typeFacture = "Client";
            $facture_client->montantTTC = (float) ($p1->montantCESU + $p1->montant);
            $facture_client->montantCESU = (float) $p1->montantCESU;
            $facture_client->montantHT = Prestation::getClientMontantHT($p1, $facture_client->montantTTC);

            // add second prestation
            if(!empty($request->get('i_priceCESU_2')) || !empty($request->get('i_price_2')))
            {

                if($request->get('sz_nbTime_2')=="FORFAIT")
                {
                    $sz_nbTime_2 = 1;
                }else{
                    $sz_nbTime_2 = $request->get('sz_nbTime_2');
                }
                //todo why it's not by default modele level
                if($request->get('i_priceCESU_2') !== null)
                {
                    $i_priceCESU_2 = str_replace(',','.',$request->get('i_priceCESU_2'));
                }else{
                    $i_priceCESU_2 = 0;
                }
                $montant_2 = str_replace(',','.',$request->get('i_price_2') );

                $p2 = new Prestation([
                    'id_client'            => $request->get('id_client'),
                    'id_artisans'          => $request->get('id_artisans'),
                    'typePayement'         =>  $request->get('sz_typePayement'),
                    'num_cheque'           => $request->get('i_numCheque'),
                    'date_prestation'      => $sz_date_pres,
                    'date_virement'        => $sz_date_vire,
                    'type'                 => $request->get('sz_workType_2'),
                    'nb_heure'             => $sz_nbTime_2,
                    'num_factureClient'    => "NA",
                    'num_factureArtisan'   => "NA",
                    'montant'              => $montant_2,
                    'montantCESU'          => $i_priceCESU_2,
                    'coeff'                => $request->get('se_taux'),
                    'sz_tva'               => $request->get('sz_tva_nature_2'),
                    'sz_tva_artisan'       => $request->get('sz_tva_artisan'),
                    'ttc_artisan'          => 0.0,
                ]);

                $status = $this->fAddPrestation($p2);

                //highligh the prestation when created
                $highlights[] = $p2->id_prestation;

                // Append facture data including the second prestation.
                $facture_client->montantTTC  += (float) ($p2->montantCESU + $p2->montant);
                $facture_client->montantCESU += (float)  $p2->montantCESU;
                $facture_client->montantHT += Prestation::getClientMontantHT($p2, $facture_client->montantTTC);
            }//p2
        }//clien&artisan
        //facture management 
        $o_facture = new FactureController();
        $i_idFacture = $o_facture->fGetLastNumFacture();
        $a_date_pres = explode('-',$sz_date_pres);
        $year=$a_date_pres[0];
        $mois=$a_date_pres[1];
        $jour=$a_date_pres[2];
        $sz_idFactureClient = $year."/".$i_idFacture;
    
        if(!empty($request->get('sz_textarea'))=="Complément sur facture")
        {
            $facture_client->txt_complement = "" ;
        }else{
            $facture_client->txt_complement = addslashes($request->get('sz_textarea')) ;
        }
        if(!empty($request->get('sz_reference')))
        {
            $facture_client->reference_prestation = addslashes($request->get('sz_reference')) ;
        }
        
        $facture_client->id_facture	    =   $sz_idFactureClient ;
        $facture_client->id_client      =   $p1->id_client;
        $facture_client->date           =   $sz_date_pres;
        
        $facture_client->save();

        // add artisan and coop bills and update prestations.
        if(!empty($facture_client->num_facture))
        {
            $i_idFacture = $o_facture->fGetLastNumFacture("Artisan");
            $sz_idFactureArtisan = $year.$mois.$jour;
            $sz_idFactureArtisan = $sz_idFactureArtisan."/".$i_idFacture;
            
            $a_finance = Prestation::getTauxAndCommission($p1, $facture_client->montantTTC);

            // Ajout facture artisan
            $facture_artisan = new Facture();
            $facture_artisan->typeFacture   = "Artisan";
            $facture_artisan->id_facture    = $sz_idFactureArtisan;
            $facture_artisan->id_artisans   = $p1->id_artisans;
            $facture_artisan->montantHT     = $a_finance["i_montantHTArtisan"];
            $facture_artisan->montantTTC    = $a_finance["i_montantTTCArtisan"];
            $facture_artisan->date          = $sz_date_pres;
            $facture_artisan->save();

            // Ajout facture cooperative
            $facture_coop = new Facture();
            $facture_coop->typeFacture      = "Cooperative";
            $facture_coop->id_facture       = "indef";
            $facture_coop->montantTTC       = $a_finance["i_gainCoop"];
            $facture_coop->montantHT        =  0 ;
            $facture_coop->date             = $sz_date_pres;
            $facture_coop->save();

            // Facture - Prestation mapping
            if(!empty($p2->id_prestation))
            {
                $p2->num_factureClient  = $facture_client->num_facture;
                $p2->num_factureArtisan = $facture_artisan->num_facture;
                $p2->ttc_artisan = $facture_artisan->montantTTC;
                $p2->save();

                $facture_client->factureprestation                  = new FacturePrestation();
                $facture_client->factureprestation->num_facture     = $facture_client->num_facture;
                $facture_client->factureprestation->id_prestation   = $p2->id_prestation;
                $facture_client->factureprestation->type            = "client";
                $facture_client->factureprestation->date            = date('Y-m-d');
                $facture_client->factureprestation->save();
    
                $facture_artisan->factureprestation                  = new FacturePrestation();
                $facture_artisan->factureprestation->num_facture     = $facture_artisan->num_facture;
                $facture_artisan->factureprestation->id_prestation   = $p2->id_prestation;
                $facture_artisan->factureprestation->type            = "artisan";
                $facture_artisan->factureprestation->date            = date('Y-m-d');
                $facture_artisan->factureprestation->save();
    
                $facture_coop->factureprestation                  = new FacturePrestation();
                $facture_coop->factureprestation->num_facture     = $facture_coop->num_facture;
                $facture_coop->factureprestation->id_prestation   = $p2->id_prestation;
                $facture_coop->factureprestation->type            = "coop";
                $facture_coop->factureprestation->date            = date('Y-m-d');
                $facture_coop->factureprestation->save();
            }

            $p1->num_factureClient  = $facture_client->num_facture;
            $p1->num_factureArtisan = $facture_artisan->num_facture;
            $p1->ttc_artisan        = $facture_artisan->montantTTC;
            $p1->save();

            $facture_client->factureprestation                  = new FacturePrestation();
            $facture_client->factureprestation->num_facture     = $facture_client->num_facture;
            $facture_client->factureprestation->id_prestation   = $p1->id_prestation;
            $facture_client->factureprestation->type            = "client";
            $facture_client->factureprestation->date            = date('Y-m-d');
            $facture_client->factureprestation->save();

            $facture_artisan->factureprestation                  = new FacturePrestation();
            $facture_artisan->factureprestation->num_facture     = $facture_artisan->num_facture;
            $facture_artisan->factureprestation->id_prestation   = $p1->id_prestation;
            $facture_artisan->factureprestation->type            = "artisan";
            $facture_artisan->factureprestation->date            = date('Y-m-d');
            $facture_artisan->factureprestation->save();

            $facture_coop->factureprestation                  = new FacturePrestation();
            $facture_coop->factureprestation->num_facture     = $facture_coop->num_facture;
            $facture_coop->factureprestation->id_prestation   = $p1->id_prestation;
            $facture_coop->factureprestation->type            = "coop";
            $facture_coop->factureprestation->date            = date('Y-m-d');
            $facture_coop->factureprestation->save();

        }

        // --
        $status['text'] = array();
        if(!empty($facture_client->num_facture) && !empty($facture_artisan->num_facture) && !empty($facture_coop->num_facture)){
            $status['text'][]   = "Félicitation!! la facture ".$facture_client->num_facture." vient d'être créée.";
            $status['style']    = "success";

            // Mail envoye au client
            if(!empty($client->email))
            {
                Mail::to($client->email)->send(new MailtrapFacture($artisan->nom, $facture_client->num_facture)); 
                $status['text'][] = "Le mail a bien éte envoyé au client.";
            }else{
                $status['text'][] = "Le client n'a pas fournit d'adresse mail, <a href='".env('APP_URL')."/public/pdf/".$facture_client->num_facture."'>veuillez imprimer la facture.</a>";
                $status['style'] = "warning";
            }

        }else{
            $status['text'][]   = "Une erreur est survenu lors de l'enregistrement de la facture!";
            $status['style']    = "danger";
            return view('prestations', ['status' => $status]);
        }
    
        $o_facture = new FactureController();
        $prestations = $o_facture->createRecapitulatifFetchArtisan(null);

        return view('prestations/index', compact('prestations'))
            ->with('status',$status)
            ->with('highlights',$highlights);
    }
//+-------------------------------------------------+
//+----------------PRIVATE PART---------------------+
//+-------------------------------------------------+
    private function fAddPrestation(Prestation $p)
    {
        $p->save();
        $status = array();
        if($p->id_prestation){
            $status['text']     = "Félicitation!! le prestation vient d'être créée.";
            $status['style']    = "success";
        }else{
            $status['text']     = "Une erreur est survenu lors de l'enregistrement de la prestation!";
            $status['style']    = "danger";
            return view('prestations/index', ['status' => $status]);
        }
        return $status;
    }
}
