<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class ClientController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::where('actif',1)->get();
        return view('clients.index', compact('clients'));
    }

  public function search($search_nom)
  {
    $option="";
    $clients = Client::where('actif',1)->where('nom', 'like', $search_nom . '%')->limit(10)->get();
    foreach($clients as $client)
    {
      $option .= $client->id_client.":".$client->nom." ".$client->prenom."@";
    }
    return $option;
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $client = new Client();
      return view('clients.create', compact('client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $this->do_validate($request); 

          $livret   = (!empty($request->get('se_livret')))? $request->get('se_livret') : 0 ;
          $fpapier  = (!empty($request->get('fpapier')))? $request->get('se_livret') : 0 ; 
          $efacture = (!empty($request->get('sz_email')))?1:0;
          $cStatus   = (!empty($request->get('se_status')))?$request->get('se_status'):1;

          $client = new Client([
            'civilite'          =>  $request->get('se_civilite'),
            //'attest_edition'    =>  $request->get('attest_edition'),
            //'attest_lecture'    =>  $request->get('attest_lecture'),
            'nom'               => $request->get('sz_nom'),
            'prenom'            => $request->get('sz_prenom'),
            'adresse1'          => $request->get('sz_adresse1'),
            'adresse2'          => $request->get('sz_adresse2'),
            'ville'             => $request->get('sz_ville'),
            'telephone'         => $request->get('sz_telephone'),
            'cp'                => $request->get('sz_cp'),
            'email'             => $request->get('sz_email'),
            //--
            'livret'            => $livret,
            'fpapier'           => $efacture,
            'b_efacture'        => $efacture,
            'actif'             => $cStatus
          ]);

          if($client->save()){
            $status['text'] = "Felicitation!! le nouveau client ".$client->civilite." ".$client->nom." ".$client->prenom." vient d'etre crée";
            $status['style'] = "success";
          }else{
            $status['text'] = "Une erreur est survenu et le nouveau client n\'a pas pu etre ajouter!";
            $status['style'] = "danger";
          }
          $clients = Client::all();
          return view('clients.index', compact('clients', 'client'), ['status' => $status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        return view('clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
      $this->do_validate($request); 

        $livret   = (!empty($request->get('se_livret')))? $request->get('se_livret') : 0 ;
        $fpapier  = (!empty($request->get('fpapier')))? $request->get('se_livret') : 0 ; 
        $efacture = (!empty($request->get('sz_email')))?1:0;
        $cStatus   = (!empty($request->get('se_status')))?$request->get('se_status'):1;

        $client = Client::where('id_client',$id)->first();

        $client->civilite       = $request->get('se_civilite');
      //$client->attest_edition = $request->get('attest_edition');
      //$client->attest_lecture = $request->get('attest_lecture');
        $client->nom            = $request->get('sz_nom');
        $client->prenom         = $request->get('sz_prenom');
        $client->adresse1       = $request->get('sz_adresse1');
        $client->adresse2       = $request->get('sz_adresse2');
        $client->ville          = $request->get('sz_ville');
        $client->telephone      = $request->get('sz_telephone');
        $client->cp             = $request->get('sz_cp');
        $client->email          = $request->get('sz_email');
        //--
        //$client->login             = $request->get('sz_login');
        //$client->email          = $request->get('sz_pwd');
        $client->actif          = $cStatus;
        $client->livret         = $livret;
        $client->fpapier        = $fpapier;
        $client->b_efacture     = $efacture;

        if($client->save()){
          $status['text'] = "Felicitation!! le client ".$client->civilite." ".$client->nom." ".$client->prenom." vient d'etre mis a jour";
          $status['style'] = "success";
          $status['nom'] = $client->nom;
        }else{
          $status['text'] = "Une erreur est survenu et le nouveau client n'a pas pu etre ajouter!";
          $status['style'] = "danger";
        }
        $clients = Client::all();
        return view('clients.index', compact('clients', 'client'), ['login'=>'Gregory PEREZ', 'status' => $status]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$client = Client::find($id);
        $client = Client::where('id_client', $id)->first();
        $client->delete();
   
        return redirect('/clients')->with('success', 'Stock has been deleted Successfully');
    }


    public function do_validate(Request $request){
      $request->validate([
        'se_civilite'=>'required',
        'sz_nom'=> 'required',
        'sz_prenom' => 'required',
        'sz_adresse1' => 'required',
        'sz_cp' => 'required',
        'sz_ville' => 'required'
      ]);
    }
}
