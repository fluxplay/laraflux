<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Facture;
use App\Client;
use App\Period;
use App\PdfController;
// Mail
use App\Mail\MailAttestation;
use Illuminate\Support\Facades\Mail;

class AttestationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function sendAttestation(Request $request)
    {
        $status = array();
        $status['text'] = array();

        $o_period = new Period();

        if (empty($request['year']) === false) {
            $o_period->setYear($request['year']);
        } 

        if (empty($request['idc']) === false) {
            $clientId = $request['idc'];
        } else {
            $clientId = 0;
            $status['text'][]   = "Impossible de recuperer l'identifiant du client!";
            $status['style']    = "danger";
        }


        if ($clientId > 0) {

            //$client = Client::where('id_client', $clientId)->where('actif', '1')->first();
            $client = Client::where('id_client', $clientId)->first();

            if (empty($client->email) === false ){//&& filter_var($client->email, FILTER_VALIDATE_EMAIL) === true ) {
               
                //Mail::to($client->email)->send(new MailAttestation($client->getClientName(), $o_period->getYear()));
                //le temps de trouver la config pour un dev env
                Mail::to('k3nrdx@gmail.com')->send(new MailAttestation($client->getClientName(), $o_period->getYear()));
                
                if (Mail::failures()) {
                    $status['text'][]   = "Impossible d'envoyer le mail au client!";
                    $status['style']    = "danger";
                }else{
                    $status['text'][]   = 'L\'attestation a bien ete envoye a <b>'.$client->getClientName().'</b> pour l\'annee <b>'.$o_period->getYear().'</b>';
                    $status['style']    = "success";
                }

            } else {
                $status['text'][]   = "Ce client n'a pas encore d'adresse mail valide!";
                $status['style']    = "danger";
            }

        }

        if (Auth::user()->getType() == "adm") {
            $lYear      = $o_period->getYear();
            $lYearPlus  = $lYear + 1;
            $dateFrom   = $lYear . '-01-01';
            $dateTo     = $lYearPlus . '-01-01';

            $attestations = Facture::where('typeFacture', 'Client')
            ->whereBetween('date', [$dateFrom, $dateTo])
                ->where('typeFacture', 'client')
                ->orderBy('id_client', 'DESC')
                ->limit(50)
                ->get();

            return view('attestations/index', compact('attestations'))
            ->with('status', $status)
            ->with('o_period', $o_period);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $o_period = new Period();
        if(empty($request['sz_year'])===false)
        {
            $o_period->setYear($request['sz_year']);
            $o_period->setMois(1);
        }else{
            $o_period->setYear(date('Y'));
            $o_period->setMois(1);
        }

        if(Auth::user()->getType()=="adm")
        {
            $lYear      = $o_period->getYear();
            $lYearPlus  = $lYear + 1;
            $dateFrom   = $lYear.'-01-01';
            $dateTo     = $lYearPlus.'-01-01';

            $attestations = Facture::where('typeFacture','Client')
            ->whereBetween('date', [$dateFrom, $dateTo])
            ->where('typeFacture', 'client')
            ->orderBy('id_client','DESC')
            ->limit(50)
            ->get();

            return view('attestations/index', compact('attestations'))
            ->with('o_period', $o_period);
        }else if(Auth::user()->getType()=="Artisan")
        {


        }else{

        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
